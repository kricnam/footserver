/*
 * DevListener.h
 *
 *  Created on: 2015��7��29��
 *      Author: zzy
 */

#ifndef DEVLISTENER_H_
#define DEVLISTENER_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <linux/netlink.h>
#include <list>
#include <dirent.h>
#include "common/ShareMem.h"
#include "common/map_share.h"

using namespace std;

#define MAP_SHARE_LSTN 	"/DevLstnShare"

class DevListener
{
public:
	DevListener();
	~DevListener();

	string m_strDirUDisk;
	int m_iDataVer;
	int m_iDataInv;
	list<string> lstCmdWait;
	string m_strIpMachine;

	int Listen();
private:
	ShareMem<shareDevLstn> m_bufShareLstn;
};

#endif /* DEVLISTENER_H_ */
