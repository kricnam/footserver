/*
 * Makeup.h
 *
 *  Created on: 2015��5��6��
 *      Author: zzy
 */

#ifndef MAKEUP_FOOT_H_
#define MAKEUP_FOOT_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <linux/input.h>
#include <sys/wait.h>
#include <list>
#include <map>
#include "GlobeDef.h"

using namespace std;

typedef pair<unsigned int, unsigned int> dotCordSerial;
typedef pair<unsigned int, unsigned int> dotCoord;
typedef pair<dotCordSerial, dotCoord> dotCoordInfo;

class Makeup
{
public:
	Makeup();
	~Makeup();

	list<string> m_lstCmdRecv;
	map<unsigned int, dotCoordInfo> dotCoordMap;

	void initDotCoordlMap();
	void SetParams(float fParam, unsigned int iInterval);
	bool Make(unsigned int szData[][4]);
	bool Massage01();
	bool TriDots01();
	bool TriDots02();
	bool MasgTriDots();
	bool DisposeModParam();
	bool DisposeMulParam();

private:
	list<string> m_lstCmd;
	float m_fParam;
	unsigned int m_iInterval;
	bool Add(string& strCmd, unsigned int iDotId, unsigned iValue);
	bool AddCoord(string& strCmd, unsigned int iDotId, unsigned iValue);
	bool AddCoord(string& strCmd, unsigned int iDotX,unsigned int iDotY, unsigned iValue);
};

#endif /* MAKEUP_H_ */
