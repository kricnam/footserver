/*
 * PressWork.cpp
 *
 *  Created on: 2015��7��16��
 *      Author: zzy
 */

#include <unistd.h>
#include <fcntl.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<stdio.h>
#include <errno.h>
#include <math.h>
#include <exception>
#include <vector>
#include "common/TraceLog.h"
#include "PressWork.h"

PressWork::PressWork()
{
	initPinNameMap();
	initPinSerialMap();
	initDotCoordlMap();
}

PressWork::~PressWork()
{

}

int PressWork::Working()
{
	DEBUG("---PressWork::Work---");
	int i = 0;
	while (m_objGpio.gpioctrl.Open(PRESSWORK_DEV) < 0)
	{
		ERROR("Cann't open PressWork Dev:%s!", PRESSWORK_DEV);
		sleep(10);
		i++;
		if (i > 10)
		{
			return -1;
		}
	}
	if (!m_SharePress.Create(MAP_SHARE_PRESS))
	{
		ERROR("Create PressWork share data fail!%s!", MAP_SHARE_PRESS);
	}
	Release();

	while (true)
	{
		if (true == m_SharePress.Get().bHMotorCtrl)
		{
			m_SharePress.Get().bHMotorCtrl = false;
			int iMSecs = m_SharePress.Get().iRunMSecs;
			switch (m_SharePress.Get().szMotorStatus)
			{
			case 0:
				DEBUG("szMotorStatus 0!");
				HMoveLStart();

				break;
			case 1:
				DEBUG("szMotorStatus 1!");
				HMoveLStop();
				//usleep(10000);
				break;
			case 2:
				DEBUG("szMotorStatus 2!");
				HMoveRStart();
				break;
			case 3:
				DEBUG("szMotorStatus 3!");
				HMoveRStop();
				//usleep(10000);
				break;
			case 4:
				DEBUG("szMotorStatus 4!Run Left Time:%d!", iMSecs);
				HMoveLStart();
				usleep(iMSecs * 1000);
				HMoveLStop();
				//usleep(10000);
				break;
			case 5:
				DEBUG("szMotorStatus 5!Run Right Time:%d!", iMSecs);
				HMoveRStart();
				usleep(iMSecs * 1000);
				HMoveRStop();
				//usleep(10000);
				break;
			default:
				DEBUG("HMotor Case not define:%d!",
						m_SharePress.Get().szMotorStatus);
				break;
			}

		}
		if (true == m_SharePress.Get().bWork)
		{
			if (m_SharePress.Get().bRelease == true)
			{
				DEBUG("Release!");
				Release();
				m_SharePress.Get().bWork = false;
				m_SharePress.Get().bRelease = false;
			}
			else
			{
				unsigned char chTmp = m_SharePress.Get().szValveMap[0][0];
				DEBUG("Working:%d Dots!", (unsigned int )chTmp - 1);
				if (chTmp > 0)
				{
					//					int i = 1;
					//					for (i = 1; i < chTmp; i++)
					//					{
					//						TRACE("Working Set:%d %d!",
					//								(int )m_SharePress.Get().szValveMap[i][0],
					//								(int )m_SharePress.Get().szValveMap[i][1]);
					//						if (false
					//								== SetValve(m_SharePress.Get().szValveMap[i][0],
					//										m_SharePress.Get().szValveMap[i][1]))
					//						{
					//							ERROR("SetValve fail!");
					//						}
					//					}
					SetAllDots();
				}
				else
				{
					ERROR("Data error:%d!", chTmp);
				}
				m_SharePress.Get().bWork = false;
			}
		}
		else
		{
			usleep(10000);
		}
	}
	WARNING("PressWork Quit!");
	return 0;
}

void PressWork::SetAllDots()
{
	DEBUG("PressWork::SetAllDots!");
	vector<unsigned char> vetDots;
	for (int i = 0; i <= MASG_DOT_SERIAL_23; i++)
	{
		vetDots.push_back(FLAG_TO_CLOSE);
	}
	unsigned char chCounte = m_SharePress.Get().szValveMap[0][0];
	for (unsigned char chIndex = 0; chIndex < chCounte; chIndex++)
	{
		unsigned char chDotNum = m_SharePress.Get().szValveMap[chIndex][0];
		unsigned char chDotValue = m_SharePress.Get().szValveMap[chIndex][1];
		vetDots[chDotNum] = chDotValue;
		TRACE("Working Dot Set:%d Value:%d!", (int )chDotNum, (int )chDotValue);
		if (chDotValue > 0)
		{
			DEBUG("chDotNum:%d!chDotValue:%d!", chDotNum, chDotValue);
		}
	}
	for (int i = 0; i <= MASG_DOT_SERIAL_7; i++)
	{
		DEBUG("ReSet ALL Dots i:%d!", i);
		if ((i == MASG_DOT_SERIAL_8) || (i == MASG_DOT_SERIAL_9))
		{
			DEBUG("Skip X Motor Left and Right Ctrl!%d!", i);
			continue;
		}
		if (false == SetValve(i, vetDots[i]))
		{
			DEBUG("SetValve fail!Num:%d Value:%d!", i, vetDots[i]);
		}
	}
}

void PressWork::initPinNameMap()
{
	DEBUG("initPinNameMap!");
	pinCtrlInfo infoTmp;
	//	infoTmp = make_pair(IO_CTRL_COLM0, IO_LINE_TOTAL);
	//	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TOTAL, infoTmp));

	infoTmp = make_pair(IO_CTRL_COLM_N, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(GAS_SWITCH_ALL, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM_N, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(GAS_HOLD_ALL, infoTmp));

	infoTmp = make_pair(IO_CTRL_COLM_ALL, IO_LINE_TRI1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(GAS_SWITCH_ALL_2, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM_ALL, IO_LINE_HOLD1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(GAS_HOLD_ALL_2, infoTmp));

	infoTmp = make_pair(IO_CTRL_COLM0, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI0, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM1, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI1, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM2, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI2, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM3, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI3, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM4, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI4, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM5, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI5, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM6, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI6, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM7, IO_LINE_TRI0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI7, infoTmp));

	infoTmp = make_pair(IO_CTRL_COLM0, IO_LINE_TRI1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI8, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM1, IO_LINE_TRI1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI9, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM2, IO_LINE_TRI1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI10, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM3, IO_LINE_TRI1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_TRI11, infoTmp));

	infoTmp = make_pair(IO_CTRL_COLM0, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD0, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM1, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD1, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM2, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD2, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM3, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD3, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM4, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD4, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM5, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD5, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM6, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD6, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM7, IO_LINE_HOLD0);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD7, infoTmp));

	infoTmp = make_pair(IO_CTRL_COLM0, IO_LINE_HOLD1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD8, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM1, IO_LINE_HOLD1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD9, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM2, IO_LINE_HOLD1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD10, infoTmp));
	infoTmp = make_pair(IO_CTRL_COLM3, IO_LINE_HOLD1);
	pinNameMap.insert(pair<string, pinCtrlInfo>(SWITCH_GAS_HOLD11, infoTmp));

	DEBUG("PinMap size:%d!", pinNameMap.size());
#ifdef DEBUG_FOOT
	map<string, pinCtrlInfo>::iterator itPin;
	for (itPin = pinNameMap.begin(); itPin != pinNameMap.end(); itPin++)
	{
		DEBUG("%s:%d %d!", (*itPin).first.c_str(), (*itPin).second.first,
				(*itPin).second.second);
	}
#endif
}

void PressWork::initPinSerialMap()
{
	DEBUG("initPinSerialMap!");
	pinCtrlInfo infoInOut, infoHold;
	pinCtrlDesc descPin;

	infoInOut = make_pair(IO_CTRL_COLM0, IO_LINE_TRI0);
	infoHold = make_pair(IO_CTRL_COLM0, IO_LINE_HOLD0);
	descPin = make_pair(infoInOut, infoHold);
	pinSerialMap.insert(
			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_0, descPin));

	infoInOut = make_pair(IO_CTRL_COLM1, IO_LINE_TRI0);
	infoHold = make_pair(IO_CTRL_COLM1, IO_LINE_HOLD0);
	descPin = make_pair(infoInOut, infoHold);
	pinSerialMap.insert(
			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_1, descPin));

	infoInOut = make_pair(IO_CTRL_COLM2, IO_LINE_TRI0);
	infoHold = make_pair(IO_CTRL_COLM2, IO_LINE_HOLD0);
	descPin = make_pair(infoInOut, infoHold);
	pinSerialMap.insert(
			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_4, descPin));

	infoInOut = make_pair(IO_CTRL_COLM3, IO_LINE_TRI0);
	infoHold = make_pair(IO_CTRL_COLM3, IO_LINE_HOLD0);
	descPin = make_pair(infoInOut, infoHold);
	pinSerialMap.insert(
			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_5, descPin));

	//For X Motor
	infoInOut = make_pair(IO_CTRL_COLM4, IO_LINE_TRI0);
	infoHold = make_pair(IO_CTRL_COLM4, IO_LINE_HOLD0);
	descPin = make_pair(infoInOut, infoHold);
	pinSerialMap.insert(
			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_8, descPin));

	//For X Motor
	infoInOut = make_pair(IO_CTRL_COLM5, IO_LINE_TRI0);
	infoHold = make_pair(IO_CTRL_COLM5, IO_LINE_HOLD0);
	descPin = make_pair(infoInOut, infoHold);
	pinSerialMap.insert(
			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_9, descPin));

//	infoInOut = make_pair(IO_CTRL_COLM6, IO_LINE_TRI0);
//	infoHold = make_pair(IO_CTRL_COLM6, IO_LINE_HOLD0);
//	descPin = make_pair(infoInOut, infoHold);
//	pinSerialMap.insert(
//			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_10, descPin));
//
//	infoInOut = make_pair(IO_CTRL_COLM7, IO_LINE_TRI0);
//	infoHold = make_pair(IO_CTRL_COLM7, IO_LINE_HOLD0);
//	descPin = make_pair(infoInOut, infoHold);
//	pinSerialMap.insert(
//			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_11, descPin));

	//////

//	infoInOut = make_pair(IO_CTRL_COLM0, IO_LINE_TRI1);
//	infoHold = make_pair(IO_CTRL_COLM0, IO_LINE_HOLD1);
//	descPin = make_pair(infoInOut, infoHold);
//	pinSerialMap.insert(
//			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_16, descPin));
//
//	infoInOut = make_pair(IO_CTRL_COLM1, IO_LINE_TRI1);
//	infoHold = make_pair(IO_CTRL_COLM1, IO_LINE_HOLD1);
//	descPin = make_pair(infoInOut, infoHold);
//	pinSerialMap.insert(
//			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_17, descPin));
//
//	infoInOut = make_pair(IO_CTRL_COLM2, IO_LINE_TRI1);
//	infoHold = make_pair(IO_CTRL_COLM2, IO_LINE_HOLD1);
//	descPin = make_pair(infoInOut, infoHold);
//	pinSerialMap.insert(
//			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_18, descPin));
//
//	infoInOut = make_pair(IO_CTRL_COLM3, IO_LINE_TRI1);
//	infoHold = make_pair(IO_CTRL_COLM3, IO_LINE_HOLD1);
//	descPin = make_pair(infoInOut, infoHold);
//	pinSerialMap.insert(
//			pair<unsigned int, pinCtrlDesc>(MASG_DOT_SERIAL_19, descPin));

	//////

#ifdef DEBUG_FOOT
	map<unsigned int, pinCtrlDesc>::iterator itPin;
	for (itPin = pinSerialMap.begin(); itPin != pinSerialMap.end(); itPin++)
	{
		DEBUG("Pin%d InOut:%d %d Hold:%d %d", (*itPin).first,
				(*itPin).second.first.first, (*itPin).second.first.second,
				(*itPin).second.second.first, (*itPin).second.second.second);
	}
#endif
}

void PressWork::initDotCoordlMap()
{
	DEBUG("PressWork::initDotCoordlMap!");
	dotCordSerial pairSerialTmp;
	dotCoord pairCoordTmp;
	dotCoordInfo pairCoordInfoTmp;
	map<dotCordSerial, dotCoord>::iterator itCoords;
	int iCountDots = 0;
	int iCountLine = 0;
	int iCountColm = 0;
	for (iCountLine = 0; iCountLine < DOTS_LINES_TOTAL; iCountLine++)
	{
		if ((iCountLine % 2) == 0)
		{
			for (iCountColm = 0; iCountColm < DOTS_LINE_DOUBLE * 2;
					iCountColm++)
			{
				pairSerialTmp.first = iCountLine;
				pairSerialTmp.second = iCountColm;
				if (iCountColm < DOTS_LINE_DOUBLE)
				{
					pairCoordTmp.first = DOT_CORD_STARTX_DOUBLE
							+ DOT_CORD_STEPLENX / 2
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				else
				{
					pairCoordTmp.first = DOT_CORD_STARTX_DOUBLE
							+ DOT_CORD_STEPLENX / 2 + DOT_CORD_OFFSET_MIRROR
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				pairCoordTmp.second = WIN_WIDTH_SIDE + DOT_CORD_STEPLENY / 2
						+ DOT_CORD_STEPLENY * iCountLine
						- WIN_WIDTH_OUTSIDE / 2;	//middle zero
				pairCoordInfoTmp = make_pair(pairSerialTmp, pairCoordTmp);
				DEBUG("DOT[%d]:%d %d %d %d!Line:%d Column:%d!", iCountDots,
						pairCoordInfoTmp.first.first,
						pairCoordInfoTmp.first.second,
						pairCoordInfoTmp.second.first,
						pairCoordInfoTmp.second.second, iCountLine, iCountColm);
				dotCoordMap.insert(
						pair<unsigned int, dotCoordInfo>(iCountDots,
								pairCoordInfoTmp));
				iCountDots++;
			}
		}
		else
		{
			for (iCountColm = 0; iCountColm < DOTS_LINE_SIGNLE * 2;
					iCountColm++)
			{
				pairSerialTmp.first = iCountLine;
				pairSerialTmp.second = iCountColm;
				if (iCountColm < DOTS_LINE_SIGNLE)
				{
					pairCoordTmp.first = DOT_CORD_STARTX_SINGLE
							+ DOT_CORD_STEPLENX / 2
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				else
				{
					pairCoordTmp.first = DOT_CORD_STARTX_SINGLE
							+ DOT_CORD_STEPLENX / 2 + DOT_CORD_OFFSET_MIRROR
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				pairCoordTmp.second = WIN_WIDTH_SIDE + DOT_CORD_STEPLENY / 2
						+ DOT_CORD_STEPLENY * iCountLine
						- WIN_WIDTH_OUTSIDE / 2;	//middle zero
				pairCoordInfoTmp = make_pair(pairSerialTmp, pairCoordTmp);
				DEBUG("DOT[%d]:%d %d %d %d!Line:%d Column:%d!", iCountDots,
						pairCoordInfoTmp.first.first,
						pairCoordInfoTmp.first.second,
						pairCoordInfoTmp.second.first,
						pairCoordInfoTmp.second.second, iCountLine, iCountColm);
				dotCoordMap.insert(
						pair<unsigned int, dotCoordInfo>(iCountDots,
								pairCoordInfoTmp));
				iCountDots++;
			}
		}
	}
#ifdef DEBUG_FOOT
	map<unsigned int, dotCoordInfo>::iterator itDot;
	for (itDot = dotCoordMap.begin(); itDot != dotCoordMap.end(); itDot++)
	{
		DEBUG("DOT[%d]:%d %d %d %d!", (*itDot).first,
				(*itDot).second.first.first, (*itDot).second.first.second,
				(*itDot).second.second.first, (*itDot).second.second.second);
	}
#endif
}

void PressWork::Release()
{
	DEBUG("Release Open!");
	SetValve(GAS_SWITCH_ALL, GAS_OUTGOING);
	SetValve(GAS_HOLD_ALL, SWITCH_OPEN);
	SetValve(GAS_SWITCH_ALL_2, GAS_OUTGOING);
	SetValve(GAS_HOLD_ALL_2, SWITCH_OPEN);
	//SetValve(SWITCH_GAS_TOTAL, GAS_INCOMING);
}

bool PressWork::SetValve(const char* szName, unsigned int iValue)
{
	map<string, pinCtrlInfo>::iterator itPin;
	if ((itPin = pinNameMap.find(szName)) != pinNameMap.end())
	{
		if (m_objGpio.SetControlValue((*itPin).second.first,
				(*itPin).second.second, iValue) > 0)
		{
			return true;
		}
	}
	else
	{
		ERROR("Cann't find Pin:%s!", szName);
	}
	DEBUG("Set Valve fail!");
	return false;
}

bool PressWork::SetValve(unsigned int iDotId, unsigned int iValue)
{
	DEBUG("SetValve:%d %d!", iDotId, iValue);
	map<unsigned int, pinCtrlDesc>::iterator itPin;
	itPin = pinSerialMap.find(iDotId);
	if (itPin != pinSerialMap.end())
	{
		DEBUG("SetValve GAS INCOMIG CORD:%d %d!", (*itPin).second.first.first,
				(*itPin).second.first.second);
		DEBUG("SetValve GAS HOLD CORD :%d %d!", (*itPin).second.second.first,
				(*itPin).second.second.second);
	}
	else
	{
		DEBUG("Cannot find Dot[%d] in pinSerialMap!", iDotId);
		return false;
	}

	if (itPin != pinSerialMap.end())
	{
		if (iValue == FLAG_TO_CLOSE)
		{
			DEBUG("FLAG_TO_CLOSE!");
			if ((m_objGpio.SetControlValue((*itPin).second.first.first,
					(*itPin).second.first.second, GAS_OUTGOING) > 0)
					&& (m_objGpio.SetControlValue((*itPin).second.second.first,
							(*itPin).second.second.second, SWITCH_OPEN) > 0))
			{
				TRACE("CLOSE SUCCESS!");
				return true;
			}
		}
		else if (iValue == FLAG_TO_OPEN)
		{
			DEBUG("FLAG_TO_OPEN!");
			if ((m_objGpio.SetControlValue((*itPin).second.first.first,
					(*itPin).second.first.second, GAS_INCOMING) > 0)
					&& (m_objGpio.SetControlValue((*itPin).second.second.first,
							(*itPin).second.second.second, SWITCH_CLOSE) > 0))
			{
				TRACE("INCOMING/HOLD SUCCESS!");
				return true;
			}
		}
		else if (iValue == FLAG_TO_HOLD)
		{
			DEBUG("FLAG_TO_HOLD!");
			if (m_objGpio.SetControlValue((*itPin).second.first.first,
					(*itPin).second.first.second, SWITCH_CLOSE) > 0)
			{
				DEBUG("HOLD ONLY SUCCESS!");
				return true;
			}
		}
		else if (iValue == FLAG_TO_NOHD)
		{
			DEBUG("FLAG_TO_NOHD!");
			if (m_objGpio.SetControlValue((*itPin).second.second.first,
					(*itPin).second.second.second, SWITCH_OPEN) > 0)
			{
				TRACE("NOHD ONLY SUCCESS!");
				return true;
			}
		}
		//		else if (iValue == FLAG_TO_NOHD)
		//		{
		//			if (m_objGpio.SetControlValue((*itPin).second.second.first,
		//					(*itPin).second.second.second, SWITCH_OPEN) > 0)
		//			{
		//				DEBUG("NOHD ONLY SUCCESS!");
		//				return true;
		//			}
		//		}
		else
		{
			ERROR("Illegal value to Set Pin[%d]:%d!", iDotId, iValue);
		}
	}
	else
	{
		ERROR("Cann't find PinId:%d!", iDotId);
	}
	DEBUG("Set Valve fail!");
	return false;
}

bool PressWork::SetValves(map<unsigned int, unsigned int> mapDotsToSet)
{
	DEBUG("SetValves size:%d!", mapDotsToSet.size());
	map<unsigned int, unsigned int>::iterator itDotToSet;
	map<unsigned int, pinCtrlDesc>::iterator itPinsMap;
	for (itDotToSet = mapDotsToSet.begin(); itDotToSet != mapDotsToSet.end();
			itDotToSet++)
	{
		if ((itPinsMap = pinSerialMap.find((*itDotToSet).first))
				!= pinSerialMap.end())
		{
			if ((*itDotToSet).second == FLAG_TO_CLOSE)
			{
				if (m_objGpio.SetControlValue((*itPinsMap).second.first.first,
						(*itPinsMap).second.first.second, GAS_OUTGOING) <= 0)
				{
					WARNING("Set Pin fail:%d %d %d!",
							(*itPinsMap).second.first.first,
							(*itPinsMap).second.first.second, GAS_OUTGOING);
				}
			}
			//todo: for PressLevel Set
			else if (((*itDotToSet).second > 0) && ((*itDotToSet).second <= 10))
			{
				if (m_objGpio.SetControlValue((*itPinsMap).second.first.first,
						(*itPinsMap).second.first.second, GAS_INCOMING) <= 0)
				{
					WARNING("Set Pin fail:%d %d %d!",
							(*itPinsMap).second.first.first,
							(*itPinsMap).second.first.second, GAS_INCOMING);
				}
			}
			else if ((*itDotToSet).second == FLAG_TO_HOLD)
			{
				if (m_objGpio.SetControlValue((*itPinsMap).second.second.first,
						(*itPinsMap).second.second.second, SWITCH_CLOSE) <= 0)
				{
					WARNING("Set Pin fail:%d %d %d!",
							(*itPinsMap).second.second.first,
							(*itPinsMap).second.second.second, SWITCH_CLOSE);
				}
			}
			else if ((*itDotToSet).second == FLAG_TO_NOHD)
			{
				if (m_objGpio.SetControlValue((*itPinsMap).second.second.first,
						(*itPinsMap).second.second.second, SWITCH_OPEN) <= 0)
				{
					WARNING("Set Pin fail:%d %d %d!",
							(*itPinsMap).second.second.first,
							(*itPinsMap).second.second.second, SWITCH_OPEN);
				}
			}
			else
			{
				ERROR("Illegal value to Set Pin[%d]:%d!", (*itDotToSet).first,
						(*itDotToSet).second);
				return false;
			}
		}
	}
	return true;
}

int PressWork::GetDotNearestX(unsigned int iPosX, bool bReversed)
{
	DEBUG("GetDotNearestX:%d Reverse:%s!", iPosX,
			bReversed == true ? "true" : "false");
//
//	iPosX = iPosX * 12 / 10;
//	DEBUG("For test: *1.2", iPosX);
	int iSerial = 0;
	float distanceTmp = 0;
	float distance;
	int iTest = 0;
	if (bReversed == false)
	{
		TRACE("iTest:%d!", iTest);
		map<unsigned int, dotCoordInfo>::iterator itCoords =
				dotCoordMap.begin();
		distance = abs((*itCoords).second.second.first - iPosX);
		iSerial = (*itCoords).first;
		for (itCoords = dotCoordMap.begin(); itCoords != dotCoordMap.end();
				itCoords++)
		{
			distanceTmp = abs((*itCoords).second.second.first - iPosX);
			if (distanceTmp < distance)
			{
				distance = distanceTmp;
				iSerial = (*itCoords).first;
				DEBUG("Nearest Dot[%d]:%f!distanceTmp:%f!Dot Pos:%d!", iSerial,
						distance, distanceTmp, (*itCoords).second.second.first);
			}
			else
			{
				DEBUG("Far Dot[%d]:%f!distanceTmp:%f!Dot Pos:%d!",
						(*itCoords).first, distance, distanceTmp,
						(*itCoords).second.second.first);
			}
		}
		iTest++;
	}
	else
	{
		map<unsigned int, dotCoordInfo>::iterator itCoords = dotCoordMap.end();
		distance = abs((*itCoords).second.second.first - iPosX);
		iSerial = (*itCoords).first;
		for (itCoords = dotCoordMap.end(); itCoords != dotCoordMap.begin();
				itCoords--)
		{
			TRACE("iTest:%d!", iTest);
			distanceTmp = abs((*itCoords).second.second.first - iPosX);
			if (distanceTmp < distance)
			{
				distance = distanceTmp;
				iSerial = (*itCoords).first;
				DEBUG("Nearest Dot[%d]:%f!", iSerial, distance);
			}
			iTest++;
		}
	}
	DEBUG("Rtn iSerial:%d!", iSerial);
	return iSerial;
}

int PressWork::GetWinDotNearest(unsigned int iPosX, unsigned int iPosY)
{
	DEBUG("GetWinDotNearest:%d %d!", iPosX, iPosY);
	//	iPosX = iPosX * 12 / 10;
	//	DEBUG("For test: *1.2", iPosX);
	map<unsigned int, dotCoordInfo>::iterator itCoords = dotCoordMap.begin();
	float distance = sqrt(
			(iPosX - (*itCoords).second.second.first)
					* (iPosX - (*itCoords).second.second.first)
					+ (iPosY - (*itCoords).second.second.second)
							* (iPosY - (*itCoords).second.second.second));
	float distanceTmp = 0;
	int iSerial = 0;
	int iTest = 0;
	for (itCoords = dotCoordMap.begin(); itCoords != dotCoordMap.end();
			itCoords++)
	{
		TRACE("iTest:%d!", iTest);
		distanceTmp = sqrt(
				(iPosX - (*itCoords).second.second.first)
						* (iPosX - (*itCoords).second.second.first)
						+ (iPosY - (*itCoords).second.second.second)
								* (iPosY - (*itCoords).second.second.second));
		if (distanceTmp < distance)
		{
			distance = distanceTmp;
			iSerial = (*itCoords).first;
			DEBUG("Nearest Dot[%d]:%f!", iSerial, distance);
		}
		iTest++;
	}
	return iSerial;
}

void PressWork::HMoveLStart()
{
	DEBUG("HMoveLStart:%d %d %d!", HORI_MOTOR_LEFT_X, HORI_MOTOR_LEFT_Y,
			HORI_MOTOR_LEFT_START);
	if (m_objGpio.SetControlValue(HORI_MOTOR_LEFT_X, HORI_MOTOR_LEFT_Y,
	HORI_MOTOR_LEFT_START) <= 0)
	{
		WARNING("HMoveLStart fail:%d %d %d!", HORI_MOTOR_LEFT_X,
				HORI_MOTOR_LEFT_Y, HORI_MOTOR_LEFT_START);
	}
}

void PressWork::HMoveLStop()
{
	DEBUG("HMoveLStop:%d %d %d!", HORI_MOTOR_LEFT_X, HORI_MOTOR_LEFT_Y,
			HORI_MOTOR_LEFT_STOP);
	if (m_objGpio.SetControlValue(HORI_MOTOR_LEFT_X, HORI_MOTOR_LEFT_Y,
	HORI_MOTOR_LEFT_STOP) <= 0)
	{
		WARNING("HMoveLStop fail:%d %d %d!", HORI_MOTOR_LEFT_X,
				HORI_MOTOR_LEFT_Y, HORI_MOTOR_LEFT_STOP);
	}
}

void PressWork::HMoveRStart()
{
	DEBUG("HMoveRStart:%d %d %d!", HORI_MOTOR_RIGHT_X, HORI_MOTOR_RIGHT_Y,
			HORI_MOTOR_RIGHT_START);
	if (m_objGpio.SetControlValue(HORI_MOTOR_RIGHT_X, HORI_MOTOR_RIGHT_Y,
	HORI_MOTOR_RIGHT_START) <= 0)
	{
		WARNING("HMoveRStart fail:%d %d %d!", HORI_MOTOR_RIGHT_X,
				HORI_MOTOR_RIGHT_Y, HORI_MOTOR_RIGHT_START);
	}
}

void PressWork::HMoveRStop()
{
	DEBUG("HMoveRStop:%d %d %d!", HORI_MOTOR_RIGHT_X, HORI_MOTOR_RIGHT_Y,
			HORI_MOTOR_RIGHT_STOP);
	if (m_objGpio.SetControlValue(HORI_MOTOR_RIGHT_X, HORI_MOTOR_RIGHT_Y,
	HORI_MOTOR_RIGHT_STOP) <= 0)
	{
		WARNING("HMoveRStop fail:%d %d %d!", HORI_MOTOR_RIGHT_X,
				HORI_MOTOR_RIGHT_Y, HORI_MOTOR_RIGHT_STOP);
	}
}
