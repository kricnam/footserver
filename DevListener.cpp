/*
 * DevListener.cpp
 *
 *  Created on: 2015��7��29��
 *      Author: zzy
 */

#include <fcntl.h>
#include "common/TraceLog.h"
#include "DevListener.h"

DevListener::DevListener()
{

}

DevListener::~DevListener()
{

}

int DevListener::Listen()
{
	if (!m_bufShareLstn.Create(MAP_SHARE_LSTN))
	{
		ERROR("Create share data fail!%s!", MAP_SHARE_LSTN);
	}
	else
	{
		memset(m_bufShareLstn.Get().szDevInf, 0x0,
				sizeof(m_bufShareLstn.Get().szDevInf));
		m_bufShareLstn.Get().bDiskInsert = false;
	}
	int iSockfd;
	struct sockaddr_nl socketAdd;
	int iLen;
	char szBuf[4096];
	struct iovec iovInfo;
	struct msghdr msgHw;
	int iCounter;
	bool bSockPrepared = false;
	bool bLoop = true;

	memset(&socketAdd, 0, sizeof(socketAdd));
	socketAdd.nl_family = AF_NETLINK;
	socketAdd.nl_groups = NETLINK_KOBJECT_UEVENT;
	socketAdd.nl_pid = 0;
	memset(&msgHw, 0, sizeof(msgHw));
	iovInfo.iov_base = (void *) szBuf;
	iovInfo.iov_len = sizeof(szBuf);
	msgHw.msg_name = (void *) &socketAdd;
	msgHw.msg_namelen = sizeof(socketAdd);
	msgHw.msg_iov = &iovInfo;
	msgHw.msg_iovlen = 1;

	int i = 0;
	while (bSockPrepared == false)
	{
		iSockfd = socket(AF_NETLINK, SOCK_RAW, NETLINK_KOBJECT_UEVENT);
		if (iSockfd == -1)
		{
			ERROR("Try to Create Socket failed:%s\n", strerror(errno));
			sleep(3);
		}
		else
		{
			if (bind(iSockfd, (struct sockaddr *) &socketAdd, sizeof(socketAdd))
					== -1)
			{
				ERROR("Bind error:%s\n", strerror(errno));
				sleep(3);
			}
			else
			{
				bSockPrepared = true;
			}
		}
		i++;
		if (i > 10)
		{
			return -1;
		}
	}

	string strFormat;
	string strDiskDir;
	string strUEvent;
	bool bAdd = false;
	bool bUsb = false;
	bool bDisk = false;
	int iPosStartDir, iPosEndDir, iPosStartFmt, iPosEndFmt;
	while (bLoop)
	{
		memset(msgHw.msg_iov->iov_base, 0x0, msgHw.msg_iov->iov_len);
		strUEvent.clear();
		iLen = recvmsg(iSockfd, &msgHw, 0);
		if (iLen < 0)
		{
			ERROR("Receive error\n");
		}
		else if (iLen < 32 || iLen > sizeof(szBuf))
		{
			ERROR("Invalid message:%d Bytes!", iLen);
		}
		else
		{
			for (iCounter = 0; iCounter < iLen; iCounter++)
			{
				if (*(szBuf + iCounter) == '\0')
				{
					szBuf[iCounter] = '\n';
				}
			}
			DEBUG("Received %d bytes\n%s\n", iLen, szBuf);
			strUEvent = szBuf;
			if (strUEvent.find("ACTION=add") != string::npos)
			{
				bAdd = true;
			}
			else if (strUEvent.find("ACTION=remove") != string::npos)
			{
				bDisk = false;
				bUsb = false;
				bAdd = false;
				strFormat.clear();
				strDiskDir.clear();
				strUEvent.clear();
			}
			if (strUEvent.find("ID_BUS=usb") != string::npos)
			{
				bUsb = true;
			}
			if (strUEvent.find("ID_TYPE=disk") != string::npos)
			{
				bDisk = true;
			}

			if ((iPosStartDir = strUEvent.find("DEVNAME=")) != string::npos)
			{
				DEBUG("DIR Start:%d!", iPosStartDir);
				string strDirTmp = strUEvent.substr(
						iPosStartDir + sizeof("DEVNAME=") - 1);
				DEBUG("Found UDisk Dir!%s!", strDirTmp.c_str());
				if ((iPosEndDir = strDirTmp.find('\n')) != string::npos)
				{
					DEBUG("DIR Len:%d!", iPosEndDir);
					strDiskDir = strDirTmp.substr(0, iPosEndDir);
					DEBUG("USB DISK Dir:%s!", strDiskDir.c_str());
				}
			}
			if ((iPosStartFmt = strUEvent.find("ID_FS_TYPE=")) != string::npos)
			{
				DEBUG("FMT Start:%d!", iPosStartFmt);
				string strFmtTmp = strUEvent.substr(
						iPosStartFmt + sizeof("ID_FS_TYPE=") - 1);
				DEBUG("Found ID_FS_TYPE:%s!", strFmtTmp.c_str());
				if ((iPosEndFmt = strFmtTmp.find('\n')) != string::npos)
				{
					DEBUG("FMT Len:%d!", iPosEndFmt);
					strFormat = strFmtTmp.substr(0, iPosEndFmt);
					DEBUG("USB DISK Format:%s!", strFormat.c_str());
				}
			}
			if ((bAdd == true) && (bUsb == true) && (bDisk == true)
					&& (strDiskDir.size() >= 3))
			{
				DEBUG("Found UDisk Info!");
				sprintf(m_bufShareLstn.Get().szDevInf, "%s",
						strDiskDir.c_str());
				if (strFormat.size() > 3)
				{
					sprintf(m_bufShareLstn.Get().szFormat, "%s",
							strFormat.c_str());
				}
				bDisk = false;
				bUsb = false;
				bAdd = false;
				strFormat.clear();
				strDiskDir.clear();
				strUEvent.clear();
			}

		}
	}
	ERROR("Quit Device Listener!");
	return 0;
}
