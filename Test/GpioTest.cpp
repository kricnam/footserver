/*
 * GpioTest.cpp
 *
 *  Created on: 2015��1��14��
 *      Author: zzy
 */

#include "../common/TraceLog.h"
#include "../common/Paralell_Control.h"
#include "../common/motor_control.h"
#include "../GlobeDef.h"
#define SetBit(x,y) x|=(1<<y)
#define ClrBit(x,y) x&=~(1<<y)

//#define TESTDOOR

#ifdef TESTDOOR
#define GPIO_DEV "/dev/door"
#else
#define PWM_CONTROLER "/dev/motor"
#endif

#define TEST_01
#ifdef TEST_01
#define GPIO_DEV "/dev/door"
#endif
#ifdef TEST_02

static long testData07[][9] =
{
	{	1, 30, 300, 1, 30, 300, 1, 30, 300},
	{	1, 60, 600, 1, 60, 600, 1, 60, 600},
	{	1, 90, 900, 1, 90, 900, 1, 90, 900},
	{	1, 60, 600, 1, 60, 600, 1, 60, 600},
	{	1, 30, 300, 1, 30, 300, 1, 30, 300},
	{	1, 60, 600, 1, 60, 600, 1, 60, 600},
	{	1, 90, 900, 1, 90, 900, 1, 90, 900},
	{	1, 60, 600, 1, 60, 600, 1, 60, 600},
	{	1, 30, 300, 1, 30, 300, 1, 30, 300},
	{	1, 60, 600, 1, 60, 600, 1, 60, 600}};

static long testData05[][9] =
{
	{	0, 0, 0, 1, 100, 500, 0, 0, 0},
	{	0, 0, 0, 1, 100, 100, 0, 0, 0},
	{	0, 0, 0, 1, 100, 500, 0, 0, 0},
	{	0, 0, 0, 1, 100, 100, 0, 0, 0},
	{	0, 0, 0, 1, 100, 500, 0, 0, 0},
	{	0, 0, 0, 1, 100, 100, 0, 0, 0},
	{	0, 0, 0, 1, 100, 500, 0, 0, 0},
	{	0, 0, 0, 1, 100, 100, 0, 0, 0},
	{	0, 0, 0, 1, 100, 500, 0, 0, 0},
	{	0, 0, 0, 1, 100, 100, 0, 0, 0}};

static long testData06[][9] =
{
	{	0, 0, 0, 0, 100, 500, 0, 0, 0},
	{	0, 0, 0, 0, 100, 100, 0, 0, 0},
	{	0, 0, 0, 0, 100, 500, 0, 0, 0},
	{	0, 0, 0, 0, 100, 100, 0, 0, 0},
	{	0, 0, 0, 0, 100, 500, 0, 0, 0},
	{	0, 0, 0, 0, 100, 100, 0, 0, 0},
	{	0, 0, 0, 0, 100, 500, 0, 0, 0},
	{	0, 0, 0, 0, 100, 100, 0, 0, 0},
	{	0, 0, 0, 0, 100, 500, 0, 0, 0},
	{	0, 0, 0, 0, 100, 100, 0, 0, 0}};

static long testData03[][9] =
{
	{	1, 20, 200, 0, 0, 0, 0, 0, 0},
	{	1, 40, 400, 0, 0, 0, 0, 0, 0},
	{	1, 60, 600, 0, 0, 0, 0, 0, 0},
	{	1, 80, 800, 0, 0, 0, 0, 0, 0},
	{	1, 100, 1000, 0, 0, 0, 0, 0, 0},
	{	1, 80, 800, 0, 0, 0, 0, 0, 0},
	{	1, 60, 600, 0, 0, 0, 0, 0, 0},
	{	1, 40, 400, 0, 0, 0, 0, 0, 0},
	{	1, 20, 200, 0, 0, 0, 0, 0, 0},
	{	1, 10, 100, 0, 0, 0, 0, 0, 0}};

static long testData04[][9] =
{
	{	0, 20, 200, 0, 0, 0, 0, 0, 0},
	{	0, 40, 400, 0, 0, 0, 0, 0, 0},
	{	0, 60, 600, 0, 0, 0, 0, 0, 0},
	{	0, 80, 800, 0, 0, 0, 0, 0, 0},
	{	0, 100, 1000, 0, 0, 0, 0, 0, 0},
	{	0, 80, 800, 0, 0, 0, 0, 0, 0},
	{	0, 60, 600, 0, 0, 0, 0, 0, 0},
	{	0, 40, 400, 0, 0, 0, 0, 0, 0},
	{	0, 20, 200, 0, 0, 0, 0, 0, 0},
	{	0, 10, 100, 0, 0, 0, 0, 0, 0}};

static long testData[][9] =
{
	{	1, 100, 1000, 1, 100, 1000, 1, 100, 1000},
	{	1, 10, 100, 1, 10, 100, 1, 10, 100},
	{	1, 100, 1000, 1, 100, 1000, 1, 100, 1000},
	{	1, 10, 100, 1, 10, 100, 1, 10, 100},
	{	1, 100, 1000, 1, 100, 1000, 1, 100, 1000},
	{	1, 10, 100, 1, 10, 100, 1, 10, 100},
	{	1, 100, 1000, 1, 100, 1000, 1, 100, 1000},
	{	1, 10, 100, 1, 10, 100, 1, 10, 100},
	{	1, 100, 1000, 1, 100, 1000, 1, 100, 1000},
	{	1, 10, 100, 1, 10, 100, 1, 10, 100}};

static long testData02[][9] =
{
	{	0, 100, 1000, 0, 100, 1000, 0, 100, 1000},
	{	0, 10, 100, 0, 10, 100, 0, 10, 100},
	{	0, 100, 1000, 0, 100, 1000, 0, 100, 1000},
	{	0, 10, 100, 0, 10, 100, 0, 10, 100},
	{	0, 100, 1000, 0, 100, 1000, 0, 100, 1000},
	{	0, 10, 100, 0, 10, 100, 0, 10, 100},
	{	0, 100, 1000, 0, 100, 1000, 0, 100, 1000},
	{	0, 10, 100, 0, 10, 100, 0, 10, 100},
	{	0, 100, 1000, 0, 100, 1000, 0, 100, 1000},
	{	0, 10, 100, 0, 10, 100, 0, 10, 100}};

int main()
{
	DEBUG("---Test---");
	SETTRACELEVEL(0);

#ifdef TESTDOOR
	ParalellControl m_objGpio;
	if (m_objGpio.gpioctrl.Open(GPIO_DEV) < 0)
	{
		ERROR("Cann't open GPIO Dev:%s!", GPIO_DEV);
		return -1;
	}
	int i = 0;
	while (true)
	{
		if (i % 2 == 0)
		{
			DEBUG("Set High VDATA:%d!", i);
			m_objGpio.gpioctrl.SetVal(1, VDATA);
		}
		else
		{
			DEBUG("Set Low VDATA:%d!", i);
			m_objGpio.gpioctrl.SetVal(0, VDATA);
		}
		sleep(5);
		if (i > 60)
		{
			break;
		}
		i++;
	}
	return 0;

#else

	MotorControl m_objPwmCtrl;

	if (m_objPwmCtrl.Open(PWM_CONTROLER) < 0)
	{
		ERROR("Cann't open GPIO Dev:%s!", PWM_CONTROLER);
		return -1;
	}
	sleep(3);
	int iCount = 30;
	int iSleepSecs = 1;
	int iBackLen = 570;
	int iDist = 300;
	//int speedbase=150/4;
	int iSpeed = 150;
	while (iCount)
	{
		DEBUG("iCount=%d!", iCount);
		if ((iCount % 2) == 0)
		{
			//			m_objPwmCtrl.addCtrlData(testData03);
			m_objPwmCtrl.move(1, 1, iDist, iSpeed);
		}
		else
		{
			//			m_objPwmCtrl.addCtrlData(testData04);
			m_objPwmCtrl.move(1, 0, iDist, iSpeed);
			//			m_objPwmCtrl.move(1, 0, iBackLen, 600);
			//			m_objPwmCtrl.move(2, 0, iBackLen, 600);
		}
		//		if ((iCount % 2) == 0)
		//		{
		//			m_objPwmCtrl.move(0, 1, 2000, 1000);
		//		}
		//		else
		//		{
		//			m_objPwmCtrl.move(0, 0, 2000, 1000);
		//		}
		sleep(iDist / iSpeed + 1);
		iCount--;
	}
	sleep(iDist / iSpeed + 1);
	m_objPwmCtrl.Close();
#endif
	return 0;
}

#endif

#ifdef TEST_01
int main()
{
	DEBUG("---PressWork::Work---");
	SETTRACELEVEL(0);
	ParalellControl m_objGpio;
	if (m_objGpio.gpioctrl.Open(GPIO_DEV) < 0)
	{
		ERROR("Cann't open GPIO Dev:%s!", GPIO_DEV);
		return -1;
	}
	unsigned char chL = 0;
	unsigned char chLC = 2;
	unsigned char chR = 0;
	int iCount = 5;
	SetBit(chR, 0);
	SetBit(chR, 2);
	SetBit(chR, 1);
	SetBit(chR, 3);
	SetBit(chR, 3);
	int iCycle = 0;
	while (true)
	{
		//		DEBUG("Open All!");
		//		m_objGpio.SetControlValue(chR, chL, GPIO_OPEN);
		//		sleep(10);
		//		DEBUG("Close All!");
		//		m_objGpio.SetControlValue(chR, chL, GPIO_CLOSE);
		//		sleep(10);

		for (iCycle = 0; iCycle < iCount; iCycle++)
		{
			SetBit(chR, iCycle);
			DEBUG("Open IN and OUT!%d %d %d!", chR, chL, chLC);
			m_objGpio.SetControlValue(chR, chL, GPIO_OPEN);
			m_objGpio.SetControlValue(chR, chLC, GPIO_OPEN);
			sleep(1);
			DEBUG("Close OUT!%d %d %d!", chR, chL, chLC);
			m_objGpio.SetControlValue(chR, chLC, GPIO_CLOSE);
			sleep(1);
			DEBUG("Close IN!%d %d %d!", chR, chL, chLC);
			m_objGpio.SetControlValue(chR, chL, GPIO_CLOSE);
			chR = 0;
			sleep(3);
		}
		sleep(3);
	}
	return 0;
}
#endif

