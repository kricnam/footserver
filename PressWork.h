/*
 * PressWork.h
 *
 *  Created on: 2015��7��16��
 *      Author: zzy
 */

#ifndef PRESSWORK_H_
#define PRESSWORK_H_
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include "common/ShareMem.h"
#include "common/Paralell_Control.h"
#include "common/map_share.h"
#include "GlobeDef.h"

using namespace std;

#define HORI_MOTOR_LEFT_X					16
#define HORI_MOTOR_LEFT_Y					0
#define HORI_MOTOR_LEFT_START			1
#define HORI_MOTOR_LEFT_STOP			0

#define HORI_MOTOR_RIGHT_X				16
#define HORI_MOTOR_RIGHT_Y				2
#define HORI_MOTOR_RIGHT_START		1
#define HORI_MOTOR_RIGHT_STOP		0

#define DEBUG_FOOT

typedef pair<unsigned int, unsigned int> pinCtrlInfo;
typedef pair<pinCtrlInfo, pinCtrlInfo> pinCtrlDesc;
typedef pair<unsigned int, unsigned int> dotCordSerial;
typedef pair<unsigned int, unsigned int> dotCoord;
typedef pair<dotCordSerial, dotCoord> dotCoordInfo;

class PressWork
{
public:
	PressWork();
	~PressWork();

	map<unsigned int, dotCoordInfo> dotCoordMap;

	int Working();
	int GetWinDotNearest(unsigned int iPosX, unsigned int iPosY);
	int GetDotNearestX(unsigned int iPosX, bool bReversed);


private:
	ParalellControl m_objGpio;
	map<string, pinCtrlInfo> pinNameMap;
	map<unsigned int, pinCtrlDesc> pinSerialMap;
	ShareMem<sharePress> m_SharePress;

	void initPinNameMap();
	void initPinSerialMap();
	void initDotCoordlMap();
	void Release();
	bool SetValve(const char* szName, unsigned int iValue);
	bool SetValve(unsigned int iPinId, unsigned int iValue);
	bool SetValves(map<string, unsigned int> mapPinsToSet);
	bool SetValves(map<unsigned int, unsigned int> mapPinsToSet);

	void SetAllDots();

	void HMoveRStart();
	void HMoveRStop();
	void HMoveLStart();
	void HMoveLStop();
};

#endif /* PRESSWORK_H_ */
