/*
 * GlobeDef.cpp
 *
 *  Created on: 2015��7��31��
 *      Author: zzy
 */

#include <fcntl.h>
#include "GlobeDef.h"
#include "common/TraceLog.h"

bool Functions::GetCmds(string strDirDataFile)
{
	DEBUG("Data File Dir:%s!", strDirDataFile.c_str());
	DIR *dirUDisk = opendir(strDirDataFile.c_str());
	if (NULL == dirUDisk)
	{
		ERROR("Cann't open the U Disk Dir:%s!", strDirDataFile.c_str());
		return false;
	}
	struct dirent * entUDisk;
	string strName;
	int i = 0;
	while ((entUDisk = readdir(dirUDisk)) != 0)
	{
		i++;
		strName = string(entUDisk->d_name);
		if (strName.find(".fmd") != string::npos)
		{
			DEBUG("Found fmd File:%s!", strName.c_str());
			break;
		}
		else
		{
			DEBUG("Found File:%s!", strName.c_str());
		}
	}
	if (strName.size() > sizeof(".fmd") - 1)
	{
		string strPath = strDirDataFile + strName;
		if (ParseFile(strPath) == true)
		{
			return true;
		}
	}
	else
	{
		ERROR("Cann't find fmd file[%d]!", i);
	}
	return false;
}

bool Functions::ParseFile(string strFileName)
{
	string strCmBuf;
	char szBuf[1024];
	int iHandler = open(strFileName.c_str(), O_RDONLY);
	int iRtn;
	if (iHandler > 0)
	{
		while ((iRtn = read(iHandler, szBuf, 1024)) > 0)
		{
			if (iRtn < 0)
			{
				ERROR("Read File Error:%s!", strFileName.c_str());
				return false;
			}
			else
			{
				strCmBuf.append(szBuf, iRtn);
			}
		}
		close(iHandler);

	}
	else
	{
		ERROR("Open File Error:%s!", strFileName.c_str());
		return false;
	}
	if (false == ParseContent(strCmBuf))
	{
		return false;
	}
	return true;
}

bool Functions::ParseContent(string strCmdBuf)
{
	int iKeyLen = sizeof("MASGDAT:<VER:0.1><INTER:300>{<x,y,p,x,y,p>}");
	int iPosStart, iPosEnd;
	if (strCmdBuf.size() >= iKeyLen)
	{
		if ((iPosStart = strCmdBuf.find("MASGDAT:")) != string::npos)
		{
			strCmdBuf = strCmdBuf.substr(iPosStart + sizeof("MASGDAT:") - 1);
			if (((iPosStart = strCmdBuf.find("<VER:")) != string::npos)
					&& ((iPosEnd = strCmdBuf.find(">")) != string::npos))
			{
				string strVer = strCmdBuf.substr(iPosStart, iPosEnd);
				DEBUG("Ver string:%s!", strVer.c_str());
				if (sscanf(strVer.c_str(), "<VER:%f>", &m_infoFile.m_fVersion)
						> 0)
				{
					DEBUG("Got VER:%.1f!", m_infoFile.m_fVersion);
				}
				else
				{
					DEBUG("Get VER Fail!");
					return false;
				}
			}
			if (((iPosStart = strCmdBuf.find("<INTER:")) != string::npos)
					&& ((iPosEnd = strCmdBuf.find(">")) != string::npos))
			{
				string strInv = strCmdBuf.substr(iPosStart, iPosEnd);
				DEBUG("Interval string:%s!", strInv.c_str());
				if (sscanf(strInv.c_str(), "<INTER:%d>",
						&m_infoFile.m_iInterval) > 0)
				{
					DEBUG("Got Interval:%d!", m_infoFile.m_iInterval);
				}
				else
				{
					DEBUG("Get Interval Fail!");
					return false;
				}

				if (((iPosStart = strCmdBuf.find("{<")) != string::npos)
						&& ((iPosEnd = strCmdBuf.find(">}")) != string::npos))
				{
					strCmdBuf = strCmdBuf.substr(iPosStart);
					DEBUG("Cmd string[%d]:%s!", strCmdBuf.size(),
							strCmdBuf.c_str());
					if (strCmdBuf.size() > sizeof("{<x,y,p,x,y,p>}"))
					{
						if (false
								== ParseBody(strCmdBuf, m_infoFile.m_fVersion))
						{
							return false;
						}
					}
					else
					{
						DEBUG("Get Cmds Fail!");
						return false;
					}
				}
			}
		}
	}
	return true;
}

bool Functions::ParseBody(string strCmdBody, float fVer)
{
	DEBUG("VER:%.1f strCmdBody:%s!", fVer, strCmdBody.c_str());
	int iPosStart, iPosEnd;
	string strCmd, strCmdBodyTmp;
	m_infoFile.m_lstCmdWait.clear();
	if (abs(fVer - DATA_FILE_VER) < FLOAT_DEVIATION)
	{
		while (((iPosStart = strCmdBody.find("{<")) != string::npos)
				&& ((iPosEnd = strCmdBody.find(">}")) != string::npos))
		{
			strCmdBodyTmp = strCmdBody.substr(iPosStart,
					iPosEnd - iPosStart + 2);
			strCmdBody = strCmdBody.substr(iPosEnd + 1);
			strCmd = "MOT ";
			while (((iPosStart = strCmdBodyTmp.find("<")) != string::npos)
					&& ((iPosEnd = strCmdBodyTmp.find(">")) != string::npos))
			{
				DEBUG("Got:%s!",
						strCmdBodyTmp.substr(iPosStart, iPosEnd - iPosStart + 1).c_str());
				strCmd += strCmdBodyTmp.substr(iPosStart,
						iPosEnd - iPosStart + 1);
				strCmdBodyTmp = strCmdBodyTmp.substr(iPosEnd + 1);
			}
			DEBUG("Push Cmd:%s!Body:%s!", strCmd.c_str(), strCmdBody.c_str());
			m_infoFile.m_lstCmdWait.push_back(strCmd);
		}
	}
	else
	{
		ERROR("Cann't Identify Ver:%f!", fVer);
	}

	if (m_infoFile.m_lstCmdWait.size() > 0)
	{
		DEBUG("Wait Cmds List:%d!!", m_infoFile.m_lstCmdWait.size());
		return true;
	}
	DEBUG("Cmd Wait List no element!");
	return false;
}
