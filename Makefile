CXXFLAGS =	-O0 -g -Wall -fmessage-length=0  -I. -I../common/ -I../cppsocket/

FOOT_SRC =   *.cpp ./common/*.cpp   ./cppsocket/*.cpp
		
LIBS = -lutil -lm -lpthread

FOOT_TARGET =	FootMasg

all: 	$(FOOT_TARGET)
$(FOOT_TARGET):	$(FOOT_SRC)
	$(CXX) $(CXXFLAGS) $(LIBS) -o $@ $^ -lrt
	
clean:
	rm -f *.o $(FOOT_TARGET)
