/*
 * Makeup.cpp
 *
 *  Created on: 2015��5��6��
 *      Author: zzy
 */

#include "common/TraceLog.h"
#include "Makeup.h"

unsigned int Massag01Data[][4] =
{
{ 11, 10, 0 },
{ 1, 19, 0, 1 },
{ 2, 19, 170, 0 },
{ 3, 36, 170, 1 },
{ 4, 36, 0, 0 },
{ 5, 53, 0, 1 },
{ 6, 53, 170, 0 },
{ 7, 70, 170, 1 },
{ 8, 70, 0, 0 },
{ 9, 87, 0, 1 },
{ 10, 87, 170, 0 } };

//unsigned int TriDots01Data[][4] =
//{
//{ 11, 10, 0 },
//{ 1, 19, 15, 1 },
//{ 2, 19, 165, 0 },
//{ 3, 36, 165, 1 },
//{ 4, 36, 15, 0 },
//{ 5, 53, 15, 1 },
//{ 6, 53, 165, 0 },
//{ 7, 70, 165, 1 },
//{ 8, 70, 15, 0 },
//{ 9, 87, 15, 1 },
//{ 10, 87, 165, 0 } };

unsigned int TriDots01Data[][4] =
{
{ 17, 8, 0 },
{ 1, 19, 0, 1 },
{ 1, 36, 0, 1 },
{ 2, 19, 160, 0 },
{ 2, 36, 160, 0 },
{ 3, 36, 150, 1 },
{ 3, 53, 150, 1 },
{ 4, 36, 0, 0 },
{ 4, 53, 0, 0 },
{ 5, 53, 0, 1 },
{ 5, 70, 0, 1 },
{ 6, 53, 150, 0 },
{ 6, 70, 150, 0 },
{ 7, 70, 150, 1 },
{ 7, 87, 150, 1 },
{ 8, 70, 0, 0 },
{ 8, 87, 0, 0 } };

unsigned int TriDots02Data[][4] =
{
{ 11, 10, 0 },
{ 1, 19, 0, 1 },
{ 2, 19, 150, 0 },
{ 3, 36, 150, 1 },
{ 4, 36, 0, 0 },
{ 5, 53, 0, 1 },
{ 6, 53, 150, 0 },
{ 7, 70, 150, 1 },
{ 8, 70, 0, 0 },
{ 9, 87, 0, 1 },
{ 10, 87, 150, 0 } };

unsigned int MasgTriDotsData[][4] =
{
{ 26, 25, 0 },
{ 1, 19, 0, 1 },
{ 2, 19, 0, 100 },
{ 3, 20, 0, 100 },
{ 4, 19, 0, 100 },
{ 5, 19, 0, 0 },
{ 6, 36, 0, 1 },
{ 7, 36, 0, 100 },
{ 8, 37, 0, 100 },
{ 9, 36, 0, 100 },
{ 10, 36, 0, 0 },
{ 11, 53, 0, 1 },
{ 12, 53, 0, 100 },
{ 13, 54, 0, 100 },
{ 14, 53, 0, 100 },
{ 15, 53, 0, 0 },
{ 16, 70, 0, 1 },
{ 17, 70, 0, 100 },
{ 18, 71, 0, 100 },
{ 19, 70, 0, 100 },
{ 20, 70, 0, 0 },
{ 21, 87, 0, 1 },
{ 22, 87, 0, 100 },
{ 23, 88, 0, 100 },
{ 24, 87, 0, 100 },
{ 25, 87, 0, 0 } };

Makeup::Makeup()
{
	m_fParam = 1;
	m_iInterval = 300;
	initDotCoordlMap();
}

Makeup::~Makeup()
{

}

void Makeup::initDotCoordlMap()
{
	DEBUG("Makeup::initDotCoordlMap!");
	dotCordSerial pairSerialTmp;
	dotCoord pairCoordTmp;
	dotCoordInfo pairCoordInfoTmp;
	map<dotCordSerial, dotCoord>::iterator itCoords;
	int iCountDots = 0;
	int iCountLine = 0;
	int iCountColm = 0;
	for (iCountLine = 0; iCountLine < DOTS_LINES_TOTAL; iCountLine++)
	{
		if ((iCountLine % 2) == 0)
		{
			for (iCountColm = 0; iCountColm < DOTS_LINE_DOUBLE * 2;
					iCountColm++)
			{
				pairSerialTmp.first = iCountLine;
				pairSerialTmp.second = iCountColm;
				if (iCountColm < DOTS_LINE_DOUBLE)
				{
					pairCoordTmp.first = DOT_CORD_STARTX_DOUBLE
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				else
				{
					pairCoordTmp.first = DOT_CORD_STARTX_DOUBLE
							+ DOT_CORD_OFFSET_MIRROR
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				pairCoordTmp.second = DOT_CORD_STARTY
						+ DOT_CORD_STEPLENY * iCountLine;
				pairCoordInfoTmp = make_pair(pairSerialTmp, pairCoordTmp);
				DEBUG("DOT[%d]:%d %d %d %d!Line:%d Column:%d!", iCountDots,
						pairCoordInfoTmp.first.first,
						pairCoordInfoTmp.first.second,
						pairCoordInfoTmp.second.first,
						pairCoordInfoTmp.second.second, iCountLine, iCountColm);
				dotCoordMap.insert(
						pair<unsigned int, dotCoordInfo>(iCountDots,
								pairCoordInfoTmp));
				iCountDots++;
			}
		}
		else
		{
			for (iCountColm = 0; iCountColm < DOTS_LINE_SIGNLE * 2;
					iCountColm++)
			{
				pairSerialTmp.first = iCountLine;
				pairSerialTmp.second = iCountColm;
				if (iCountColm < DOTS_LINE_SIGNLE)
				{
					pairCoordTmp.first = DOT_CORD_STARTX_SINGLE
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				else
				{
					pairCoordTmp.first = DOT_CORD_STARTX_SINGLE
							+ DOT_CORD_OFFSET_MIRROR
							+ DOT_CORD_STEPLENX * iCountColm;
				}
				pairCoordTmp.second = DOT_CORD_STARTY
						+ DOT_CORD_STEPLENY * iCountLine;
				pairCoordInfoTmp = make_pair(pairSerialTmp, pairCoordTmp);
				DEBUG("DOT[%d]:%d %d %d %d!Line:%d Column:%d!", iCountDots,
						pairCoordInfoTmp.first.first,
						pairCoordInfoTmp.first.second,
						pairCoordInfoTmp.second.first,
						pairCoordInfoTmp.second.second, iCountLine, iCountColm);
				dotCoordMap.insert(
						pair<unsigned int, dotCoordInfo>(iCountDots,
								pairCoordInfoTmp));
				iCountDots++;
			}
		}
	}
#ifdef DEBUG_FOOT
	map<unsigned int, dotCoordInfo>::iterator itDot;
	for (itDot = dotCoordMap.begin(); itDot != dotCoordMap.end(); itDot++)
	{
		DEBUG("DOT[%d]:%d %d %d %d!", (*itDot).first,
				(*itDot).second.first.first, (*itDot).second.first.second,
				(*itDot).second.second.first, (*itDot).second.second.second);
	}
#endif
}

void Makeup::SetParams(float fParam, unsigned int iInterval)
{
}

bool Makeup::Make(unsigned int szData[][4])
{
	DEBUG("---Make[%d/%d]:%f %d!", (int )szData[0][0], (int )szData[0][1],
			m_fParam, m_iInterval);
	if ((int) szData[0][0] <= 0)
	{
		return false;
	}
	string strCmdTmp = "MOT ";
	int i = 0;
	int j = (int) szData[1][0];
	for (i = 1; i <= ((int) szData[0][0]); i++)
	{
		if (((int) szData[i][0]) != j)
		{
			strCmdTmp += "\n";
			m_lstCmd.push_back(strCmdTmp);
			DEBUG("Push Cmd to List:[%d/%d]%s!", i, j, strCmdTmp.c_str());
			strCmdTmp = "MOT ";
			j++;
		}
		if (((int) szData[i][0]) == j)
		{
			DEBUG("Add %d %d!%d %d!", (int )szData[i][1], (int )szData[i][2], i,
					j);
			AddCoord(strCmdTmp, szData[i][1], szData[i][2], szData[i][3]);
		}
	}
	DEBUG("Makeup :%d/%d!", i, j);
	return true;
}

bool Makeup::Add(string& strCmd, unsigned int iDotId, unsigned iValue)
{
	char szTmp[5];
	strCmd += "<";
	sprintf(szTmp, "%d", iDotId);
	strCmd += szTmp;
	strCmd += ",";
	sprintf(szTmp, "%d", iValue);
	strCmd += szTmp;
	strCmd += ">";
	return true;
}

bool Makeup::AddCoord(string& strCmd, unsigned int iDotId, unsigned iValue)
{
	map<unsigned int, dotCoordInfo>::iterator itDot;
	itDot = dotCoordMap.find(iDotId);
	if (itDot == dotCoordMap.end())
	{
		ERROR("Can't find Dot %d!", iDotId);
		return false;
	}

	char szTmp[5];
	strCmd += "<";
	sprintf(szTmp, "%d", (*itDot).second.second.first);
	strCmd += szTmp;
	strCmd += ",";
	sprintf(szTmp, "%d", (*itDot).second.second.second);
	strCmd += szTmp;
	strCmd += ",";
	sprintf(szTmp, "%d", iValue);
	strCmd += szTmp;
	strCmd += ">";
	return true;
}

bool Makeup::AddCoord(string& strCmd, unsigned int iDotX, unsigned int iDotY,
		unsigned iValue)
{
	char szTmp[10];
	strCmd += "<";
	sprintf(szTmp, "%d", iDotX);
	strCmd += szTmp;
	strCmd += ",";
	sprintf(szTmp, "%d", iDotY);
	strCmd += szTmp;
	strCmd += ",";
	sprintf(szTmp, "%d", iValue);
	strCmd += szTmp;
	strCmd += ">";
	return true;
}

bool Makeup::Massage01()
{
	DEBUG("MassageBase!");
	m_lstCmd.clear();
	if (false == DisposeModParam())
	{
		return false;
	}
	if (false == Make(Massag01Data))
	{
		return false;
	}
	m_lstCmdRecv.insert(m_lstCmdRecv.begin(), m_lstCmd.begin(), m_lstCmd.end());
	m_lstCmd.clear();
	return true;
}

bool Makeup::TriDots01()
{
	DEBUG("TriDots01!");
	m_lstCmd.clear();
	if (false == DisposeModParam())
	{
		return false;
	}
	if (false == Make(TriDots01Data))
	{
		return false;
	}
	m_lstCmdRecv.insert(m_lstCmdRecv.begin(), m_lstCmd.begin(), m_lstCmd.end());
	m_lstCmd.clear();
	return true;
}

bool Makeup::TriDots02()
{
	DEBUG("TriDots02!");
	m_lstCmd.clear();
	if (false == DisposeModParam())
	{
		return false;
	}
	if (false == Make(TriDots02Data))
	{
		return false;
	}
	m_lstCmdRecv.insert(m_lstCmdRecv.begin(), m_lstCmd.begin(), m_lstCmd.end());
	m_lstCmd.clear();
	return true;
}

bool Makeup::MasgTriDots()
{
	DEBUG("MasgTriDots!");
	m_lstCmd.clear();
	if (false == DisposeModParam())
	{
		return false;
	}
	if (false == Make(MasgTriDotsData))
	{
		return false;
	}
	m_lstCmdRecv.insert(m_lstCmdRecv.begin(), m_lstCmd.begin(), m_lstCmd.end());
	m_lstCmd.clear();
	return true;
}

bool Makeup::DisposeModParam()
{
	int iModId = -1;
	int iParam = -1;
	if ((sscanf(m_lstCmdRecv.front().c_str(), "MOD %d %d", &iModId, &iParam)
			== 2) && (iModId >= 0))
	{
		if (iParam > 1)
		{
			char szTmp[12];
			sprintf(szTmp, "MOD %d %d\n", iModId, iParam - 1);
			DEBUG("Param is %d!Reduce Cmd To:%s!", iParam, szTmp);
			if (m_lstCmdRecv.size() > 0)
			{
				m_lstCmdRecv.pop_front();
				m_lstCmdRecv.push_front(szTmp);
			}
		}
		else if (iParam == 1)
		{
			DEBUG("Param is one!");
			if (m_lstCmdRecv.size() > 0)
			{
				m_lstCmdRecv.pop_front();
			}
		}
		else if (iParam == 0)
		{
			DEBUG("Persist Mod!Only STOPALL Cmd Can Stop it!");
		}
		else
		{
			ERROR("Can't identify param:%d!", iParam);
			if (m_lstCmdRecv.size() > 0)
			{
				m_lstCmdRecv.pop_front();
			}
			return false;
		}
	}
	else if ((sscanf(m_lstCmdRecv.front().c_str(), "MOD %d", &iModId) > 0)
			&& (iModId >= 0))
	{
		DEBUG("Param Null, Run Once!");
		if (m_lstCmdRecv.size() > 0)
		{
			m_lstCmdRecv.pop_front();
		}
	}
	else
	{
		if (m_lstCmdRecv.size() > 0)
		{
			ERROR("MOD Cmd ERROR!%s!", m_lstCmdRecv.front().c_str());
			m_lstCmdRecv.pop_front();
		}
		else
		{
			ERROR("MOD Cmd/List ERROR!");
		}
	}
	return true;
}

bool Makeup::DisposeMulParam()
{
	DEBUG("DisposeMulParam:%s!", m_lstCmdRecv.front().c_str());
	string strMulHeader, strMultBody, strData;
	unsigned int iPosStart, iPosEnd;
	unsigned int iModId, iModTimes;
	int iTimes = -1;
	list<string> lstMod;
	char szModCmdTmp[12];
	if (((iPosStart = m_lstCmdRecv.front().find("Multi")) != string::npos)
			&& ((iPosEnd = m_lstCmdRecv.front().find(':')) != string::npos))
	{
		strMulHeader = m_lstCmdRecv.front().substr(iPosStart,
				iPosEnd - iPosStart);
		strMultBody = m_lstCmdRecv.front().substr(iPosEnd);
		DEBUG("Get Multi Head:%s!Body:%s!", strMulHeader.c_str(),
				strMultBody.c_str());

		if ((sscanf(strMulHeader.c_str(), "Multi %d", &iTimes) > 0)
				&& (iTimes >= 0))
		{
			DEBUG("Get Multi Times:%d!", iTimes);
			if (iTimes > 1)
			{
				string strMultiCmd;
				char szTmp[9];
				sprintf(szTmp, "Multi %d", iTimes - 1);
				strMultiCmd = szTmp;
				strMultiCmd += strMultBody;
				DEBUG("Times is %d!Reduce Cmd To:%s!", iTimes,
						strMultiCmd.c_str());
				if (m_lstCmdRecv.size() > 0)
				{
					m_lstCmdRecv.pop_front();
					m_lstCmdRecv.push_front(strMultiCmd);
				}
			}
			else if (iTimes == 1)
			{
				DEBUG("Param is one!");
				if (m_lstCmdRecv.size() > 0)
				{
					m_lstCmdRecv.pop_front();
				}
			}
			else if (iTimes == 0)
			{
				DEBUG("Persist Multi!Only STOPALL cmd can stop it!");
			}
			else
			{
				ERROR("Can't identify Multi times:%d!", iTimes);
				if (m_lstCmdRecv.size() > 0)
				{
					m_lstCmdRecv.pop_front();
				}
				return false;
			}
		}
		else
		{
			if (m_lstCmdRecv.size() > 0)
			{
				ERROR("Multi Cmd ERROR!%s!", m_lstCmdRecv.front().c_str());
				m_lstCmdRecv.pop_front();
			}
			else
			{
				ERROR("Multi Cmd/List ERROR!");
			}
			return false;
		}

		memset(szModCmdTmp, '\0', sizeof(szModCmdTmp));
		while (strMultBody.size() > 3)
		{
			if (((iPosStart = strMultBody.find('<')) != string::npos)
					&& ((iPosEnd = strMultBody.find('>')) != string::npos))
			{
				strData = strMultBody.substr(iPosStart,
						iPosEnd - iPosStart + 1);
				if (sscanf(strData.c_str(), "<%d,%d>", &iModId, &iModTimes) > 0)
				{
					DEBUG("Set Mod:%d %d!", iModId, iModTimes);
					sprintf(szModCmdTmp, "MOD %d %d", iModId, iModTimes);
					lstMod.push_back(szModCmdTmp);
					memset(szModCmdTmp, '\0', sizeof(szModCmdTmp));
				}
				else
				{
					ERROR("ScanData fail!");
				}
				DEBUG("Cmd:%s!Tmp:%s!", strMultBody.c_str(), strData.c_str());
				strMultBody = strMultBody.substr(iPosEnd + 1);
			}
			else
			{
				break;
			}
			memset(szModCmdTmp, '\0', sizeof(szModCmdTmp));
		}
		if (lstMod.size() <= 0)
		{
			ERROR("Cann't find MOD Setting!");
			return false;
		}
		m_lstCmdRecv.insert(m_lstCmdRecv.begin(), lstMod.begin(), lstMod.end());
	}
	else
	{
		ERROR("Cann't find Multi Cmd!%s!", m_lstCmdRecv.front().c_str());
		m_lstCmdRecv.pop_front();
		return false;
	}
	return true;
}

