/*
 * gpio_control.h
 *
 *  Created on: Jun 27, 2014
 *      Author: root
 */

#ifndef LCD_CONTROL_H_
#define LCD_CONTROL_H_

#include "TraceLog.h"
//#include "Channel.h"

#include <string>


using namespace std;
#ifndef FONT_MATRIX
#define  FONT_MATRIX unsigned char
#endif

typedef enum
{
	line1 = 3,
	line2 = 1,
	line3 = 7,
	line4 = 5,
	lineall = 8
}LINE_CMD;
typedef enum DATA_CMD
{
	CMD,
	DATA
}COM_STATUS;
class LCDControl
{

public:

	LCDControl();
	virtual ~LCDControl();
	int Open(const char* szDev);
	int Open();
	void Close();
	void lcd_write_matrix(LINE_CMD row,unsigned char column,FONT_MATRIX *pt,unsigned char num);
	void lcd_clear_all();
	void lcd_clear(LINE_CMD line);
	virtual void SetTimeOut(int usec);
	virtual int GetHandle() {return handle;};
protected:
	int handle;
	int timeout;
	string strDevName;
};


#endif /* GPIO_CONTROL_H_ */
