/*
 * Paralell_Control.cpp
 *
 *  Created on: Oct 27, 2014
 *      Author: root
 */
/*
 * Control.cpp
 *
 *  Created on: Jul 18, 2014
 *      Author: root
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include "gpio_control.h"
#include <unistd.h>
#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include <sys/ioctl.h>
#include "TraceLog.h"
#include "Paralell_Control.h"
#include <time.h>

/*22 allclr
 *23 hclk
 *24 hdata
 *25 vset
 *26 vdata
 *27 vclk
 #define ACLR 22
 #define HCLK 23
 #define HDATA 24
 #define VSET 25
 #define VDATA 26
 #define VCLK 27
 */

using namespace std;

ParalellControl::ParalellControl()
{
	m_iPin0 = -1;
	m_iPin1 = -1;
	m_iPin2 = -1;
	m_iPin3 = -1;
	int i;
	for (i = 0; i < 12; i++)
	{
		xval[i] = 0;
	}

}
ParalellControl::~ParalellControl()
{

}

void ParalellControl::recValues(unsigned char x, unsigned char y,
		unsigned char value)
{
	if (((int) y == 0) || ((int) y == 1))
	{
		switch ((int) x)
		{
		case 8 + 4 + 2 + 1:
			m_iPin3 = (short) value;
			m_iPin2 = (short) value;
			m_iPin1 = (short) value;
			m_iPin0 = (short) value;
			DEBUG("recValues Case 15:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 8 + 4 + 2:
			m_iPin3 = (short) value;
			m_iPin2 = (short) value;
			m_iPin1 = (short) value;
			DEBUG("recValues Case 14:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 8 + 4 + 1:
			m_iPin3 = (short) value;
			m_iPin2 = (short) value;
			m_iPin0 = (short) value;
			DEBUG("recValues Case 13:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 8 + 4:
			m_iPin3 = (short) value;
			m_iPin2 = (short) value;
			DEBUG("recValues Case 12:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 8 + 2 + 1:
			m_iPin3 = (short) value;
			m_iPin1 = (short) value;
			m_iPin0 = (short) value;
			DEBUG("recValues Case 11:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 8 + 2:
			m_iPin3 = (short) value;
			m_iPin1 = (short) value;
			DEBUG("recValues Case 10:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 8 + 1:
			m_iPin3 = (short) value;
			m_iPin0 = (short) value;
			DEBUG("recValues Case 9:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 8:
			m_iPin3 = (short) value;
			DEBUG("recValues Case 8:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 4 + 2 + 1:
			m_iPin2 = (short) value;
			m_iPin1 = (short) value;
			m_iPin0 = (short) value;
			DEBUG("recValues Case 7:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 4 + 2:
			m_iPin2 = (short) value;
			m_iPin1 = (short) value;
			DEBUG("recValues Case 6:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 4 + 1:
			m_iPin2 = (short) value;
			m_iPin0 = (short) value;
			DEBUG("recValues Case 5:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 4:
			m_iPin2 = (short) value;
			DEBUG("recValues Case 4:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 2 + 1:
			m_iPin1 = (short) value;
			m_iPin0 = (short) value;
			DEBUG("recValues Case 3:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 2:
			m_iPin1 = (short) value;
			DEBUG("recValues Case 2:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		case 1:
			m_iPin0 = (short) value;
			DEBUG("recValues Case 1:%d %d %d %d!", m_iPin0, m_iPin1, m_iPin2,
					m_iPin3);
			break;
		default:
			DEBUG("No this Case!");
			break;
		}
	}
}

int ParalellControl::SetControlValue(unsigned char x, unsigned char y,
		unsigned char value)
{
	DEBUG("---SetControlValue:%d %d %0X---", (int )x, (int )y, value);
	if ((int) x == 0)
	{
		TRACE("Can't set this, Return!");
		return 1;
	}
	int i, j;
	unsigned char xtval = 0;
	unsigned char ytval = 0;
	//gpioctrl.SetVal();
	gpioctrl.SetVal(1, ACLR);
	usleep(USLEEP_TIME);
	gpioctrl.SetVal(0, ACLR);
	usleep(USLEEP_TIME);
	gpioctrl.SetVal(1, VSET);
	if ((y > 11) || ((value != 0) && (value != 1)))
	{
		ERROR("Error , Return!");
		return 0;
	}
	if (value == 0)
	{

		xval[y] = xval[y] & (~x);
	}
	else
	{
		xval[y] = xval[y] | x;
	}

	for (i = 8; i > 0; i--)
	{
		gpioctrl.SetVal(1, HCLK); //low

		if ((xval[y] >> (i - 1)) & 0x01)
		{
			xtval = 0;
		}
		else
		{
			xtval = 1;
		}
		gpioctrl.SetVal(xtval, HDATA);
		usleep(USLEEP_TIME);
		gpioctrl.SetVal(0, HCLK);
		usleep(USLEEP_TIME);

	}
	for (i = 0; i < 2; i++)
	{
		if (i == 0)
		{
			gpioctrl.SetVal(0, VSET); //hight
			usleep(USLEEP_TIME);
			gpioctrl.SetVal(1, VSET); //low

		}
		else
		{
			gpioctrl.SetVal(1, VSET); //hight
			for (j = y + 1; j > 0; j--)
			{
				gpioctrl.SetVal(1, VCLK);
				if (j > y)
				{
					ytval = 0;
				}
				else
					ytval = 1;
				gpioctrl.SetVal(ytval, VDATA);
				usleep(USLEEP_TIME);
				gpioctrl.SetVal(0, VCLK);
				usleep(USLEEP_TIME);

			}
		}

		gpioctrl.SetVal(0, VSET); //hight
	}
	//recValues(x, y, value);
	return 1;
}
