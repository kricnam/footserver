/*
 * gpio_control.h
 *
 *  Created on: Jun 27, 2014
 *      Author: root
 */

#ifndef GPIO_CONTROL_H_
#define GPIO_CONTROL_H_

#include "TraceLog.h"
#include "Channel.h"
#include "gpio_control.h"

#include <string>


using namespace std;

class GpioControl :  public virtual Channel
{

public:

	GpioControl();
	virtual ~GpioControl();
	int Open(const char* szDev);
	void Close();
	virtual int Open(void);
	virtual int SetVal(unsigned int cmd,unsigned long pin);
	virtual bool IsOpen(void);
	virtual void SetTimeOut(int usec);
	virtual void SetOutputValue(unsigned char x,unsigned char y,unsigned char value);
	virtual int GetHandle() {return handle;};
	virtual int Read(char* buff,int len);
	virtual bool Purge(void);
	virtual int Write(const char* buff,int len);
	virtual void Lock();
	virtual void Unlock();
	int GetVal(unsigned long pin);
protected:
	int handle;
	int timeout;
	string strDevName;
};


#endif /* GPIO_CONTROL_H_ */
