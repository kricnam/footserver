/*
 * ShareMem.h
 *
 *  Created on: Aug 15, 2013
 *      Author: mxx
 */

#ifndef SHAREMEM_H_
#define SHAREMEM_H_
#include<unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>
#include <string>
#include "TraceLog.h"
using namespace std;

template<typename T> class ShareMem
{
protected:
	void* ptr;
	string strFileName;
	int nFile;
	int open(const char* szFileName, int flag, int prot)
	{
		if (nFile > 0 && strFileName == szFileName)
			return nFile;
		nFile = shm_open(szFileName, flag, S_IRUSR | S_IWUSR);
		if (nFile < 0)
		{
			ERRTRACE();
			return nFile;
		}

		if (flag & O_CREAT)
			ftruncate(nFile, sizeof(T));

		ptr = mmap(NULL, sizeof(T), prot, MAP_SHARED, nFile, 0);
		if (ptr == MAP_FAILED)
		{
			ERRTRACE();
			close(nFile);
			nFile = -1;
			ptr = NULL;
		}
		else
		{
			strFileName = szFileName;
			close(nFile);
		}
		return nFile;
	}
public:
	ShareMem()
	{
		nFile = -1;
		ptr = NULL;
	}
	;
	virtual ~ShareMem()
	{
		if (ptr && ptr != MAP_FAILED)
		{
			munmap(ptr,sizeof(T));
		}
	}
	;
	int Create(const char* szFileName)
	{
		return open(szFileName, O_CREAT | O_RDWR, PROT_READ | PROT_WRITE);
	}
	;
	int Open(const char* szFileName)
	{
		return open(szFileName, O_RDWR, PROT_READ);
	}
	int OpenRW(const char* szFileName)
	{
		return open(szFileName, O_RDWR, PROT_READ|PROT_WRITE);
	}
	;
	T& Get(void)
	{
		return *(T*)ptr;
	};
};

#endif /* SHAREMEM_H_ */
