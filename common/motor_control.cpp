/*
 * gpio_control.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: node
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include "motor_control.h"
#include <unistd.h>
#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include <sys/ioctl.h>
//#include "GlobeDef.h"

using namespace std;

MotorControl::MotorControl()
{
	handle = -1;
	timeout = 20000;
	strDevName = PWM_CTRL_DEV;
}

MotorControl::~MotorControl()
{
	if (handle > 0)
		Close();
}

bool MotorControl::IsOpen()
{
	return handle != -1;
}

void MotorControl::SetTimeOut(int usec)
{
	timeout = usec;
}

int MotorControl::Open(void)
{
	return Open(strDevName.c_str());
}

int MotorControl::Open(const char* szDev)
{
	Close();

	if (strDevName != szDev)
		strDevName = szDev;
//	TRACE("strDevName:%s", strDevName.c_str());

	handle = open(strDevName.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (handle < 0)
	{
		ERRTRACE()
		;
		return -1;
	}

	return 0;
}

void MotorControl::Close()
{
	if (handle > 0)
	{
		close(handle);
		handle = -1;
	}
}

bool MotorControl::addCtrlData(long (*data)[9])
{
	int i;
	for (i = 0; i < 10; i++)
	{
		memcpy(pwm_data.data[i], data[i], sizeof(long) * 9);
	}
	if (write(handle, &pwm_data, sizeof(_pwm_data_t)) < 0)
	{
		TRACE("write error");
	}
	return 1;
}

int MotorControl::move(unsigned short axis, unsigned short direction,
		int distPuls, int speedWidth)
{
	pwm_para.index = axis;
	pwm_para.dir = direction;
	pwm_para.pluse = distPuls;
	pwm_para.speedrm = speedWidth;
	if (write(handle, &pwm_para, sizeof(pwm_para)) < 0)
	{
		TRACE("write error");
	}
	if (ioctl(handle, axis + 3, 60) < 0)
	{
		TRACE("ioctl error");
		//ERRTRACE();
		return -1;
	}
	return 1;
}

int MotorControl::stop(unsigned short axis)
{
	int rvalue;
	rvalue = ioctl(handle, 6 + axis, 0, 0, 0, 0, 0, 0);
	if (rvalue < 0)
	{
		TRACE("ioctl error");
		ERRTRACE()
		;
		return -1;
	}
	return rvalue;

}
bool MotorControl::stopAll()
{
	int rvalue;
	rvalue = ioctl(handle, 9, 0, 0, 0, 0, 0, 0);
	if (rvalue < 0)
	{
		TRACE("ioctl error");
		ERRTRACE()
		;
		return 0;
	}
	return 1;

}

int MotorControl::runstatus(unsigned short axis)
{
	int ret;
	if ((ret = ioctl(handle, axis + 13, 60)) < 0)
	{
		TRACE("ioctl error");
		//ERRTRACE();
		return -1;
	}
	return ret;

}

int MotorControl::getCounter(unsigned short axis)
{
	int rvalue;
	rvalue = ioctl(handle, 10 + axis, 0, 0, 0, 0, 0, 0);
	if (rvalue < 0)
	{
		TRACE("ioctl error");
		ERRTRACE()
		;
		return -1;
	}
	return rvalue;
}

#ifdef _MUL_TIMER_MAIN

#define FINENAME "/dev/motor"
int main(void)
{

	MotorControl motorcontrol;

	printf("start move 1..............\n");
	motorcontrol.Open(FINENAME);
	motorcontrol.move(0,0,50,10);
	motorcontrol.move(1,0,50,10);
	motorcontrol.move(2,0,50,10);
	sleep(5);
	motorcontrol.move(0,1,50,10);
	motorcontrol.move(1,1,50,10);
	motorcontrol.move(2,1,50,10);
	// motorcontrol.move(0,1,50,10);
	while(1)
	{

		//sleep(3);
		//	stepcontrol.move(0,0,3200,40);
		//sleep(3);
	}
	return 0;
}

#endif

