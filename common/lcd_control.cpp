/*
 * gpio_control.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: node
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include "lcd_control.h"
#include <unistd.h>
#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include <sys/ioctl.h>
#include "TraceLog.h"

using namespace std;

LCDControl::LCDControl()
{
	handle = -1;
	timeout = 20000;
}

LCDControl::~LCDControl()
{
	if (handle > 0)
		Close();
}



void LCDControl::SetTimeOut(int usec)
{
	timeout = usec;
}

int LCDControl::Open(void)
{
	return Open(strDevName.c_str());
}

int LCDControl::Open(const char* szDev)
{
	//Close();

	if (strDevName != szDev)
		strDevName = szDev;
//	TRACE("strDevName:%s", strDevName.c_str());

	handle = open(strDevName.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (handle < 0)
	{
		ERRTRACE();
		return -1;
	}

	return 0;
}


void LCDControl::Close()
{
	if (handle > 0)
	{
		close(handle);
		handle = -1;
	}
}


void LCDControl::lcd_write_matrix(LINE_CMD row,unsigned char column,FONT_MATRIX *pt,unsigned char num)
{
	DEBUG("lcd_write_matrix:%d %d %d!", row, column, num);
	DUMP((const char* )pt, (int )(num * 2));
	unsigned char tempbuf[2];
	tempbuf[0] = row;
	tempbuf[1] = column;
	if (write(handle, &tempbuf, 2) < 0)
	{
		ERROR("write error");
	}
	if (write(handle, pt, num * 2))
	{
		ERROR("write error");
	}
	//  sleep(1);
	if (ioctl(handle, num, 60) < 0)
	{
		ERROR("ioctl error");
		//ERRTRACE();
	}
}
void LCDControl::lcd_clear(LINE_CMD line)
{
	if(ioctl(handle,5,line)<0){
				TRACE("ioctl error");
				//ERRTRACE();
		}
}
void LCDControl::lcd_clear_all()
{
	if(ioctl(handle,3,34)<0){
				TRACE("ioctl error");
				//ERRTRACE();
		}
}

//#define _MUL_TIMER_MAIN

#ifdef _MUL_TIMER_MAIN

#define FINENAME "/dev/lcd"
const FONT_MATRIX being_zheng[] = {
0x00,0x40,0x4F,0x40,0x40,0x40,0x7F,0x42,0x42,0xC2,0x42,0x00,
0x10,0x10,0xF0,0x10,0x10,0x10,0xF0,0x10,0x10,0x10,0x10,0x10
};

int main(void)
{
	SETTRACELEVEL(0);
	LCDControl lcdcontrol;
	lcdcontrol.Open(FINENAME);
	lcdcontrol.lcd_write_matrix(line2,12,(FONT_MATRIX *)being_zheng,12);
    while(1)
    {


    //	stepcontrol.move(0,0,3200,40);
    	//sleep(3);
    }
    return 0;
}

#endif

