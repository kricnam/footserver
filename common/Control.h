/*
 * Control.h
 *
 *  Created on: Jul 18, 2014
 *      Author: root
 */

#ifndef CONTROL_H_
#define CONTROL_H_

#include "TraceLog.h"
#include "Channel.h"
#include "gpio_control.h"

#include <string>

#define CONTROL_STATUS_NB 4

using namespace std;

class Control
{

public:
	Control();
	GpioControl gpioctrl;
	virtual void OpenContrldev(string devname);
	//virtual void SetContrdevName();
	virtual	~Control();
	virtual int SetControlValue(unsigned char x,unsigned char y,unsigned char value)=0;
protected:


};



#endif /* CONTROL_H_ */
