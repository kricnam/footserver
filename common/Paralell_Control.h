/*
 * Paralell_Control.h
 *
 *  Created on: Oct 27, 2014
 *      Author: root
 */

/*22 allclr
 *23 hclk
 *24 hdata
 *25 vset
 *26 vdata
 *27 vclk
 */

#ifndef PARALELL_CONTROL_H_
#define PARALELL_CONTROL_H_

#include <string>
#include "Control.h"

#define GPIO_OPEN 1
#define GPIO_CLOSE 0

#define ACLR 22
#define HCLK 31
#define HDATA 29
#define VSET 25
#define VDATA 27
#define VCLK 26

using namespace std;

#define USLEEP_TIME 3

class ParalellControl:public virtual Control
{

public:
	ParalellControl();
	virtual	~ParalellControl();
	virtual int SetControlValue(unsigned char x,unsigned char y,unsigned char value);
	void recValues(unsigned char x,unsigned char y,unsigned char value);
	short m_iPin0;
	short m_iPin1;
	short m_iPin2;
	short m_iPin3;
protected:
	string devname;
	unsigned char xval[12];

};



#endif /* PARALELL_CONTROL_H_ */
