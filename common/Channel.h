/*
 * Channel.h
 *
 *      Author: mxx
 */

#ifndef CHANNEL_H_
#define CHANNEL_H_
using namespace std;
#include <exception>
#include <string>
#include <errno.h>
#include <sys/time.h>
#include <string.h>
#include <string>



class ChannelException:public exception
{
public:
	ChannelException() {err=0;bUnConnected=true;};
	ChannelException(int errNo) {err=errNo;bUnConnected=false;};
	virtual ~ChannelException() throw(){};
	virtual const char* what()
			{ return (bUnConnected)?"UnConnected":strerror_r(err,cache,128);};

	int err;
	bool bUnConnected;
	char cache[128];
};

class Channel
{
public:
	Channel();
	virtual ~Channel();
	//Open a communication channel
	//return: 0->OK, -1->ERROR
	virtual int Open()=0;
	virtual void Close()=0;
	virtual bool Purge(void)=0;
	virtual int Read(char* buff,int len)=0;
	virtual int Write(const char* buff,int len)=0;
	virtual void Lock()=0;
	virtual void Unlock()=0;
	virtual void SetTimeOut(int usec)=0;
	virtual bool IsOpen()=0;
	virtual int  GetHandle()=0;
	virtual void SetReadOnly(bool enRead) { bReadOnly = enRead;};
	struct timeval & GetActiveTime(){ return tmLastAction;};
	string m_strLastError;
protected:
	struct timeval tmLastAction;
	bool bReadOnly;
	unsigned char xaddress;
	unsigned char yaddress;
};


#endif /* CHANNEL_H_ */
