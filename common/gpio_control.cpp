/*
 * gpio_control.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: node
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include "gpio_control.h"
#include <unistd.h>
#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include <sys/ioctl.h>
#include "TraceLog.h"

using namespace std;

GpioControl::GpioControl()
{
	handle = -1;
	timeout = 20000;
}

GpioControl::~GpioControl()
{
	if (handle > 0)
		Close();
}

bool GpioControl::IsOpen()
{
	return handle != -1;
}

void GpioControl::SetTimeOut(int usec)
{
	timeout = usec;
}

int GpioControl::Open(void)
{
	return Open(strDevName.c_str());
}

int GpioControl::Open(const char* szDev)
{
	Close();

	if (strDevName != szDev)
		strDevName = szDev;
//	TRACE("strDevName:%s", strDevName.c_str());

	handle = open(strDevName.c_str(),
			(bReadOnly) ? O_RDONLY : O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (handle < 0)
	{
		ERRTRACE()
		;
		return -1;
	}

	return 0;
}

void GpioControl::Close()
{
	if (handle > 0)
	{
		close(handle);
		handle = -1;
	}
}

int GpioControl::SetVal(unsigned int cmd, unsigned long pin)
{
	//TRACE("%s",buf);
	//return Write(buf, strlen(buf));

	if (ioctl(handle, cmd, pin) < 0)
	{
		ERROR("ioctl error");
		ERRTRACE()
		;
		return -1;
	}
	return cmd;
}
void GpioControl::SetOutputValue(unsigned char x, unsigned char y,
		unsigned char value)
{
	unsigned long abs;
	xaddress = x;
	yaddress = y;
	abs = xaddress * 10 + yaddress;
	SetVal(value, abs);
}

int GpioControl::Read(char* buff, int len)
{
	return 0;
}

bool GpioControl::Purge(void)
{
	return true;
}

int GpioControl::Write(const char* buff, int len)
{
	return 0;
}

void GpioControl::Lock()
{

}

void GpioControl::Unlock()
{

}

int GpioControl::GetVal(unsigned long pin)
{
	int rvalue;
	rvalue = ioctl(handle, 0, pin);
	if (rvalue < 0)
	{
		ERROR("ioctl error");
		ERRTRACE()
		;
		return -1;
	}
	//	TRACE("rvalue:%d!", rvalue);
	return rvalue;
}
