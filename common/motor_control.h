/*
 * gpio_control.h
 *
 *  Created on: Jun 27, 2014
 *      Author: root
 */

#ifndef MOTOR_CONTROL_H_
#define MOTRO_CONTROL_H_

#include "Channel.h"
#include "TraceLog.h"

#include <string>

#define PWM_CTRL_DEV "/dev/motor"

struct list_head
{
    struct list_head *next, *prev;
};
typedef struct _pwm_data
{
   long data[10][9];
   struct list_head list;
}_pwm_data_t;

typedef struct _pwm_para
{
	unsigned int index;
	unsigned int dir;
	unsigned int pluse;
	unsigned int speedrm;

} _pwm_para_t;
using namespace std;

class MotorControl
{

public:

	MotorControl();
	virtual ~MotorControl();
	int Open(const char* szDev);
	void Close();
	virtual int Open(void);
	virtual int move(unsigned short axis, unsigned short direction,
			int distPuls, int speedWidth);

	/*閸欏倹鏆熺拠瀛樻閿涳拷	axis --- 閻㈠灚婧�紓鏍у娇閿涘苯顩ч敍锟芥稉绡ㄦ潪杈剧礉1娑撶鏉炶揪绱�娑撶鏉烇拷	*/
	virtual int stop(unsigned short axis);
	virtual bool stopAll();
	virtual int runstatus(unsigned short axis);
	virtual int getCounter(unsigned short axis);
	virtual bool IsOpen(void);
	virtual void SetTimeOut(int usec);
	bool addCtrlData(long (*data)[9]);
	virtual int GetHandle()
	{
		return handle;
	}
	;
protected:
	int handle;
	int timeout;
	_pwm_para_t pwm_para;
	string strDevName;
	_pwm_data_t pwm_data;
};

#endif /* GPIO_CONTROL_H_ */
