/*
 * SerialPort.cpp
 *
 *  Created on: 2011-02-15
 *      Author: mxx
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include "SerialPort.h"
#include <unistd.h>
#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include "termios.h"
#include "TraceLog.h"

using namespace std;

SerialPort::SerialPort()
{
	handle = -1;
	timeout = 30000;
	baud = B9600;
}

SerialPort::~SerialPort()
{
	if (handle > 0)
		Close();
}

bool SerialPort::IsOpen()
{
	return handle != -1;
}

const char* SerialPort::GetPort(void)
{
	return strDevName.c_str();
}

bool SerialPort::Purge(void)
{

	if (!IsOpen())
		return false;
	char buf[4096];
	//FIXME: possible dead lock
	while (read(handle, buf, 4096) == 4096)
		;
	return true;
}

void SerialPort::SetTimeOut(int usec)
{
	timeout = usec;
}

int SerialPort::Open(void)
{
	return Open(strDevName.c_str());
}

int SerialPort::Open(const char* szDev)
{
	Close();

	if (strDevName != szDev)
		strDevName = szDev;
//	TRACE("strDevName:%s", strDevName.c_str());

	handle = open(strDevName.c_str(), (bReadOnly)?O_RDONLY:O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (handle < 0)
	{
		ERRTRACE();
		return -1;
	}
	DEBUG("Open Com %s Success!", szDev);
	SetCom();

	return 0;
}

void SerialPort::SetBaud(int n)
{
	DEBUG("SetBaud:%d!", n);
	switch (n)
	{
	case 1200:
		baud = B1200;
		break;
	case 2400:
		baud = B2400;
		break;
	case 4800:
		baud = B4800;
		break;
	case 9600:
		baud = B9600;
		break;
	case 19200:
		baud = B19200;
		break;
	case 38400:
		baud = B38400;
		break;
	case 57600:
		baud = B57600;
		break;
	case 115200:
		baud = B115200;
		break;
	case 230400:
		baud = B230400;
		break;
	default:
		printf("unsupported rate %d!\n", n);
	}
//	TRACE("SetBaud %d!baud=%d!", n, baud);
}

void SerialPort::SetCom(void)
{
//	TRACE("port.baud=%d!", baud);
	if (handle < 0)
		return;

	struct termios newtio;

	tcgetattr(handle, &newtio); /* save current port settings */
	/* set new port settings for canonical input processing */
	newtio.c_cflag = 0;
	newtio.c_cflag &= ~(CSIZE | PARENB);
	//	newtio.c_cflag |= B115200 | CS8 | CLOCAL | CREAD;
	newtio.c_cflag |= baud | CS8 | CLOCAL | CREAD;

	newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | IEXTEN);

	newtio.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR
			| ICRNL | IXON);

	newtio.c_oflag &= ~OPOST;

	/* set input mode (non-canonical, no echo,...) */
	newtio.c_cc[VMIN] = 0;
	newtio.c_cc[VTIME] = 0;

	int n = tcsetattr(handle, TCSANOW, &newtio);
	if (n < 0)
		ERRTRACE();

	tcflush(handle, TCIFLUSH);
}

void SerialPort::Lock(void)
{
	if (flock(handle, LOCK_EX) < 0)
	{
		ERRTRACE();
	}
}

void SerialPort::Close()
{
	if (handle > 0)
	{
		tcflush(handle, TCIOFLUSH);
		close(handle);
		handle = -1;
	}
}

void SerialPort::Unlock(void)
{
	if (flock(handle, LOCK_UN) < 0)
		ERRTRACE();
}

int SerialPort::Read(char* buf, int len)
{
	if (len == 0)
		return 0;
	if (handle == -1)
		Open(strDevName.c_str());
    int try_again = 3;
	int n;
	do
	{
		n = read(handle, buf, len);
		//DEBUG("read %d bytes!", n);
		if (n > 0)
			return n;
		if (n == -1)
		{
			ERRTRACE();
			Close();
			return 0;
		}
		if (try_again--)
		{
			usleep(timeout);
			continue;
		}
	} while (try_again);
	return n;
}

int SerialPort::Write(const char* buf)
{
	TRACE("%s",buf);
	return Write(buf, strlen(buf));
}

int SerialPort::Write(const char* buf, int len)
{
	if (len == 0)
		return 0;
	if (handle == -1)
		Open(strDevName.c_str());

	//	DUMP(buf,len);

	int n = write(handle, buf, len);
	tcdrain(handle);
	if (n > 0)
		return n;
	if (n == -1)
	{
		ERRTRACE();
		Close();
	}
	return 0;
}
