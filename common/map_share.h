#ifndef MAP_SHARE_DATA_H_
#define MAP_SHARE_DATA_H_

struct sharePress
{
	bool bWork;
	unsigned short iMod;
	bool bRelease;
	bool bFreq;
	unsigned int iIntervalMSecs;
	unsigned char szValveMap[25][2];
	bool bHMotorCtrl;
	unsigned char szMotorStatus; // 0 --- left start 	1 --- left stop 	2 --- right start 	3 --- right stop   4 --- run left with time 5 --- run right with time
	unsigned int iRunMSecs;
};

struct shareFootor
{
	bool bYLeftLimit;
	bool bYRightLimit;
	unsigned short m_iKeyValue;
};

struct shareDevLstn
{
	char szDevInf[256];
	bool bDiskInsert;
	char szFormat[16];
};

#endif /* MAP_SHARE_DATA_H_ */
