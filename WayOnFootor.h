/*
 * WayOnFootor.h
 *
 *  Created on: 2015��7��16��
 *      Author: zzy
 */

#ifndef WAYONFOOTOR_H_
#define WAYONFOOTOR_H_

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>
#include <list>
#include <ifaddrs.h>
#include "common/motor_control.h"
#include "common/Paralell_Control.h"
#include "common/lcd_control.h"
#include "PressWork.h"
#include "Makeup.h"
//#include "PosLimit.h"
#include "DevListener.h"
#include "GlobeDef.h"

using namespace std;

#define MAP_SHARE_FOOTOR 	"/FootorShare"

#define CMD_KEY_STOP 	"STOPALL"
#define CMD_KEY_RESET 	"RESET"
#define CMD_KEY_MOD 	"MOD "
#define CMD_KEY_MOT 	"MOT "
#define CMD_KEY_MOVD 	"MOVD "
#define CMD_KEY_MOVT 	"MOVT "
#define CMD_KEY_GPIO 	"GPIO "
#define CMD_KEY_INTV 	"INTERVAL"
#define CMD_KEY_STAT 	"STATUS"
#define CMD_KEY_HBeat 	"HBeat"
#define CMD_KEY_MULTI 	"Multi"
#define CMD_KEY_FILE 		"FILE:"
#define CMD_KEY_LEFT 		"LEFT "
#define CMD_KEY_RIGHT 		"RIGHT "
#define CMD_KEY_RELEASE 	"RELEASE"
#define CMD_KEY_AREA 	"AREA"
#define CMD_KEY_MOR 	"MOR "

#define CMD_KEY_HLSR 	"HMoveLStart"
#define CMD_KEY_HLSP 	"HMoveLStop"
#define CMD_KEY_HRSR 	"HMoveRStart"
#define CMD_KEY_HRSP 	"HMoveRStop"
#define CMD_KEY_SLEEPM 	"SLEEPM"

#define CMD_SERIAL_STOP 		0
#define CMD_SERIAL_RESET 	1
#define CMD_SERIAL_MULTI 	2
#define CMD_SERIAL_MOD 		3
#define CMD_SERIAL_MOT 		4
#define CMD_SERIAL_INTV 		5
#define CMD_SERIAL_STAT 		6
#define CMD_SERIAL_HBeat 	7
#define CMD_SERIAL_FILE 		8
#define CMD_SERIAL_LEFT 		9
#define CMD_SERIAL_RIGHT 	10
#define CMD_SERIAL_GPIO 		11
#define CMD_SERIAL_RELEASE 		12
#define CMD_SERIAL_MOVD 	13
#define CMD_SERIAL_AREA 		14
#define CMD_SERIAL_MOR 		15

#define CMD_SERIAL_HLSR 		16
#define CMD_SERIAL_HLSP 		17
#define CMD_SERIAL_HRSR 		18
#define CMD_SERIAL_HRSP 		19

#define CMD_SERIAL_MOVT 	20

#define CMD_SERIAL_SLEEPM	21

#define MOD_ID_0 	0
#define MOD_ID_1 	1
#define MOD_ID_2 	2
#define MOD_ID_3		3
#define MOD_ID_4 	4
#define MOD_ID_5 	5

#define DISP_LINE_0		0
#define DISP_LINE_1		1
#define DISP_LINE_2		2
#define DISP_LINE_3		3

#define SIZE_OF_KEY_MENU 	16
#define CURSOR_START				0
#define MENU_START				24

#define INTERVAL_TIME_MIN		30

#define DISPLAY_DEBUG
#ifdef DISPLAY_DEBUG
const FONT_MATRIX ZHENG_HZ[] =
{ 0x00, 0x40, 0x4F, 0x40, 0x40, 0x40, 0x7F, 0x42, 0x42, 0xC2, 0x42, 0x00, 0x10,
		0x10, 0xF0, 0x10, 0x10, 0x10, 0xF0, 0x10, 0x10, 0x10, 0x10, 0x10 };
#endif

typedef struct
{
	unsigned char chKeyIndex;
	unsigned char chDnState;
	unsigned char chUpState;
	unsigned char chCfState;
	int (*Operation)();
} KbdTabStruct;

class Display
{
public:
	Display();
	~Display();
	static LCDControl m_objLcd;
	unsigned char m_chKeyIndex;
	string m_strCmdBuff;

	bool repaint(unsigned short iKey);
	static int InitDisplay();
	static int Process0_1();
	static int Process1_0();
	static int Process1_1();
	static int Process1_2();
	static int Process1_3();
	static int Process2_0();
	static int Process2_1();
	static int Process2_2();
	static int Process2_3();
	static int Process3_0();
	static int Process3_1();
	static int Process3_2();
	static int Process4_0();
	static int Process4_1();
	static int Process4_2();
	bool intiDev();
	bool addCmdStr(unsigned char chIndex);
	static unsigned int getLineSerial(unsigned int iLine);
	bool initMenu();
	static bool setCursor(unsigned int iLine);
	static bool Write(unsigned int iLine, unsigned int iStartPos,
			unsigned char* szData, unsigned char chLen);
};

class PosLmtStat
{
public:
	int iValueYLeft;
	int iValueYRight;
};

typedef pair<string, unsigned int> CmdSerial;

class WayOnFootor
{
public:
	WayOnFootor();
	~WayOnFootor();

	static int m_iPosLmtHandler;
	static int iPosLmtYLeft;
	static int iPosLmtYRight;
	static int m_posSysY;
	static int m_posActX;
	static int m_posActY;
	static int m_offsetDotX;
	static MotorControl m_pwmCtrl;
	Display m_objDisplay;

	Makeup m_makeUp;
	list<string> m_lstInfoFBack;
	map<string, unsigned int> m_mapCmdBook;
	ShareMem<shareFootor> m_bufShareFootor;

	bool BuildChildProcess();
	bool Working();
	bool LocateOrigine();

private:
	string m_strLastCmd;
	unsigned int m_intervalMSecs;
	timeval m_timeTag, timeEnd;
	unsigned int m_statusSYS;
	string m_strLocalIpWlan;
	string m_strLocalIpEth;
	timeval m_timeRefresh;
	Functions m_fuctions;

	pid_t m_pidMasgPress;
	pid_t m_pidMainWork;
	pid_t m_pidDevLstn;

	PressWork m_pressWork;
	DevListener m_lstnDev;
	ShareMem<sharePress> m_bufSharePress;
	ShareMem<shareDevLstn> m_bufShareLstn;

	int checkChildPid(pid_t pidWait);
	bool InitPosLmtDev();
	void checkKey();
	void InitCmdBook();
	int GetCmdId(string strCmd);
	bool ExcuMot(string strCmd);
	bool ExcuMotRelative(string strCmd);
	bool ExcuMovT(string strCmd);
	bool ExcuMovDot(string strCmd);
	bool TestGPIO(string strCmd);
	bool ExcuMotCmd(string strCmd);
	bool ExcuMod(string strCmd);
	bool ExcuMulti(string strCmd);
	bool ExcuFile(string strCmd);
	bool ExcuLeft(string strCmd);
	bool ExcuRight(string strCmd);
	bool SleepM(string strCmd);
	void ResetInterval(string strCmd);
	double timeDiff(timeval tEnd, timeval tStart);
	string TrimSpace(string & str);
	bool getLocalIP();
	bool RefreshIP();
	int ReLocateX(unsigned char& iDotID, int iPositionX);
	int GetWinDotNearest(unsigned int iPosX, unsigned int iPosY);
	int GetDotNearestX(unsigned int iPosX, bool bReversed);
};

#endif /* WAYONFOOTOR_H_ */
