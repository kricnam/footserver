/*
 * FootServer.cpp
 *
 *  Created on: 2015��7��16��
 *      Author: zzy
 */

#include <signal.h>
#include <iostream>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "cppsocket/tcpserver.h"
#include "cppsocket/tcpclient.h"
#include "common/TraceLog.h"
#include "WayOnFootor.h"

#define THIS_SERVER_PORT 6666

using namespace std;
using namespace CPPSocket;

typedef struct
{
	TCPConnection* pClient;
} ConnClient;

class FootServer
{
public:
	FootServer();
	~FootServer();

	void MainCycle();
	void StartServer();
	int SetWaitFD();
	void CheckFD(int max_handle);
	void ReadRemote();
	void ConnectProcess();
	void ReadFromConns();
	void RemoveDeadConns();
	void Handle();

	TCPServer tcpServer;
	TCPConnection* m_pClient;
	WayOnFootor m_footor;

private:

	fd_set setRead;
	fd_set setWrite;

	short m_iTestTimes;
	short m_iDelaySecs;
};

int main()
{
	SETTRACELEVEL(2);
	signal(SIGPIPE, SIG_IGN);
	FootServer footServer;

	pid_t pidServer;
	do
	{
		pidServer = fork();
		if (pidServer < 0)
		{
			ERRTRACE()
			;
			return 1;
		}

		if (pidServer)
		{
			if (waitpid(pidServer, NULL, 0) < 0)
				ERRTRACE()
			;

			ERROR("FootServer Exit, Restart after 30 Secs!");
			sleep(30);
		}

	} while (pidServer);
	if (false == footServer.m_footor.BuildChildProcess())
	{
		ERROR("Cann't fork child pid!Exit!");
		return -1;
	}
	if (footServer.m_footor.m_objDisplay.m_objLcd.GetHandle() > 0)
	{
		footServer.m_footor.m_objDisplay.InitDisplay();
	}
	else
	{
		ERROR("Init LCD Fail!");
	}
	footServer.StartServer();
	footServer.MainCycle();
	return 0;
}

FootServer::FootServer()
{
	time_t timeTag;
	time(&timeTag);
	DEBUG("FootServer Start:%s!", ctime(&timeTag));
	m_iTestTimes = 1;
	m_iDelaySecs = 1;
	m_pClient = new TCPConnection;
}

FootServer::~FootServer()
{
	delete m_pClient;
}

void FootServer::MainCycle()
{
	DEBUG("----------------------MainCycle----------------------");
	while (true)
	{
		CheckFD(SetWaitFD());
	}
	WARNING("Warning!Quit Main Cycle!");
}

void FootServer::StartServer()
{
	DEBUG("StartServer Port:%d!", THIS_SERVER_PORT);
	tcpServer.start(THIS_SERVER_PORT, 3);
}

int FootServer::SetWaitFD(void)
{
	int max_handle = 0;
	FD_ZERO(&setRead);
	FD_ZERO(&setWrite);

	int handle = tcpServer.getSocket();
	max_handle = max(handle, max_handle);
	FD_SET(handle, &setRead);
	try
	{
		if (m_pClient->isConnected())
		{
			int handle = m_pClient->socket;
			max_handle = max(max_handle, handle);

			FD_SET(handle, &setRead);
			FD_SET(handle, &setWrite);
		}

	} catch (Exception& ex)
	{
		ERROR("%s", ex.what());
	}

	return max_handle;
}

void FootServer::CheckFD(int max_handle)
{
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 10000;
	int ret = select(max_handle + 1, &setRead, &setWrite, NULL, &tv);
	if (ret < 0)
	{
		ERRTRACE()
		;
		return;
	}
	if (((int) tcpServer.getSocket() > 0)
			&& (FD_ISSET(tcpServer.getSocket(), &setRead)))
	{
		ConnectProcess();
	}
	ReadFromConns();
	try
	{
		if ((true == m_pClient->isConnected())
				&& (m_footor.m_lstInfoFBack.size() > 0))
		{
			DEBUG("Try to Send:%s!", m_footor.m_lstInfoFBack.front().c_str());
			(*m_pClient) << m_footor.m_lstInfoFBack.front();
			m_footor.m_lstInfoFBack.pop_front();
		}
	} catch (Exception& ex)
	{
		ERROR("%s", ex.what());
		if (true == m_pClient->isConnected())
		{
			m_pClient->disconnect();
		}
	}
	Handle();
}

void FootServer::ConnectProcess(void)
{
	TRACE("ConnectProcess!");
	Address address;
	TCPConnection* pConn = NULL;
	try
	{
		pConn = tcpServer.getConnection(address);
		if (pConn)
		{
			INFO("Get Client %s,%u", address.getIPString().c_str(),
					address.getPort());
			m_pClient = pConn;
			pConn = NULL;
		}
	} catch (Exception& ex)
	{
		ERROR("%s", ex.what());
	}
}

void FootServer::ReadFromConns()
{
	int handle;
	handle = m_pClient->socket;
	if (m_pClient->isConnected() && FD_ISSET(handle, &setRead))
	{
		try
		{
			string m_strCmdRecv;
			TRACE("Read From Client %s:%d",
					m_pClient->getConnectedAddress().getIPString().c_str(),
					m_pClient->getConnectedAddress().getPort());
			(*m_pClient) >> m_strCmdRecv;
			if (m_strCmdRecv.empty())
			{
				m_pClient->disconnect();
				TRACE("Connection closed!");
			}
			else
			{
				DEBUG("Read[%d]:%s!", m_strCmdRecv.size(),
						m_strCmdRecv.c_str());
				unsigned int iPos = 0;
				while (m_strCmdRecv.size() > 3)
				{
					if ((iPos = m_strCmdRecv.find("\n")) != string::npos)
					{
						DEBUG("Pick Cmd %d Bytes [Total:%d]:%s!Resolve:%s!",
								m_strCmdRecv.size(),
								m_footor.m_makeUp.m_lstCmdRecv.size(),
								m_strCmdRecv.substr(0, iPos + 1).c_str(),
								m_strCmdRecv.substr(iPos + 1).c_str());
						m_footor.m_makeUp.m_lstCmdRecv.push_back(
								m_strCmdRecv.substr(0, iPos + 1));
						m_strCmdRecv = m_strCmdRecv.substr(iPos + 1);
					}
					else
					{
						m_footor.m_makeUp.m_lstCmdRecv.push_back(m_strCmdRecv);
						DEBUG("Got Cmd Line[%d Bytes Total:%d]:%s!",
								m_strCmdRecv.size(),
								m_footor.m_makeUp.m_lstCmdRecv.size(),
								m_footor.m_makeUp.m_lstCmdRecv.back().c_str());
						break;
					}
				}
				m_strCmdRecv.clear();
			}
		} catch (Exception &ex)
		{
			ERROR("%s", ex.what());
			m_pClient->disconnect();
		}
	}
}

void FootServer::RemoveDeadConns(void)
{
	if (!m_pClient->isConnected())
	{
		try
		{
			TRACE("remove link");
			delete m_pClient;
		} catch (Exception& ex)
		{
			ERROR("%s", ex.what());
		} catch (exception& e)
		{
			ERROR("%s", e.what());
		}
	}
}

void FootServer::Handle()
{
	m_footor.Working();
	if (false == m_footor.BuildChildProcess())
	{
		WARNING("Cann't fork child build!");
	}

#ifdef FOOT_DEBUG
	m_iTestTimes = 0;
	if (m_iTestTimes > 0)
	{
		DEBUG("m_iTestTimes:%d!", m_iTestTimes);
		m_iTestTimes--;
	}
#endif
}
