/*
 * GlobeDef.h
 *
 *  Created on: 2015��7��31��
 *      Author: zzy
 */

#ifndef GLOBEDEF_H_
#define GLOBEDEF_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <linux/netlink.h>
#include <list>
#include <dirent.h>
#include <string>

using namespace std;

#define POS_LMT_DEV 				"/dev/input/event2"
#define LCD_DEV 						"/dev/lcd"
#define PRESSWORK_DEV 			"/dev/door"
#define MAP_SHARE_PRESS 		"/PressShare"
//#define MAP_SHARE_PRESS 		"/HMotorShare"

#define POS_LMT_Y_LEFT 			256
#define POS_LMT_Y_RIGHT 		257
#define BUTTON_UP 					258
#define BUTTON_DOWN 			259
#define BUTTON_OK 					260
#define BUTTON_ESTOP 			261

#define POS_LMT_NODEF 		-1
#define POS_LMT_PRESSED 		0
#define POS_LMT_RELEASED 	1

#define SYS_POS_UNDEF 			-1
#define SYS_POS_OFFSET 			3

#define MAX_LEN_Y_TOTAL		308
#define MAX_LENGTH_Y			(MAX_LEN_Y_TOTAL - 2*SYS_POS_OFFSET - MAX_WIDTH_W)
#define MAX_LENGTH_X			15 + 20 + 20 + 20 + 20 + 15
#define MAX_WIDTH_W			100

#define WIN_WIDTH_SIDE				20
#define WIN_WIDTH_INSIDE			60
#define WIN_WIDTH_OUTSIDE			100
#define WIN_LENGTH						105
#define FOOTER_LENGTH					360
#define DIRECTION_RANGE				(((FOOTER_LENGTH - WIN_WIDTH_OUTSIDE - 2*SYS_POS_OFFSET )/2) - \
((WIN_WIDTH_OUTSIDE - WIN_WIDTH_INSIDE)/2))
//todo:Need to Measure
#define DOT_CORD_STARTX_DOUBLE		15
#define DOT_CORD_STARTX_SINGLE		15
#define DOT_CORD_STEPLENX					20

#define DOT_CORD_OFFSET_MIRROR		120

#define DOT_CORD_STARTY						15
#define DOT_CORD_STEPLENY					20

//#define DOTS_LINE_DOUBLE					4
//#define DOTS_LINE_SIGNLE					4
#define DOTS_LINE_DOUBLE					2
#define DOTS_LINE_SIGNLE						2
#define DOTS_LINES_TOTAL						2
//#define DOTS_LINES_TOTAL						4
//#define DOTS_TOTAL									52

//#define DOTS_TOTAL									7

#define MOTOR_DIS_UNLMTED		65535
#define MOTOR_STD_SPEED				150	//0.1mm/s
#define MOTOR_STD_SPEED_X			10	//mm/s
#define MOTOR_INDEX						1
#define MOTOR_LEFT						0
#define MOTOR_RIGHT						1

#define MOTOR_X_LEFT					0
#define MOTOR_X_RIGHT					1
#define MOTOR_X_LSTOP					2
#define MOTOR_X_RSTOP					3
#define MOTOR_X_TIMEL					4
#define MOTOR_X_TIMER					5

#define SYS_STATUS_WAIT		0
#define SYS_STATUS_RUN			1

#define DATA_FILE_VER				0.1
#define FLOAT_DEVIATION		0.0001

#define MENU_MAIN_SELE_0	0
#define MENU_MAIN_SELE_1	1
#define MENU_MOD_SELE_0	2
#define MENU_MOD_SELE_1	3
#define MENU_MOD_SELE_2	4
#define MENU_MOD_SELE_3	5
#define MENU_STRH_SELE_0	6
#define MENU_STRH_SELE_1	7
#define MENU_STRH_SELE_2	8
#define MENU_STRH_SELE_3	9
#define MENU_MOD_EXC_0		10
#define MENU_MOD_EXC_1		11
#define MENU_MOD_EXC_2		12
#define MENU_STRH_EXC_0		13
#define MENU_STRH_EXC_1		14
#define MENU_STRH_EXC_2		15

//#define TOTAL_LINES 				3
#define IO_LINE_TRI0 				0
//#define IO_LINE_HOLD 			1
#define IO_LINE_HOLD0 			2
#define IO_LINE_TRI1 				5
#define IO_LINE_HOLD1 			7
#define IO_LINE_TOTAL 			3

#define GAS_OUTGOING 			0
#define GAS_INCOMING 			1
#define SWITCH_CLOSE 			0
#define SWITCH_OPEN 				1

#define FLAG_TO_CLOSE 			0
#define FLAG_TO_OPEN 			1
#define FLAG_TO_OPENHOLD 			1
#define FLAG_TO_HOLD 			100
#define FLAG_TO_NOHD 			101

#define IO_CTRL_COLM0 			1
#define IO_CTRL_COLM1 			2
#define IO_CTRL_COLM2 			4
#define IO_CTRL_COLM3 			8
#define IO_CTRL_COLM4 			16
#define IO_CTRL_COLM5 			32
#define IO_CTRL_COLM6 			64
#define IO_CTRL_COLM7 			128
#define IO_CTRL_COLM_ALL 	255
#define IO_CTRL_COLM_N 		(255-16)

#define SWITCH_GAS_TRI0 		"SWITCH_GAS_TRI0"
#define SWITCH_GAS_TRI1 		"SWITCH_GAS_TRI1"
#define SWITCH_GAS_TRI2 		"SWITCH_GAS_TRI2"
#define SWITCH_GAS_TRI3 		"SWITCH_GAS_TRI3"
#define SWITCH_GAS_TRI4 		"SWITCH_GAS_TRI4"
#define SWITCH_GAS_TRI5 		"SWITCH_GAS_TRI5"
#define SWITCH_GAS_TRI6 		"SWITCH_GAS_TRI6"
#define SWITCH_GAS_TRI7 		"SWITCH_GAS_TRI7"
#define SWITCH_GAS_TRI8 		"SWITCH_GAS_TRI8"
#define SWITCH_GAS_TRI9 		"SWITCH_GAS_TRI9"
#define SWITCH_GAS_TRI10 		"SWITCH_GAS_TRI10"
#define SWITCH_GAS_TRI11 		"SWITCH_GAS_TRI11"

#define SWITCH_GAS_HOLD0 	"SWITCH_GAS_HOLD0"
#define SWITCH_GAS_HOLD1 	"SWITCH_GAS_HOLD1"
#define SWITCH_GAS_HOLD2 	"SWITCH_GAS_HOLD2"
#define SWITCH_GAS_HOLD3 	"SWITCH_GAS_HOLD3"
#define SWITCH_GAS_HOLD4 	"SWITCH_GAS_HOLD4"
#define SWITCH_GAS_HOLD5 	"SWITCH_GAS_HOLD5"
#define SWITCH_GAS_HOLD6 	"SWITCH_GAS_HOLD6"
#define SWITCH_GAS_HOLD7 	"SWITCH_GAS_HOLD7"
#define SWITCH_GAS_HOLD8 	"SWITCH_GAS_HOLD8"
#define SWITCH_GAS_HOLD9 	"SWITCH_GAS_HOLD9"
#define SWITCH_GAS_HOLD10 	"SWITCH_GAS_HOLD10"
#define SWITCH_GAS_HOLD11 	"SWITCH_GAS_HOLD11"
#define SWITCH_GAS_TOTAL 	"SWITCH_GAS_TOTAL"
#define GAS_SWITCH_ALL 		"GAS_SWITCH_ALL"
#define GAS_HOLD_ALL 			"GAS_HOLD_ALL"
#define GAS_SWITCH_ALL_2 		"GAS_SWITCH_ALL_2"
#define GAS_HOLD_ALL_2 			"GAS_HOLD_ALL_2"

#define MASG_DOT_SERIAL_0 				0
#define MASG_DOT_SERIAL_1 				1
#define MASG_DOT_SERIAL_2 				2
#define MASG_DOT_SERIAL_3 				3
#define MASG_DOT_SERIAL_4 				4
#define MASG_DOT_SERIAL_5 				5
#define MASG_DOT_SERIAL_6 				6
#define MASG_DOT_SERIAL_7 				7
#define MASG_DOT_SERIAL_8 				8
#define MASG_DOT_SERIAL_9 				9
#define MASG_DOT_SERIAL_10 			10
#define MASG_DOT_SERIAL_11 			11
#define MASG_DOT_SERIAL_12 			12
#define MASG_DOT_SERIAL_13 			13
#define MASG_DOT_SERIAL_14 			14
#define MASG_DOT_SERIAL_15 			15
#define MASG_DOT_SERIAL_16 			16
#define MASG_DOT_SERIAL_17 			17
#define MASG_DOT_SERIAL_18 			18
#define MASG_DOT_SERIAL_19 			19
#define MASG_DOT_SERIAL_20 			20
#define MASG_DOT_SERIAL_21 			21
#define MASG_DOT_SERIAL_22 			22
#define MASG_DOT_SERIAL_23	 		23

class MasgFileInfo
{
public:
	float m_fVersion;
	int m_iInterval;
	list<string> m_lstCmdWait;
};

class Functions
{
public:
	bool GetCmds(string pathData);
	bool ParseFile(string strFileName);
	bool ParseContent(string strCmdBuf);
	bool ParseBody(string strCmdBody, float fVer);
	MasgFileInfo m_infoFile;
};

#endif /* GLOBEDEF_H_ */
