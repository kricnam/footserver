/*
 * WayOnFootor.cpp
 *
 *  Created on: 2015��7��16��
 *      Author: zzy
 */

#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <exception>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <linux/input.h>
#include <math.h>

#include "common/TraceLog.h"
#include "WayOnFootor.h"

int WayOnFootor::m_iPosLmtHandler;
MotorControl WayOnFootor::m_pwmCtrl;
LCDControl Display::m_objLcd;
int WayOnFootor::m_posSysY;
int WayOnFootor::m_posActY;
int WayOnFootor::m_posActX;
int WayOnFootor::m_offsetDotX;
int WayOnFootor::iPosLmtYLeft = POS_LMT_NODEF;
int WayOnFootor::iPosLmtYRight = POS_LMT_NODEF;
ShareMem<shareFootor> bufShareFootor;

KbdTabStruct KeyTab[SIZE_OF_KEY_MENU] =
{
{
MENU_MAIN_SELE_0, MENU_MAIN_SELE_1, MENU_MAIN_SELE_1, MENU_MOD_SELE_0,
		Display::InitDisplay },
{ MENU_MAIN_SELE_1, MENU_MAIN_SELE_0, MENU_MAIN_SELE_0, MENU_STRH_SELE_0,
		Display::Process0_1 },

{ MENU_MOD_SELE_0, MENU_MOD_SELE_1, MENU_MOD_SELE_3, MENU_MOD_EXC_0,
		Display::Process1_0 },
{ MENU_MOD_SELE_1, MENU_MOD_SELE_2, MENU_MOD_SELE_0, MENU_MOD_EXC_1,
		Display::Process1_1 },
{ MENU_MOD_SELE_2, MENU_MOD_SELE_3, MENU_MOD_SELE_1, MENU_MOD_EXC_2,
		Display::Process1_2 },
{ MENU_MOD_SELE_3, MENU_MOD_SELE_0, MENU_MOD_SELE_2, MENU_MAIN_SELE_0,
		Display::Process1_3 },

{ MENU_STRH_SELE_0, MENU_STRH_SELE_1, MENU_STRH_SELE_3, MENU_STRH_EXC_0,
		Display::Process2_0 },
{ MENU_STRH_SELE_1, MENU_STRH_SELE_2, MENU_STRH_SELE_0, MENU_STRH_EXC_1,
		Display::Process2_1 },
{ MENU_STRH_SELE_2, MENU_STRH_SELE_3, MENU_STRH_SELE_1, MENU_STRH_EXC_2,
		Display::Process2_2 },
{ MENU_STRH_SELE_3, MENU_STRH_SELE_0, MENU_STRH_SELE_2,
MENU_MAIN_SELE_0, Display::Process2_3 },

{ MENU_MOD_EXC_0, MENU_MOD_EXC_0, MENU_MOD_EXC_0, MENU_MOD_SELE_0,
		Display::Process3_0 },
{ MENU_MOD_EXC_1, MENU_MOD_EXC_1, MENU_MOD_EXC_1, MENU_MOD_SELE_1,
		Display::Process3_1 },
{ MENU_MOD_EXC_2, MENU_MOD_EXC_2, MENU_MOD_EXC_2, MENU_MOD_SELE_2,
		Display::Process3_2 },

{ MENU_STRH_EXC_0, MENU_STRH_EXC_0, MENU_STRH_EXC_0, MENU_STRH_SELE_0,
		Display::Process4_0 },
{ MENU_STRH_EXC_1, MENU_STRH_EXC_1, MENU_STRH_EXC_1, MENU_STRH_SELE_1,
		Display::Process4_1 },
{ MENU_STRH_EXC_2, MENU_STRH_EXC_2, MENU_STRH_EXC_2, MENU_STRH_SELE_2,
		Display::Process4_2 } };

void PosLimitHandler(int signo)
{
	DEBUG("signo:%d!", signo);
	struct input_event inputEvent;
	int iRtn = 1;
	if (WayOnFootor::m_iPosLmtHandler < 0)
	{
		ERROR("PosLmt Dev Handler Error:d!", WayOnFootor::m_iPosLmtHandler);
		return;
	}
	unsigned short iKeyValue = 0;
	while (iRtn > 0)
	{
		if ((signo == SIGIO)
				&& ((iRtn = read(WayOnFootor::m_iPosLmtHandler, &inputEvent,
						sizeof(inputEvent))) == sizeof(inputEvent))
				&& (inputEvent.type == EV_KEY))
		{
			DEBUG("signo:%d!key code:%d value:%d type:%d!", signo,
					inputEvent.code, inputEvent.value, inputEvent.type);
			if (inputEvent.value < 8)
			{
				switch (inputEvent.code)
				{
				case POS_LMT_Y_LEFT:
					DEBUG("POS_LMT_Y_LEFT");
					if (inputEvent.value == POS_LMT_PRESSED)
					{
						DEBUG("POS_LMT_PRESSED");
						WayOnFootor::iPosLmtYLeft = POS_LMT_PRESSED;
						WayOnFootor::m_pwmCtrl.stop(MOTOR_INDEX);
						WayOnFootor::m_posSysY = SYS_POS_UNDEF;
					}
					else if (inputEvent.value == POS_LMT_RELEASED)
					{
						DEBUG("POS_LMT_RELEASED");
						WayOnFootor::iPosLmtYLeft = POS_LMT_RELEASED;
					}
					else
					{
						ERROR("Got invalid key value!");
					}
					break;
				case POS_LMT_Y_RIGHT:
					DEBUG("POS_LMT_Y_RIGHT");
					if (inputEvent.value == POS_LMT_PRESSED)
					{
						DEBUG("POS_LMT_PRESSED");
						WayOnFootor::iPosLmtYRight = POS_LMT_PRESSED;
						WayOnFootor::m_pwmCtrl.stop(MOTOR_INDEX);
						WayOnFootor::m_posSysY = SYS_POS_UNDEF;
					}
					else if (inputEvent.value == POS_LMT_RELEASED)
					{
						DEBUG("POS_LMT_RELEASED");
						WayOnFootor::iPosLmtYRight = POS_LMT_RELEASED;
					}
					else
					{
						ERROR("Got invalid key value!");
					}
					break;
				case BUTTON_UP:
				case BUTTON_DOWN:
				case BUTTON_OK:
				case BUTTON_ESTOP:
					if (inputEvent.value == POS_LMT_PRESSED)
					{
						DEBUG("KEY[%d] PRESSED!", inputEvent.code);
						iKeyValue = inputEvent.code;
						if (iKeyValue == BUTTON_ESTOP)
						{
							DEBUG("ESTOP!");
							if (WayOnFootor::m_pwmCtrl.GetHandle() > 0)
							{
								WayOnFootor::m_pwmCtrl.stop(MOTOR_INDEX);
							}
							else
							{
								ERROR("PWM Ctrl unusable!");
							}
							WayOnFootor::m_posSysY = SYS_POS_UNDEF;
						}
						else if ((iKeyValue == BUTTON_UP)
								|| (iKeyValue == BUTTON_DOWN)
								|| (iKeyValue == BUTTON_OK))
						{
							bufShareFootor.Create(MAP_SHARE_FOOTOR);
							bufShareFootor.Get().m_iKeyValue = iKeyValue;
							DEBUG("MenuKey!%d!", iKeyValue);
						}
					}
					else if (inputEvent.value == POS_LMT_RELEASED)
					{
						TRACE("KEY[%d] RELEASED!!", inputEvent.code);
					}
					else
					{
						ERROR("Got invalid key value!");
					}
					break;
				default:
					DEBUG("Code no Process:%d!", inputEvent.code);
					break;
				}
			}
		}
		else
		{
			TRACE("signo:%d!", signo);
			if (iRtn > 0)
			{
				TRACE("key code:%d value:%d type:%d!", signo, inputEvent.code,
						inputEvent.value, inputEvent.type);
			}
		}
	}
	return;
}

WayOnFootor::WayOnFootor()
{
	m_statusSYS = SYS_STATUS_WAIT;
	m_pidMasgPress = -1;
	m_pidMainWork = -1;
	m_pidDevLstn = -1;
	m_intervalMSecs = 2000;
	m_posSysY = 0;
	m_posActY = m_posSysY;
	m_posActX = 0;
	m_offsetDotX = 0;
	m_timeRefresh.tv_sec = 0;
	m_timeRefresh.tv_usec = 0;
	iPosLmtYLeft = POS_LMT_NODEF;
	iPosLmtYRight = POS_LMT_NODEF;
	m_iPosLmtHandler = -1;
	gettimeofday(&m_timeTag, NULL);
	//	ForTest
	if (m_pwmCtrl.Open() < 0)
	{
		ERROR("InitDev Cann't open PWM Ctrl Dev!");
	}

	if (!m_bufSharePress.Create(MAP_SHARE_PRESS))
	{
		ERROR("Create press share data fail!%s!", MAP_SHARE_PRESS);
	}
	if (!m_bufShareLstn.Create(MAP_SHARE_LSTN))
	{
		ERROR("Create Dev listener share data fail!%s!", MAP_SHARE_LSTN);
	}
	if (!m_bufShareFootor.Create(MAP_SHARE_FOOTOR))
	{
		ERROR("Create Dev listener share data fail!%s!", MAP_SHARE_LSTN);
	}
	else
	{
		m_bufShareFootor.Get().m_iKeyValue = 0;
	}

	InitCmdBook();
	if (false == InitPosLmtDev())
	{
		ERROR("Init Pos Limit Dev fail!");
	}
	signal(SIGIO, PosLimitHandler);
}

bool WayOnFootor::InitPosLmtDev()
{
	if ((m_iPosLmtHandler = open(POS_LMT_DEV, O_RDWR)) < 0)
	{
		ERROR("Cann't open POS Limit Dev:%s!", POS_LMT_DEV);
		return false;
	}
	fcntl(m_iPosLmtHandler, F_SETOWN, getpid());
	int iFlag = fcntl(m_iPosLmtHandler, F_GETFL);
	fcntl(m_iPosLmtHandler, F_SETFL, iFlag | FASYNC);
	return true;
}

WayOnFootor::~WayOnFootor()
{
	close(m_iPosLmtHandler);
	m_pwmCtrl.Close();
}

bool WayOnFootor::LocateOrigine()
{
	m_pwmCtrl.stop(MOTOR_INDEX);
	m_statusSYS = SYS_STATUS_WAIT;
	m_bufSharePress.Get().bRelease = true;
	m_bufSharePress.Get().bWork = true;

	if (POS_LMT_PRESSED != iPosLmtYLeft)
	{
		if (m_pwmCtrl.move(MOTOR_INDEX, MOTOR_LEFT, MOTOR_DIS_UNLMTED,
		MOTOR_STD_SPEED) < 0)
		{
			ERROR("Move Motor Fail!");
			return false;
		}
	}
	timeval tTimeStart, tTimeNow;
	gettimeofday(&tTimeStart, NULL);
	int iRtn = m_pwmCtrl.runstatus(MOTOR_INDEX);
	while (iRtn > 0)
	{
		gettimeofday(&tTimeNow, NULL);
		if (timeDiff(tTimeNow, tTimeStart) > 10000)
		{
			WARNING("Time overflow!");
			return false;
		}
		usleep(100000);
		iRtn = m_pwmCtrl.runstatus(MOTOR_INDEX);
	}
	if (iRtn < 0)
	{
		ERROR("Get Motor Status fail!");
		return false;
	}
	m_pwmCtrl.move(MOTOR_INDEX, MOTOR_RIGHT, SYS_POS_OFFSET * 10,
	MOTOR_STD_SPEED);
	gettimeofday(&tTimeStart, NULL);
	iRtn = m_pwmCtrl.runstatus(MOTOR_INDEX);
	while (iRtn > 0)
	{
		gettimeofday(&tTimeNow, NULL);
		if (timeDiff(tTimeNow, tTimeStart) > 10000)
		{
			WARNING("Time overflow!");
			return false;
		}
		usleep(100000);
		iRtn = m_pwmCtrl.runstatus(MOTOR_INDEX);
	}
	if (iRtn < 0)
	{
		ERROR("Get Motor Status fail!");
		return false;
	}
	m_posSysY = 0;

	return true;
}

bool WayOnFootor::Working()
{
	timeval timeNow;
	gettimeofday(&timeNow, NULL);
	if (m_makeUp.m_lstCmdRecv.size() > 0)
	{
		string strCmd = m_makeUp.m_lstCmdRecv.front();
		TRACE("Get Cmd Line:%s!%d", strCmd.c_str(),
				m_makeUp.m_lstCmdRecv.size());
		list<string>::iterator it;
		for (it = m_makeUp.m_lstCmdRecv.begin();
				it != m_makeUp.m_lstCmdRecv.end(); it++)
		{
			if ((*it).find(CMD_KEY_STOP) != string::npos)
			{
				m_makeUp.m_lstCmdRecv.clear();
				//todo
				return 0;
			}
			else if ((*it).find(CMD_KEY_RESET) != string::npos)
			{
				m_makeUp.m_lstCmdRecv.clear();
				//todo
				if (false == LocateOrigine())
				{
					ERROR("Locate Origine fail!");
				}
				m_bufSharePress.Get().bRelease = true;
				m_bufSharePress.Get().bWork = true;
				return 0;
			}
		}
		int iCmdId = GetCmdId(strCmd);
		switch (iCmdId)
		{
		case CMD_SERIAL_STOP:
			break;
		case CMD_SERIAL_RESET:
			break;
		case CMD_SERIAL_MULTI:
			ExcuMulti(strCmd);
			break;
		case CMD_SERIAL_MOD:
			ExcuMod(strCmd);
			break;
		case CMD_SERIAL_MOR:
			if (m_strLastCmd == strCmd)
			{
				DEBUG("CMD equal with last, delete!%s %s!",
						m_strLastCmd.c_str(), strCmd.c_str());
				if (m_makeUp.m_lstCmdRecv.size() > 0)
				{
					m_makeUp.m_lstCmdRecv.pop_front();
				}
				gettimeofday(&timeEnd, NULL);
				return true;
			}
			DEBUG("Try strCmd %s!", strCmd.c_str());

			if (timeDiff(timeNow, m_timeTag) > m_intervalMSecs)
			{
				m_strLastCmd = strCmd;
				//if (ExcuMotCmd(strCmd) == true)
				if (ExcuMotRelative(strCmd) == true)
				{
					m_statusSYS = SYS_STATUS_RUN;
				}
				else
				{
					ERROR("Excut MOR Cmd Fail!%s!", strCmd.c_str());
					//m_makeUp.m_lstCmdRecv.pop_front();
				}
			}
			else
			{
				DEBUG("m_intervalMSecs:%d!timeDiff:%d!", m_intervalMSecs,
						timeDiff(timeNow, m_timeTag));
			}
			break;
		case CMD_SERIAL_MOVT:
			if (m_strLastCmd == strCmd)
			{
				DEBUG("CMD equal with last, delete!%s %s!",
						m_strLastCmd.c_str(), strCmd.c_str());
				if (m_makeUp.m_lstCmdRecv.size() > 0)
				{
					m_makeUp.m_lstCmdRecv.pop_front();
				}
				gettimeofday(&timeEnd, NULL);
				return true;
			}
			DEBUG("Try strCmd %s!", strCmd.c_str());

			if (timeDiff(timeNow, m_timeTag) > m_intervalMSecs)
			{
				m_strLastCmd = strCmd;
				//if (ExcuMotCmd(strCmd) == true)
				if (ExcuMovT(strCmd) == true)
				{
					m_statusSYS = SYS_STATUS_RUN;
				}
				else
				{
					ERROR("Excut MOR Cmd Fail!%s!", strCmd.c_str());
					//m_makeUp.m_lstCmdRecv.pop_front();
				}
			}
			else
			{
				DEBUG("m_intervalMSecs:%d!timeDiff:%d!", m_intervalMSecs,
						timeDiff(timeNow, m_timeTag));
			}
			break;
		case CMD_SERIAL_MOVD:
			if (m_strLastCmd == strCmd)
			{
				DEBUG("CMD equal with last, delete!%s %s!",
						m_strLastCmd.c_str(), strCmd.c_str());
				if (m_makeUp.m_lstCmdRecv.size() > 0)
				{
					m_makeUp.m_lstCmdRecv.pop_front();
				}
				gettimeofday(&timeEnd, NULL);
				return true;
			}
			DEBUG("Try strCmd %s!", strCmd.c_str());

			if (timeDiff(timeNow, m_timeTag) > m_intervalMSecs)
			{
				m_strLastCmd = strCmd;
				if (ExcuMovDot(strCmd) == true)
				{
					m_statusSYS = SYS_STATUS_RUN;
				}
				else
				{
					ERROR("Excut MOT Cmd Fail!%s!", strCmd.c_str());
					m_makeUp.m_lstCmdRecv.pop_front();
				}
			}
			else
			{
				DEBUG("m_intervalMSecs:%d!timeDiff:%d!", m_intervalMSecs,
						timeDiff(timeNow, m_timeTag));
			}
			break;
		case CMD_SERIAL_INTV:
			ResetInterval(strCmd);
			break;
		case CMD_SERIAL_STAT:
			break;
		case CMD_SERIAL_HBeat:
			break;
		case CMD_SERIAL_FILE:
			ExcuFile(strCmd);
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		case CMD_SERIAL_LEFT:
			ExcuLeft(strCmd);
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		case CMD_SERIAL_RIGHT:
			ExcuRight(strCmd);
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		case CMD_SERIAL_SLEEPM:
			SleepM(strCmd);
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		case CMD_SERIAL_GPIO:
			if (TestGPIO(strCmd) == false)
			{
				ERROR("Excut Test GPIO Cmd Fail!%s!", strCmd.c_str());
				m_makeUp.m_lstCmdRecv.pop_front();
			}
			break;
		case CMD_SERIAL_RELEASE:
			m_bufSharePress.Get().bRelease = true;
			m_bufSharePress.Get().bWork = true;
			if (m_makeUp.m_lstCmdRecv.size() > 0)
			{
				m_makeUp.m_lstCmdRecv.pop_front();
			}
			break;
		case CMD_SERIAL_HLSR:
			DEBUG("CMD_SERIAL_HLSR!");
			m_bufSharePress.Get().szMotorStatus = 0;
			m_bufSharePress.Get().bHMotorCtrl = true;
			if (m_makeUp.m_lstCmdRecv.size() > 0)
			{
				m_makeUp.m_lstCmdRecv.pop_front();
			}
			break;
		case CMD_SERIAL_HLSP:
			DEBUG("CMD_SERIAL_HLSP!");
			m_bufSharePress.Get().szMotorStatus = 1;
			m_bufSharePress.Get().bHMotorCtrl = true;
			if (m_makeUp.m_lstCmdRecv.size() > 0)
			{
				m_makeUp.m_lstCmdRecv.pop_front();
			}
			break;
		case CMD_SERIAL_HRSR:
			DEBUG("CMD_SERIAL_HRSR!");
			m_bufSharePress.Get().szMotorStatus = 2;
			m_bufSharePress.Get().bHMotorCtrl = true;
			if (m_makeUp.m_lstCmdRecv.size() > 0)
			{
				m_makeUp.m_lstCmdRecv.pop_front();
			}
			break;
		case CMD_SERIAL_HRSP:
			DEBUG("CMD_SERIAL_HRSP!");
			m_bufSharePress.Get().szMotorStatus = 3;
			m_bufSharePress.Get().bHMotorCtrl = true;
			if (m_makeUp.m_lstCmdRecv.size() > 0)
			{
				m_makeUp.m_lstCmdRecv.pop_front();
			}
			break;
		case CMD_SERIAL_AREA:
			unsigned int iAreaX;
			unsigned int iAreaY;
			if (sscanf(strCmd.c_str(), "%*s %d,%d", &iAreaX, &iAreaY) < 0)
			{
				string strRsp = "AREA ";
				char szTmp[10];
				sprintf(szTmp, "%d", MAX_LENGTH_X);
				strRsp.append(szTmp);
				strRsp.append(", ");
				//sprintf(szTmp, "%d", MAX_LENGTH_Y/2);
				sprintf(szTmp, "%d", DIRECTION_RANGE);

				strRsp.append(szTmp);
				strRsp += '\n';
				DEBUG("Quest AREA!m_strToSend:%s!", strRsp.c_str());
				m_lstInfoFBack.push_back(strRsp);
			}
			else
			{
				DEBUG("Set AREA!%d %d!", iAreaX, iAreaY);
				//Set current area
			}
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		default:
			WARNING("Cann't find CmdID:%d!", iCmdId);
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		}
		gettimeofday(&timeEnd, NULL);
	}
	gettimeofday(&timeNow, NULL);
	//	if ((timeDiff(timeNow, m_timeTag) > 2 * m_intervalMSecs)
	//			&& (m_statusSYS == SYS_STATUS_RUN)&&(m_makeUp.m_lstCmdRecv.size() == 0))
	//	{
	//		DEBUG("Timeout Now!%f!", timeDiff(timeNow, m_timeTag));
	//		m_statusSYS = SYS_STATUS_WAIT;
	//		m_bufSharePress.Get().bRelease = true;
	//		m_bufSharePress.Get().bWork = true;
	//	}else
	if ((timeDiff(timeNow, timeEnd) > 500) && (m_statusSYS == SYS_STATUS_RUN)
			&& (m_makeUp.m_lstCmdRecv.size() == 0))
	{
		DEBUG("Timeout End!%f!", timeDiff(timeNow, timeEnd));
		m_statusSYS = SYS_STATUS_WAIT;
		m_posActY = m_posSysY;
		m_posActX = m_offsetDotX;
		m_bufSharePress.Get().bRelease = true;
		m_bufSharePress.Get().bWork = true;
	}
	else if (m_statusSYS == SYS_STATUS_WAIT)
	{
		if (timeDiff(timeNow, m_timeRefresh) > 10000)
		{
			getLocalIP();
			gettimeofday(&m_timeRefresh, NULL);
		}
		if (strlen(m_bufShareLstn.Get().szDevInf) > strlen("/dev/"))
		{
			DEBUG("UDisk Inserted Info:%s!", m_bufShareLstn.Get().szDevInf);
			string strInfo = m_bufShareLstn.Get().szDevInf;
			int iPosStart = strInfo.find("/dev/");
			if (iPosStart != string::npos)
			{
				strInfo = strInfo.substr(iPosStart + sizeof("/dev/") - 1);
				DEBUG("UDISK Dev Name:%s!", strInfo.c_str());
				strInfo.insert(0, "/media/");
				strInfo.append("/");
				DEBUG("UDISK DIR:%s!", strInfo.c_str());
				if (false == m_fuctions.GetCmds(strInfo))
				{
					ERROR("Get UDisk Data Fail!");
				}
			}
			memset(m_bufShareLstn.Get().szDevInf, 0x0,
					sizeof(m_bufShareLstn.Get().szDevInf));
		}
	}
	checkKey();
	return 0;
}

void WayOnFootor::checkKey()
{
	unsigned short iKeyVal = m_bufShareFootor.Get().m_iKeyValue;
	m_bufShareFootor.Get().m_iKeyValue = 0;
	if ((iKeyVal == BUTTON_UP) || (iKeyVal == BUTTON_DOWN)
			|| (iKeyVal == BUTTON_OK))
	{
		if (true == m_objDisplay.repaint(iKeyVal))
		{
			if (iKeyVal == BUTTON_OK)
			{
				if (m_objDisplay.m_strCmdBuff.size() > 3)
				{
					m_makeUp.m_lstCmdRecv.push_back(m_objDisplay.m_strCmdBuff);
					m_objDisplay.m_strCmdBuff.clear();
				}
			}
		}
		else
		{
			ERROR("Repain fail!%d!", iKeyVal);
		}
	}
}

int WayOnFootor::GetCmdId(string strCmd)
{
	if (strCmd.size() < 3)
	{
		return false;
	}
	map<string, unsigned int>::iterator itCmd;
	for (itCmd = m_mapCmdBook.begin(); itCmd != m_mapCmdBook.end(); itCmd++)
	{
		if (strCmd.find((*itCmd).first) != string::npos)
		{
			return (*itCmd).second;
		}
	}
	return -1;
}

void WayOnFootor::InitCmdBook()
{
	CmdSerial cmdPair(CMD_KEY_STOP, CMD_SERIAL_STOP);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_RESET, CMD_SERIAL_RESET);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_MULTI, CMD_SERIAL_MULTI);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_MOD, CMD_SERIAL_MOD);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_MOT, CMD_SERIAL_MOT);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_INTV, CMD_SERIAL_INTV);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_STAT, CMD_SERIAL_STAT);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_HBeat, CMD_SERIAL_HBeat);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_FILE, CMD_SERIAL_FILE);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_LEFT, CMD_SERIAL_LEFT);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_RIGHT, CMD_SERIAL_RIGHT);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_GPIO, CMD_SERIAL_GPIO);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_RELEASE, CMD_SERIAL_RELEASE);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_MOVD, CMD_SERIAL_MOVD);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_AREA, CMD_SERIAL_AREA);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_MOR, CMD_SERIAL_MOR);
	m_mapCmdBook.insert(cmdPair);

	cmdPair = make_pair(CMD_KEY_HLSR, CMD_SERIAL_HLSR);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_HLSP, CMD_SERIAL_HLSP);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_HRSR, CMD_SERIAL_HRSR);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_HRSP, CMD_SERIAL_HRSP);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_MOVT, CMD_SERIAL_MOVT);
	m_mapCmdBook.insert(cmdPair);
	cmdPair = make_pair(CMD_KEY_SLEEPM, CMD_SERIAL_SLEEPM);
	m_mapCmdBook.insert(cmdPair);

#ifdef DEBUG_FOOT
	map<string, unsigned int>::iterator itCmd;
	for (itCmd = m_mapCmdBook.begin(); itCmd != m_mapCmdBook.end(); itCmd++)
	{
		DEBUG("%s:%d!", (*itCmd).first.c_str(), (*itCmd).second);
	}
#endif
}

bool WayOnFootor::ExcuMovDot(string strCmd)
{
	DEBUG("MovDot Cmd:%s!", strCmd.c_str());
	if (m_makeUp.m_lstCmdRecv.size() > 0)
	{
		m_makeUp.m_lstCmdRecv.pop_front();
	}
	int iPosStart = 0;
	int iPosEnd = 0;
	string strData;
	int iPositionY = 0;
	int iDotId = -1;
	unsigned int intervalMSecsToSet = 0, intervalMSecsToSetTmp = 0;
	unsigned char i = 1;
	if ((iPosStart = strCmd.find(CMD_KEY_MOVD)) != string::npos)
	{
		strCmd = strCmd.substr(iPosStart + sizeof(CMD_KEY_MOVD) - 1);
		DEBUG("Cmd:%s!", strCmd.c_str());
		while (strCmd.size() > 3)
		{
			if (((iPosStart = strCmd.find('<')) != string::npos) && ((iPosEnd =
					strCmd.find('>')) != string::npos))
			{
				strData = strCmd.substr(iPosStart, iPosEnd - iPosStart + 1);
				strData = TrimSpace(strData);
				unsigned int iPosXId = 0, iValue = 0;
				if (sscanf(strData.c_str(), "<%d,%d,%d,%d>", &iPosXId,
						&iPositionY, &iValue, &intervalMSecsToSetTmp) == 4)
				{
					if ((iPosXId < 0) || (iPosXId > MASG_DOT_SERIAL_23)
							|| (iPositionY < 0)
							|| (iPositionY > (MAX_LENGTH_Y + MAX_WIDTH_W))
							|| (iValue < FLAG_TO_CLOSE)
							|| (iValue > FLAG_TO_NOHD))
					{
						ERROR("Mot Cmd Value Error:%d %d %d!", iPosXId,
								iPositionY, iValue, intervalMSecsToSetTmp);
						return false;
					}

					if (((iValue == FLAG_TO_NOHD) || (iValue == FLAG_TO_HOLD))
							&& (intervalMSecsToSetTmp > intervalMSecsToSet))
					{
						intervalMSecsToSet = intervalMSecsToSetTmp;
					}
					DEBUG("Got Mot Value:%d %d %d %d!", iPosXId, iPositionY,
							iValue, intervalMSecsToSetTmp);

					iDotId = iPosXId;

					DEBUG("Set ShareMem:%d %d!", iDotId, iValue);
					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else if (sscanf(strData.c_str(), "<%d,%d,%d>", &iPosXId,
						&iPositionY, &iValue) == 3)
				{
					if ((iPosXId < 0) || (iPosXId > MASG_DOT_SERIAL_23)
							|| (iPositionY < 0)
							|| (iPositionY > (MAX_LENGTH_Y + MAX_WIDTH_W))
							|| (iValue < 0) || (iValue > 101))
					{
						ERROR("Mot Cmd Value Error:%d %d %d!", iPosXId,
								iPositionY, iValue);
						return false;
					}
					DEBUG("Got Mot Value:%d %d %d!", iPosXId, iPositionY,
							iValue);

					iDotId = iPosXId;

					DEBUG("Set ShareMem:%d %d!", iDotId, iValue);
					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else
				{
					ERROR("ScanData fail!");
				}
				DEBUG("Cmd:%s!Tmp:%s!", strCmd.c_str(), strData.c_str());
				strCmd = strCmd.substr(iPosEnd + 1);
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		return false;
	}
	if (i > 1)
	{
		DEBUG("Mot data Got:%d!", (unsigned int )(i) - 1);;
		float fDis;
		float fSpeed;
		map<unsigned int, dotCoordInfo>::iterator itDot;
		itDot = m_pressWork.dotCoordMap.find(iDotId);
		if (itDot == m_pressWork.dotCoordMap.end())
		{
			ERROR("Cann't Dot:%d!", iDotId);
			return false;
		}
		m_intervalMSecs = 50;
		if (iPositionY <= m_posSysY)
		{
			DEBUG("iPositionY:%d < m_posSysY:%d!", iPositionY, m_posSysY);
			fDis = m_posSysY - iPositionY;
			fSpeed = MOTOR_STD_SPEED;
			m_intervalMSecs = ((fDis * 10) / fSpeed) * 1000;
			if (m_intervalMSecs < INTERVAL_TIME_MIN)
			{
				m_intervalMSecs = INTERVAL_TIME_MIN;
				DEBUG("m_intervalMSecs is 0, reset to m_intervalMSecs=%d!",
						m_intervalMSecs);
			}
			DEBUG(
					"iPositionY[%d] < m_posSysY[%d] Distance:%.1f Speed:%.1f, m_intervalMSecs=%d!",
					iPositionY, m_posSysY, fDis, fSpeed, m_intervalMSecs);
			int iDistance = fDis * 10;
			int iSpeed = fSpeed;
			DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);
			if (iDistance > 0)
			{
				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_LEFT, iDistance, iSpeed);
			}
		}
		else if (iPositionY > m_posSysY)
		{
			DEBUG("iPositionY:%d > m_posSysY:%d!", iPositionY, m_posSysY);
			fDis = iPositionY - m_posSysY;
			fSpeed = MOTOR_STD_SPEED;
			m_intervalMSecs = ((fDis * 10) / fSpeed) * 1000;
			if (m_intervalMSecs < INTERVAL_TIME_MIN)
			{
				m_intervalMSecs = INTERVAL_TIME_MIN;
				DEBUG("m_intervalMSecs is 0, reset to 500!m_intervalMSecs=%d!",
						m_intervalMSecs);
			}
			DEBUG(
					"iPositionY[%d] > (m_posSysY + MAX_WIDTH_W)[%d] Distance:%.1f Speed:%.1f m_intervalMSecs=%d!",
					iPositionY, m_posSysY + MAX_WIDTH_W, fDis, fSpeed,
					m_intervalMSecs);
			int iDistance = fDis * 10;
			int iSpeed = fSpeed;
			DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);
			if (iDistance > 0)
			{
				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_RIGHT, iDistance, iSpeed);
			}
		}
		m_posSysY = iPositionY;

		if (intervalMSecsToSet > m_intervalMSecs)
		{
			m_intervalMSecs = intervalMSecsToSet;
			DEBUG("Reset m_intervalMSecs to Hold MSecs:%d!", m_intervalMSecs);
		}
		DEBUG("iPositionY:%d!m_posSysY:%d!m_intervalMSecs:%d!", iPositionY,
				m_posSysY, m_intervalMSecs);
		gettimeofday(&m_timeTag, NULL);

		if (m_statusSYS == SYS_STATUS_WAIT)
		{
			usleep((m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			DEBUG("SYS_STATUS_WAIT usleeped time:%d!",
					(m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			usleep(INTERVAL_TIME_MIN * 1000);
		}
		else
		{
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			usleep((m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			DEBUG("usleeped time:%d!",
					(m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);

			//usleep(INTERVAL_TIME_MIN * 1000);
		}
	}
	return true;
}

//CMD: DotId DValue XDistance YDistance
bool WayOnFootor::ExcuMovT(string strCmd)
{
	DEBUG("ExcuMovT Cmd:%s!", strCmd.c_str());
	if (m_makeUp.m_lstCmdRecv.size() > 0)
	{
		m_makeUp.m_lstCmdRecv.pop_front();
	}
	int iPosStart = 0;
	int iPosEnd = 0;
	string strData;
	int iDValue = 0;
	//int iDotId = -1;
	int iDId = 0;
	unsigned int intervalMSecsToSet = 0;
	int iYDis = 0, iXDis = 0;
	unsigned char i = 1;
	if ((iPosStart = strCmd.find(CMD_KEY_MOVT)) != string::npos)
	{
		strCmd = strCmd.substr(iPosStart + sizeof(CMD_KEY_MOVD) - 1);
		DEBUG("Cmd:%s!", strCmd.c_str());

		if (((iPosStart = strCmd.find('<')) != string::npos) && ((iPosEnd =
				strCmd.find('>')) != string::npos))
		{
			strData = strCmd.substr(iPosStart, iPosEnd - iPosStart + 1);
			strData = TrimSpace(strData);

			if (sscanf(strData.c_str(), "<%d,%d,%d,%d>", &iDId, &iDValue,
					&iXDis, &iYDis) == 4)
			{
				if ((iDId < 0) || (iDValue < 0))
				{
					ERROR("MOVT Cmd Value Error:%d %d %d!", iDId, iDValue,
							iXDis, iYDis);
					return false;
				}

				DEBUG("Got MOVT Value:%d %d %d %d!", iDId, iDValue, iXDis,
						iYDis);

				i++;
			}
			else
			{
				ERROR("ScanData fail!");
			}
			DEBUG("Cmd:%s!Tmp:%s!", strCmd.c_str(), strData.c_str());
			strCmd = strCmd.substr(iPosEnd + 1);
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}
	if (i > 1)
	{
		DEBUG("MOVT data Got:%d!", (unsigned int )(i) - 1);;
		float fDis;
		float fSpeed;

		//Y
		m_intervalMSecs = 100;
		int iLimitY = MAX_LENGTH_Y + MAX_WIDTH_W;
		if (iYDis < iLimitY)
		{
			DEBUG("iYDis:%d!iLimitY:%d!", iYDis, iLimitY);

			if (iYDis > 0)
			{
				DEBUG("iYDis:%d!", iYDis);

				fSpeed = MOTOR_STD_SPEED;
				m_intervalMSecs = ((iYDis * 10) / fSpeed) * 1000;

				int iDistance = iYDis * 10;
				int iSpeed = fSpeed;
				DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);

				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_RIGHT, iDistance, iSpeed);

			}
			else if (iYDis < 0)
			{
				DEBUG("iYDis:%d!", iYDis);
				fSpeed = MOTOR_STD_SPEED;
				int iDistance = -1 * iYDis * 10;
				m_intervalMSecs = ((iDistance) / fSpeed) * 1000;

				int iSpeed = fSpeed;
				DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);
				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_LEFT, iDistance, iSpeed);
			}

			//X
			map<unsigned int, dotCoordInfo>::iterator itDot;
			itDot = m_pressWork.dotCoordMap.find(iDId);
			if (itDot == m_pressWork.dotCoordMap.end())
			{
				ERROR("Cann't Dot:%d!", iDId);
				return false;
			}
			DEBUG("Set ShareMem iDId:%d iDValue:%d!", iDId, iDValue);
			m_bufSharePress.Get().szValveMap[1][0] = (unsigned char) (iDId);
			m_bufSharePress.Get().szValveMap[1][1] = (unsigned char) (iDValue);

			int iXRunMSecs = 0;	//0.1mm  Y and X 1mm
			if (iXDis < 0)
			{
				DEBUG("iXDis < 0:%d!", iXDis);
				iXRunMSecs = -1 * iXDis * 1000 / MOTOR_STD_SPEED_X;
				m_bufSharePress.Get().szMotorStatus = MOTOR_X_TIMEL;
				m_bufSharePress.Get().iRunMSecs = iXRunMSecs;
				m_bufSharePress.Get().bHMotorCtrl = true;
			}
			else if (iXDis > 0)
			{
				DEBUG("iXDis > 0:%d!", iXDis);
				iXRunMSecs = iXDis * 1000 / MOTOR_STD_SPEED_X;
				m_bufSharePress.Get().szMotorStatus = MOTOR_X_TIMER;
				m_bufSharePress.Get().iRunMSecs = iXRunMSecs;
				m_bufSharePress.Get().bHMotorCtrl = true;
			}

			DEBUG("iXRunMSecs:%d!", iXRunMSecs);
			if (iXRunMSecs > m_intervalMSecs)
			{
				m_intervalMSecs = iXRunMSecs;
			}

		}
		else
		{
			DEBUG("iYDis Error: %d!iLimitY:%d!", iYDis, iLimitY);
			return false;
		}

		if (intervalMSecsToSet > m_intervalMSecs)
		{
			m_intervalMSecs = intervalMSecsToSet;
			DEBUG("Reset m_intervalMSecs to Hold MSecs:%d!", m_intervalMSecs);
		}

		//m_intervalMSecs += 10; //X Motor Inter time

		DEBUG("iPositionY:%d!m_posSysY:%d!m_intervalMSecs:%d!", iDValue,
				m_posSysY, m_intervalMSecs);
		gettimeofday(&m_timeTag, NULL);

		if (m_statusSYS == SYS_STATUS_WAIT)
		{
			usleep((m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			DEBUG("SYS_STATUS_WAIT usleeped time:%d!",
					(m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			usleep(INTERVAL_TIME_MIN * 1000);
		}
		else
		{
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			usleep((m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			DEBUG("usleeped time:%d!",
					(m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);

			//usleep(INTERVAL_TIME_MIN * 1000);
		}
	}
	return true;
}

bool WayOnFootor::ExcuMotRelative(string strCmd)
{
	DEBUG("MotRelative Cmd:%s!", strCmd.c_str());
	if (m_makeUp.m_lstCmdRecv.size() > 0)
	{
		m_makeUp.m_lstCmdRecv.pop_front();
	}
	int iPosStart = 0;
	int iPosEnd = 0;
	string strData;
	int iPosX = 0;
	int iPositionY = 0;
	int iDotId;
	unsigned int intervalMSecsToSet = 0, intervalMSecsToSetTmp = 0;
	unsigned char i = 1;
	m_intervalMSecs = 100;
	unsigned int m_intervalMSecsTmp = m_intervalMSecs;
	int iOffsetX = 0;
	if ((iPosStart = strCmd.find(CMD_KEY_MOR)) != string::npos)
	{
		strCmd = strCmd.substr(iPosStart + sizeof(CMD_KEY_MOR) - 1);
		DEBUG("Cmd:%s!", strCmd.c_str());
		while (strCmd.size() > 3)
		{
			if (((iPosStart = strCmd.find('<')) != string::npos) && ((iPosEnd =
					strCmd.find('>')) != string::npos))
			{
				strData = strCmd.substr(iPosStart, iPosEnd - iPosStart + 1);
				strData = TrimSpace(strData);
				unsigned int iValue = 0;
				unsigned int iDisMOR = 0;
				unsigned int iSubCmdCounter = 0;
				if (sscanf(strData.c_str(), "<%d,%d,%d,%d>", &iPosX,
						&iPositionY, &iValue, &intervalMSecsToSetTmp) == 4)
				{
					if (((iPosX + m_posActX) < 0)
							|| ((iPosX + m_posActX) > (WIN_LENGTH))
							|| ((m_posSysY - iPositionY)
									< (-1 * DIRECTION_RANGE))
							|| ((m_posSysY + iPositionY) > DIRECTION_RANGE)
							|| (iValue < FLAG_TO_CLOSE)
							|| (iValue > FLAG_TO_NOHD))
					{
						ERROR(
								"Mot Cmd Value Error:%d %d %d!m_posSysY:%d!DIRECTION_RANGE:%d!m_posActY:%d!m_posActX:%d!",
								iPosX, iPositionY, iValue, m_posSysY,
								DIRECTION_RANGE, m_posActY, m_posActX);
						//m_posActY -= iPositionY;
						return false;
					}

					if (((iValue == FLAG_TO_NOHD) || (iValue == FLAG_TO_HOLD))
							&& (intervalMSecsToSetTmp > intervalMSecsToSet))
					{
						intervalMSecsToSet = intervalMSecsToSetTmp;
					}
					DEBUG("Got Mot Value:%d %d %d %d!", iPosX, iPositionY,
							iValue, intervalMSecsToSetTmp);

					int iPosACTX = iPosX + m_posActX;
					if (iPositionY
							> (m_posSysY + WIN_WIDTH_OUTSIDE - WIN_WIDTH_SIDE))
					{
						iDotId = m_pressWork.GetDotNearestX(iPosACTX, true);
					}
					else if (iPositionY < (m_posSysY + WIN_WIDTH_SIDE))
					{
						iDotId = m_pressWork.GetDotNearestX(iPosACTX, false);
					}
					else
					{
						iDotId = m_pressWork.GetWinDotNearest(iPosACTX,
								iPositionY - m_posSysY);
					}

					DEBUG("Set ShareMem:%d %d!", iDotId, iValue);

					//todo:X Axis Motor CTRL
					iOffsetX = ReLocateX((unsigned char&) iDotId,
							(unsigned int) iPosACTX);

					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else if (sscanf(strData.c_str(), "<%d,%d,%d>", &iPosX,
						&iPositionY, &iValue) == 3)
				{
					//m_posActY += iPositionY;
					//					if ((iPosX < 0) || (iPosX > (WIN_LENGTH))
					//							|| ((iPositionY - m_posSysY)
					//									< (-2 * DIRECTION_RANGE))
					//							|| ((iPositionY - m_posSysY) > 2 * DIRECTION_RANGE)
					//							|| (iValue < FLAG_TO_CLOSE)
					//							|| (iValue > FLAG_TO_NOHD)
					//							|| ((iPositionY - m_posActY)
					//									< (-2 * DIRECTION_RANGE))
					//							|| ((iPositionY - m_posActY) > 2 * DIRECTION_RANGE))
					if (((iPosX + m_posActX) < 0)
							|| ((iPosX + m_posActX) > (WIN_LENGTH))
							|| ((m_posSysY - iPositionY)
									< (-1 * DIRECTION_RANGE))
							|| ((m_posSysY + iPositionY) > DIRECTION_RANGE)
							|| (iValue < FLAG_TO_CLOSE)
							|| (iValue > FLAG_TO_NOHD))
					{
						ERROR(
								"Mot Cmd Value Error:%d %d %d!m_posSysY:%d!DIRECTION_RANGE:%d!m_posActY:%d!m_posActX:%d!",
								iPosX, iPositionY, iValue, m_posSysY,
								DIRECTION_RANGE, m_posActY, m_posActX);
						//m_posActY -= iPositionY;

						if (iPosX + m_posActX < 0)
						{
							ERROR("iPosX + m_posActX < 0!%d, %d!", iPosX,
									m_posActX);
						}
						if ((iPosX + m_posActX) > (WIN_LENGTH))
						{
							ERROR(
									"(iPosX + m_posActX) > (WIN_LENGTH)!%d, %d, %d!",
									iPosX, m_posActX, WIN_LENGTH);
						}
						if ((m_posSysY - iPositionY) < (-1 * DIRECTION_RANGE))
						{
							ERROR(
									"(m_posSysY - iPositionY)< (-1 * DIRECTION_RANGE)!%d, %d, %d!",
									m_posSysY, iPositionY, DIRECTION_RANGE);
						}
						if ((m_posSysY + iPositionY) > DIRECTION_RANGE)
						{
							ERROR(
									"(m_posSysY + iPositionY) > DIRECTION_RANGE!%d, %d, %d!",
									m_posSysY, iPositionY, DIRECTION_RANGE);
						}
						if (iValue < FLAG_TO_CLOSE)
						{
							ERROR("iPosX + m_posActX < 0!%d, %d!", iValue,
									FLAG_TO_CLOSE);
						}
						if (iValue > FLAG_TO_NOHD)
						{
							ERROR("iPosX + m_posActX < 0!%d, %d!", iValue,
									FLAG_TO_NOHD);
						}

						return false;
					}

					DEBUG("Got Mot Value:%d %d %d!m_posActY:%d!", iPosX,
							iPositionY, iValue, m_posActY);
					//if (iPositionY >= (m_posSysY + WIN_WIDTH_INSIDE / 2))

					int iPosACTX = iPosX + m_posActX;
					if (((m_posActY + iPositionY) - m_posSysY)
							>= ( WIN_WIDTH_INSIDE / 2))
					{
						iDotId = GetDotNearestX(iPosACTX, true);
					}
					else if (((m_posActY + iPositionY) - m_posSysY)
							<= (-1 * WIN_WIDTH_INSIDE / 2))
					{
						iDotId = GetDotNearestX(iPosACTX, false);
					}
					else
					{
						iDotId = GetWinDotNearest(iPosACTX,
								(m_posActY + iPositionY) - m_posSysY);
					}

					iOffsetX = ReLocateX((unsigned char &) iDotId,
							(unsigned int) iPosACTX);

					DEBUG("Set ShareMem:%d %d!iOffsetX:%d!", iDotId, iValue,
							iOffsetX);
					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else
				{
					ERROR("ScanData fail!");
				}
				DEBUG("Cmd:%s!Tmp:%s!", strCmd.c_str(), strData.c_str());
				strCmd = strCmd.substr(iPosEnd + 1);
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		return false;
	}
	if (i > 1)
	{
		DEBUG("MotR data Got:%d!", (unsigned int )(i) - 1);;
		float fDis = 0;
		float fSpeed = 0;
		map<unsigned int, dotCoordInfo>::iterator itDot;
		itDot = m_pressWork.dotCoordMap.find(iDotId);
		if (itDot == m_pressWork.dotCoordMap.end())
		{
			ERROR("Cann't find Dot:%d!", iDotId);
			return false;
		}
		m_posActY = m_posActY + iPositionY;
		//m_posActX += iPosX;
		//m_posActX = m_offsetDotX;
		m_posActX = m_posActX + iPosX;
		DEBUG(
				"Redefine m_posActY iPositionY:%d!m_posActY:%d!m_posActX=%d, iPosX=%d!",
				iPositionY, m_posActY, m_posActX, iPosX);
		//if (iPositionY >= (m_posSysY + WIN_WIDTH_INSIDE / 2))
		if (((m_posActY - m_posSysY)) >= ( WIN_WIDTH_INSIDE / 2))
		{
			DEBUG("> iPositionY:%d!m_posSysY:%d!", iPositionY, m_posSysY);
			fDis = (m_posActY - m_posSysY) - ( DOT_CORD_STEPLENY);
			fSpeed = MOTOR_STD_SPEED;
			m_intervalMSecs = ((fDis * 10) / fSpeed) * 1000;

			int iDistance = fDis * 10;
			int iSpeed = fSpeed;
			DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);
			if (iDistance > 0)
			{
				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_RIGHT, iDistance, iSpeed);
				//m_posSysY = iPositionY - DOT_CORD_STEPLENY;
				m_posSysY += fDis;
				m_posActY = m_posSysY + DOT_CORD_STEPLENY;
			}
		}
		else if ((m_posActY - m_posSysY) <= (-1 * WIN_WIDTH_INSIDE / 2))
		{
			DEBUG("< iPositionY:%d!m_posSysY:%d!", iPositionY, m_posSysY);
			fDis = m_posSysY - m_posActY - DOT_CORD_STEPLENY;
			fSpeed = MOTOR_STD_SPEED;
			m_intervalMSecs = ((fDis * 10) / fSpeed) * 1000;

			int iDistance = fDis * 10;
			int iSpeed = fSpeed;
			DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);
			if (iDistance > 0)
			{
				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_LEFT, iDistance, iSpeed);
				//m_posSysY = iPositionY + DOT_CORD_STEPLENY;
				m_posSysY -= fDis;
				m_posActY = m_posSysY - DOT_CORD_STEPLENY;
			}
		}
		else
		{
			DEBUG("iPositionY In Rectage:%d!m_posSysY:%d!m_posActY:%d!",
					iPositionY, m_posSysY, m_posActY);
		}

		if (intervalMSecsToSet > m_intervalMSecs)
		{
			m_intervalMSecs = intervalMSecsToSet;
			DEBUG("Reset m_intervalMSecs to Hold MSecs:%d!", m_intervalMSecs);
		}

		//		if(m_intervalMSecs < m_intervalMSecsTmp * (i - 1))
		//		{
		//			m_intervalMSecs = m_intervalMSecsTmp *(i - 1);
		//			DEBUG("m_intervalMSecs redefine to 100 * (i - 1) : %d!i=%d!", m_intervalMSecs, (i - 1));
		//		}

		DEBUG("iPositionY:%d!m_posSysY:%d!m_intervalMSecs:%d!", iPositionY,
				m_posSysY, m_intervalMSecs);
		gettimeofday(&m_timeTag, NULL);

		int iXRunMSecs = 0;			//0.1mm  Y and X 1mm
		if (iOffsetX < 0)
		{
			iXRunMSecs = -1 * iOffsetX * 1000 / MOTOR_STD_SPEED_X;
			m_bufSharePress.Get().szMotorStatus = MOTOR_X_TIMEL;
			m_bufSharePress.Get().iRunMSecs = iXRunMSecs;
			m_bufSharePress.Get().bHMotorCtrl = true;
		}
		else if (iOffsetX > 0)
		{
			iXRunMSecs = iOffsetX * 1000 / MOTOR_STD_SPEED_X;
			m_bufSharePress.Get().szMotorStatus = MOTOR_X_TIMER;
			m_bufSharePress.Get().iRunMSecs = iXRunMSecs;
			m_bufSharePress.Get().bHMotorCtrl = true;
		}

		DEBUG("iXRunMSecs:%d!", iXRunMSecs);
		if (iXRunMSecs > m_intervalMSecs)
		{
			m_intervalMSecs = iXRunMSecs;
		}

		if (m_statusSYS == SYS_STATUS_WAIT)
		{
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			DEBUG("SYS_STATUS_WAIT usleeped time:%d!",
					(m_intervalMSecs) * 1000);
			usleep((m_intervalMSecs) * 1000);

		}
		else
		{
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			DEBUG("usleeped time:%d!", (m_intervalMSecs) * 1000);
			usleep((m_intervalMSecs) * 1000);
		}
		DEBUG("m_posSysY:%d!m_posActY:%d!fDis:%d!", m_posSysY, m_posActY, fDis);
	}
	return true;
}

int WayOnFootor::GetDotNearestX(unsigned int iPosX, bool bReversed)
{
	DEBUG("GetDotNearestX:%d Reverse:%s!", iPosX,
			bReversed == true ? "true" : "false");
	//
	//	iPosX = iPosX * 12 / 10;
	//	DEBUG("For test: *1.2", iPosX);
	int iSerial = 0;
	float distanceTmp = 0;
	float distance;
	int iTest = 0;
	if (bReversed == false)
	{
		TRACE("iTest:%d!", iTest);
		map<unsigned int, dotCoordInfo>::iterator itCoords =
				m_pressWork.dotCoordMap.begin();
		distance = abs((*itCoords).second.second.first - iPosX + m_offsetDotX);
		iSerial = (*itCoords).first;
		for (itCoords = m_pressWork.dotCoordMap.begin();
				itCoords != m_pressWork.dotCoordMap.end(); itCoords++)
		{
			DEBUG("GetDotNearestX bReversed:false iTest:%d!", iTest);
			distanceTmp = abs(
					(*itCoords).second.second.first - iPosX + m_offsetDotX);
			DEBUG("GetDotNearestX iSerialNo.:%d! distanceTmp:%f!distance:%f!",
					(*itCoords).first, distanceTmp, distance);
			if (distanceTmp < distance)
			{
				distance = distanceTmp;
				iSerial = (*itCoords).first;
				DEBUG("Nearest Dot[%d]:%f!distanceTmp:%f!Dot Pos:%d!", iSerial,
						distance, distanceTmp, (*itCoords).second.second.first);
			}
			else
			{
				TRACE("Far Dot[%d]:%f!distanceTmp:%f!Dot Pos:%d!",
						(*itCoords).first, distance, distanceTmp,
						(*itCoords).second.second.first);
			}
			iTest++;
		}
	}
	else
	{
		map<unsigned int, dotCoordInfo>::iterator itCoords =
				m_pressWork.dotCoordMap.begin();
		distance = abs((*itCoords).second.second.first - iPosX + m_offsetDotX);
		iSerial = (*itCoords).first;
		for (itCoords = m_pressWork.dotCoordMap.begin();
				itCoords != m_pressWork.dotCoordMap.end(); itCoords++)
		{
			//DEBUG("GetDotNearestX bReversed:true iTest:%d!", iTest);
			distanceTmp = abs(
					(*itCoords).second.second.first - iPosX + m_offsetDotX);
			//DEBUG("GetDotNearestX iSerialNo.:%d! distanceTmp:%f!distance:%f!",
			//		(*itCoords).first, distanceTmp, distance);
			if (distanceTmp <= distance)
			{
				distance = distanceTmp;
				iSerial = (*itCoords).first;
				DEBUG("bReversed:true Nearest Dot[%d]:%f!<=:%f!", iSerial,
						distance, distanceTmp);
			}
			iTest++;
		}
	}
	DEBUG("Rtn iSerial:%d!", iSerial);
	return iSerial;
}

int WayOnFootor::GetWinDotNearest(unsigned int iPosX, unsigned int iPosY)
{
	DEBUG("GetWinDotNearest:%d %d!", iPosX, iPosY);
//	iPosX = iPosX * 12 / 10;
//	DEBUG("For test: *1.2", iPosX);
	map<unsigned int, dotCoordInfo>::iterator itCoords =
			m_pressWork.dotCoordMap.begin();
	float distance = sqrt(
			(iPosX - (*itCoords).second.second.first - m_offsetDotX)
					* (iPosX - (*itCoords).second.second.first - m_offsetDotX)
					+ (iPosY - (*itCoords).second.second.second)
							* (iPosY - (*itCoords).second.second.second));
	float distanceTmp = 0;
	int iSerial = 0;
	int iTest = 0;
	for (itCoords = m_pressWork.dotCoordMap.begin();
			itCoords != m_pressWork.dotCoordMap.end(); itCoords++)
	{
		TRACE("iTest:%d!", iTest);
		distanceTmp = sqrt(
				(iPosX - (*itCoords).second.second.first - m_offsetDotX)
						* (iPosX - (*itCoords).second.second.first
								- m_offsetDotX)
						+ (iPosY - (*itCoords).second.second.second)
								* (iPosY - (*itCoords).second.second.second));
		if (distanceTmp < distance)
		{
			distance = distanceTmp;
			iSerial = (*itCoords).first;
			DEBUG("Nearest Dot[%d]:%f!", iSerial, distance);
		}
		iTest++;
	}
	return iSerial;
}

int WayOnFootor::ReLocateX(unsigned char& iDotID, int iPositionX)
{
	DEBUG("ReLocateX:%d!iPositionX:%d!", iDotID, iPositionX);
	int iRtn = 0;
	int iDis = -9999;
	map<unsigned int, dotCoordInfo>::iterator itCoords =
			m_pressWork.dotCoordMap.begin();
	for (itCoords = m_pressWork.dotCoordMap.begin();
			itCoords != m_pressWork.dotCoordMap.end(); itCoords++)
	{
		if ((*itCoords).first == iDotID)
		{
			iDis = iPositionX - (*itCoords).second.second.first - m_offsetDotX;
			DEBUG("iDotID[%d] CordX:%d!m_offsetDotX:%d!", iDotID,
					(*itCoords).second.second.first, m_offsetDotX);
			break;
		}
	}
	if (iDis == -9999)
	{
		DEBUG("Fail To Get X Offset!");
		return 0;
	}
	DEBUG("iDis:%d!", iDis);
//	switch (iDotID)
//	{
//	case 0:
//	case 8:
//	case 1:
//	case 9:
//		m_offsetDotX = m_offsetDotX + iDis;
//		break;
//	case 2:
//	case 10:
//		iDis = iDis + DOT_CORD_STEPLENX;
//		m_offsetDotX = m_offsetDotX + iDis;
//		iDotID = 1;
//		break;
//	case 3:
//	case 11:
//		iDis = iDis + 2 * DOT_CORD_STEPLENX;
//		m_offsetDotX = m_offsetDotX + iDis;
//		iDotID = 1;
//		break;
//	default:
//		DEBUG("Dot ID ERROR:%d!", iDotID);
//		break;
//	}

	//m_offsetDotX = m_offsetDotX + iDis;

//	if ((m_offsetDotX + iDis) > 2 * DOT_CORD_STEPLENX)
//	{
//		DEBUG("m_offsetDotX + iDis Too Big:%d!", m_offsetDotX);
//		m_offsetDotX = 2 * DOT_CORD_STEPLENX;
//		iDis = 2 * DOT_CORD_STEPLENX - m_offsetDotX;
//	}
//	else if ((m_offsetDotX + iDis) < 0)
//	{
//		DEBUG("m_offsetDotX + iDis Too Small:%d!", m_offsetDotX);
//		m_offsetDotX = 0;
//		iDis = -1 * m_offsetDotX;
//	}

	if (iDis + m_offsetDotX < 0)
	{
		DEBUG("iDis + m_offsetDotX < 0 Reset m_offsetDotX:%d!iDis:%d!",
				m_offsetDotX, iDis);
		iDis = -m_offsetDotX;
		m_offsetDotX = 0;
	}
	else if (iDis + m_offsetDotX > 2 * DOT_CORD_STEPLENX)
	{
		iDis = 2 * DOT_CORD_STEPLENX - m_offsetDotX;
	}
	else
	{
		m_offsetDotX = m_offsetDotX + iDis;
	}

//	if (iDis > 2 * DOT_CORD_STEPLENX)
//	{
//		DEBUG("iDis Too Big:%d!", iDis);
//		iDis = 2 * DOT_CORD_STEPLENX;
//	}
//	else if (iDis < -2 * DOT_CORD_STEPLENX)
//	{
//		DEBUG("iDis Too Small:%d!", iDis);
//		iDis = -2 * DOT_CORD_STEPLENX;
//	}

	DEBUG("X iDis:%d m_offsetDotX:%d iDotID:%d!", iDis, m_offsetDotX, iDotID);
	iRtn = iDis;
	return iRtn;
}

bool WayOnFootor::ExcuMot(string strCmd)
{
	DEBUG("Mot Cmd:%s!", strCmd.c_str());
	if (m_makeUp.m_lstCmdRecv.size() > 0)
	{
		m_makeUp.m_lstCmdRecv.pop_front();
	}
	unsigned int iPosStart = 0;
	unsigned int iPosEnd = 0;
	string strData;
	unsigned char i = 1;
	if ((iPosStart = strCmd.find(CMD_KEY_MOT)) != string::npos)
	{
		strCmd = strCmd.substr(iPosStart + sizeof(CMD_KEY_MOT) - 1);
		DEBUG("Cmd:%s!", strCmd.c_str());
		while (strCmd.size() > 3)
		{
			if (((iPosStart = strCmd.find('<')) != string::npos) && ((iPosEnd =
					strCmd.find('>')) != string::npos))
			{
				strData = strCmd.substr(iPosStart, iPosEnd - iPosStart + 1);
				strData = TrimSpace(strData);
				unsigned int iDotId = 0, iValue = 0;
				if (sscanf(strData.c_str(), "<%d,%d>", &iDotId, &iValue) == 2)
				{
					DEBUG("Set ShareMem:%d %d!", iDotId, iValue);
					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else
				{
					ERROR("ScanData fail!");
				}
				DEBUG("Cmd:%s!Tmp:%s!", strCmd.c_str(), strData.c_str());
				strCmd = strCmd.substr(iPosEnd + 1);
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		return false;
	}
	if (i > 1)
	{
		DEBUG("Mot data Got:%d!", (unsigned int )(i) - 1);
		m_bufSharePress.Get().szValveMap[0][0] = i;
		m_bufSharePress.Get().bWork = true;
	}
	return true;
}

//Mot Cmd	Form:MOT <x,y,p><x,y,p><x,y,p><x,y,p>...
bool WayOnFootor::ExcuMotCmd(string strCmd)
{
	DEBUG("Mot Cmd:%s!", strCmd.c_str());
	if (m_makeUp.m_lstCmdRecv.size() > 0)
	{
		m_makeUp.m_lstCmdRecv.pop_front();
	}
	int iPosStart = 0;
	int iPosEnd = 0;
	string strData;
	int iPositionY = 0;
	int iDotId;
	unsigned int intervalMSecsToSet = 0, intervalMSecsToSetTmp = 0;
	unsigned char i = 1;
	if ((iPosStart = strCmd.find(CMD_KEY_MOT)) != string::npos)
	{
		strCmd = strCmd.substr(iPosStart + sizeof(CMD_KEY_MOT) - 1);
		DEBUG("Cmd:%s!", strCmd.c_str());
		while (strCmd.size() > 3)
		{
			if (((iPosStart = strCmd.find('<')) != string::npos) && ((iPosEnd =
					strCmd.find('>')) != string::npos))
			{
				strData = strCmd.substr(iPosStart, iPosEnd - iPosStart + 1);
				strData = TrimSpace(strData);
				unsigned int iPosX = 0, iValue = 0;
				if (sscanf(strData.c_str(), "<%d,%d,%d,%d>", &iPosX,
						&iPositionY, &iValue, &intervalMSecsToSetTmp) == 4)
				{
					if ((iPosX < 0) || (iPosX > (MAX_LENGTH_Y + MAX_WIDTH_W))
							|| (iPositionY < 0)
							|| (iPositionY > (MAX_LENGTH_Y + MAX_WIDTH_W))
							|| (iValue < FLAG_TO_CLOSE)
							|| (iValue > FLAG_TO_NOHD))
					{
						ERROR("Mot Cmd Value Error:%d %d %d!", iPosX,
								iPositionY, iValue, intervalMSecsToSetTmp);
						return false;
					}

					if (((iValue == FLAG_TO_NOHD) || (iValue == FLAG_TO_HOLD))
							&& (intervalMSecsToSetTmp > intervalMSecsToSet))
					{
						intervalMSecsToSet = intervalMSecsToSetTmp;
					}
					DEBUG("Got Mot Value:%d %d %d %d!", iPosX, iPositionY,
							iValue, intervalMSecsToSetTmp);
					if (iPositionY > (m_posSysY + MAX_WIDTH_W))
					{
						// iDotId = m_pressWork.GetDotNearestX(iPosX, true);     //Only one line , do not need to reverse
						iDotId = m_pressWork.GetDotNearestX(iPosX, true);
					}
					else if (iPositionY < m_posSysY)
					{
						iDotId = m_pressWork.GetDotNearestX(iPosX, false);
					}
					else
					{
						iDotId = m_pressWork.GetWinDotNearest(iPosX,
								iPositionY);
					}

					DEBUG("Set ShareMem:%d %d!", iDotId, iValue);
					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else if (sscanf(strData.c_str(), "<%d,%d,%d>", &iPosX,
						&iPositionY, &iValue) == 3)
				{
					if ((iPosX < 0) || (iPosX > (MAX_LENGTH_Y + MAX_WIDTH_W))
							|| (iPositionY < 0)
							|| (iPositionY > (MAX_LENGTH_Y + MAX_WIDTH_W))
							|| (iValue < 0) || (iValue > 101))
					{
						ERROR("Mot Cmd Value Error:%d %d %d!", iPosX,
								iPositionY, iValue);
						return false;
					}
					DEBUG("Got Mot Value:%d %d %d!", iPosX, iPositionY, iValue);
					if (iPositionY > (m_posSysY + MAX_WIDTH_W))
					{
						// iDotId = m_pressWork.GetDotNearestX(iPosX, true);   //Only one line , do not need to reverse
						iDotId = m_pressWork.GetDotNearestX(iPosX, true);
					}
					else if (iPositionY < m_posSysY)
					{
						iDotId = m_pressWork.GetDotNearestX(iPosX, false);
					}
					else
					{
						iDotId = m_pressWork.GetWinDotNearest(iPosX,
								iPositionY);
					}

					DEBUG("Set ShareMem:%d %d!", iDotId, iValue);
					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else
				{
					ERROR("ScanData fail!");
				}
				DEBUG("Cmd:%s!Tmp:%s!", strCmd.c_str(), strData.c_str());
				strCmd = strCmd.substr(iPosEnd + 1);
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		return false;
	}
	if (i > 1)
	{
		DEBUG("Mot data Got:%d!", (unsigned int )(i) - 1);;
		float fDis;
		float fSpeed;
		map<unsigned int, dotCoordInfo>::iterator itDot;
		itDot = m_pressWork.dotCoordMap.find(iDotId);
		if (itDot == m_pressWork.dotCoordMap.end())
		{
			ERROR("Cann't Dot:%d!", iDotId);
			return false;
		}
		m_intervalMSecs = 50;
		if (iPositionY < m_posSysY + DOT_CORD_STARTY)
		{
			DEBUG("< iPositionY:%d!m_posSysY:%d!", iPositionY, m_posSysY);
			fDis = m_posSysY + DOT_CORD_STARTY
					+ (*itDot).second.first.first * DOT_CORD_STEPLENY
					- iPositionY;
			if (fDis > m_posSysY)
			{
				fDis = m_posSysY;
			}
			//fSpeed = (fDis) / (m_intervalMSecs / 1000.0);
			fSpeed = MOTOR_STD_SPEED;
			m_intervalMSecs = ((fDis * 10) / fSpeed) * 1000;
			if (m_intervalMSecs < INTERVAL_TIME_MIN)
			{
				m_intervalMSecs = INTERVAL_TIME_MIN;
				DEBUG("m_intervalMSecs is 0, reset to m_intervalMSecs=%d!",
						m_intervalMSecs);
			}
			DEBUG(
					"iPositionY[%d] < m_posSysY[%d] Distance:%.1f Speed:%.1f, m_intervalMSecs=%d!",
					iPositionY, m_posSysY, fDis, fSpeed, m_intervalMSecs);
			int iDistance = fDis * 10;
			int iSpeed = fSpeed;
			DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);
			if (iDistance > 0)
			{
				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_LEFT, iDistance, iSpeed);
			}

			//			m_posSysY = m_posSysY - DOT_CORD_STARTY
			//					- (*itDot).second.first.first * DOT_CORD_STEPLENY;
			m_posSysY = m_posSysY - fDis;
		}
		else if (iPositionY > (m_posSysY + DOT_CORD_STARTY))
		{
			DEBUG("> iPositionY:%d!m_posSysY:%d!", iPositionY, m_posSysY);
			fDis = iPositionY - DOT_CORD_STARTY
					- (*itDot).second.first.first * DOT_CORD_STEPLENY
					- m_posSysY;
			if (fDis > (MAX_LENGTH_Y - m_posSysY - MAX_WIDTH_W))
			{
				fDis = MAX_LENGTH_Y - m_posSysY - MAX_WIDTH_W;
			}
			//fSpeed = (fDis) / (m_intervalMSecs / 1000.0);
			fSpeed = MOTOR_STD_SPEED;
			m_intervalMSecs = ((fDis * 10) / fSpeed) * 1000;
			if (m_intervalMSecs < INTERVAL_TIME_MIN)
			{
				m_intervalMSecs = INTERVAL_TIME_MIN;
				DEBUG("m_intervalMSecs is 0, reset to 500!m_intervalMSecs=%d!",
						m_intervalMSecs);
			}
			DEBUG(
					"iPositionY[%d] > (m_posSysY + MAX_WIDTH_W)[%d] Distance:%.1f Speed:%.1f m_intervalMSecs=%d!",
					iPositionY, m_posSysY + MAX_WIDTH_W, fDis, fSpeed,
					m_intervalMSecs);
			int iDistance = fDis * 10;
			int iSpeed = fSpeed;
			DEBUG("iDistance:%d iSpeed:%d!", iDistance, iSpeed);
			if (iDistance > 0)
			{
				m_pwmCtrl.move(MOTOR_INDEX, MOTOR_RIGHT, iDistance, iSpeed);
			}

			//			m_posSysY = m_posSysY + DOT_CORD_STARTY
			//					+ (*itDot).second.first.first * DOT_CORD_STEPLENY;
			m_posSysY = m_posSysY + fDis;

		}
		if (intervalMSecsToSet > m_intervalMSecs)
		{
			m_intervalMSecs = intervalMSecsToSet;
			DEBUG("Reset m_intervalMSecs to Hold MSecs:%d!", m_intervalMSecs);
		}
		DEBUG("iPositionY:%d!m_posSysY:%d!m_intervalMSecs:%d!", iPositionY,
				m_posSysY, m_intervalMSecs);
		gettimeofday(&m_timeTag, NULL);

		if (m_statusSYS == SYS_STATUS_WAIT)
		{
			usleep((m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			DEBUG("SYS_STATUS_WAIT usleeped time:%d!",
					(m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			usleep(INTERVAL_TIME_MIN * 1000);
		}
		else
		{
			m_bufSharePress.Get().szValveMap[0][0] = i;
			m_bufSharePress.Get().bWork = true;
			usleep((m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);
			DEBUG("usleeped time:%d!",
					(m_intervalMSecs - INTERVAL_TIME_MIN) * 1000);

			//usleep(INTERVAL_TIME_MIN * 1000);
		}
	}
	return true;
}

bool WayOnFootor::TestGPIO(string strCmd)
{
	DEBUG("TestGPIO Cmd:%s!", strCmd.c_str());
	if (m_makeUp.m_lstCmdRecv.size() > 0)
	{
		m_makeUp.m_lstCmdRecv.pop_front();
	}
	int iPosStart = 0;
	int iPosEnd = 0;
	string strData;
	int iDotId;
	unsigned char i = 1;
	if ((iPosStart = strCmd.find(CMD_KEY_GPIO)) != string::npos)
	{
		strCmd = strCmd.substr(iPosStart + sizeof(CMD_KEY_GPIO) - 1);
		DEBUG("Cmd:%s!", strCmd.c_str());
		while (strCmd.size() > 3)
		{
			if (((iPosStart = strCmd.find('<')) != string::npos) && ((iPosEnd =
					strCmd.find('>')) != string::npos))
			{
				strData = strCmd.substr(iPosStart, iPosEnd - iPosStart + 1);
				strData = TrimSpace(strData);
				unsigned int iValue = 0;
				if (sscanf(strData.c_str(), "<%d,%d>", &iDotId, &iValue) == 2)
				{
					DEBUG("Set ShareMem:%d %d!", iDotId, iValue);
					m_bufSharePress.Get().szValveMap[i][0] =
							(unsigned char) (iDotId);
					m_bufSharePress.Get().szValveMap[i][1] =
							(unsigned char) (iValue);
					i++;
				}
				else
				{
					ERROR("ScanData fail!");
				}
				DEBUG("Cmd:%s!Tmp:%s!", strCmd.c_str(), strData.c_str());
				strCmd = strCmd.substr(iPosEnd + 1);
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		return false;
	}
	if (i > 1)
	{
		DEBUG("TestGPIO data Got:%d!", (unsigned int )(i) - 1);;
		m_bufSharePress.Get().szValveMap[0][0] = i;
		m_bufSharePress.Get().bWork = true;
	}
	return true;
}

void WayOnFootor::ResetInterval(string strCmd)
{
	int iInterval = -1;
	if (sscanf(strCmd.c_str(), "%*s %d", &iInterval) > 0)
	{
		if (iInterval >= 10)
		{
			m_intervalMSecs = iInterval;
			DEBUG("Reset Interval to:%d!", m_intervalMSecs);
		}
		else
		{
			ERROR("Interval reset value illegal:%d!", iInterval);
		}
	}
	else if (strCmd.find(CMD_KEY_INTV) != string::npos)
	{
		char szTmp[10];
		sprintf(szTmp, "%d", m_intervalMSecs);
		string strIntervalFB = "INTERVAL ";
		strIntervalFB.append(szTmp);
		m_lstInfoFBack.push_back(strIntervalFB);
		DEBUG("IntervalFB:%s!", strIntervalFB.c_str());
	}
	m_makeUp.m_lstCmdRecv.pop_front();
}

bool WayOnFootor::ExcuMod(string strCmd)
{
	DEBUG("Mod Cmd:%s!", strCmd.c_str());
	int iModId = -1;
	if (sscanf(strCmd.c_str(), "%*s %d", &iModId) > 0)
	{
		switch (iModId)
		{
		case MOD_ID_0:
			m_makeUp.Massage01();
			break;
		case MOD_ID_1:
			m_makeUp.TriDots01();
			break;
		case MOD_ID_2:
			m_makeUp.TriDots02();
			break;
		case MOD_ID_3:
			m_makeUp.MasgTriDots();
			break;
		case MOD_ID_4:
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		case MOD_ID_5:
			m_makeUp.m_lstCmdRecv.pop_front();
			break;
		default:
			ERROR("MOD Not Defined!%d!", iModId);
			m_makeUp.m_lstCmdRecv.pop_front();
			return false;
		}
	}
	return true;
}

bool WayOnFootor::ExcuMulti(string strCmd)
{
	return m_makeUp.DisposeMulParam();
}

bool WayOnFootor::ExcuFile(string strCmd)
{
	int iPosStart = strCmd.find(CMD_KEY_FILE);
	if (iPosStart != string::npos)
	{
		string strFileData = strCmd.substr(
				iPosStart + sizeof(CMD_KEY_FILE) - 1);
		DEBUG("FileData:%s!", strFileData.c_str());
		if (false == m_fuctions.ParseContent(strFileData))
		{
			ERROR("Can't Parse File Data!%s!", strCmd.c_str());
		}
		else
		{
			return true;
		}
	}
	return false;
}

bool WayOnFootor::ExcuLeft(string strCmd)
{
	DEBUG("ExcuLeft:%s!", strCmd.c_str());
	int iPosStart = strCmd.find(CMD_KEY_LEFT);
	if (iPosStart != string::npos)
	{
		strCmd = strCmd.substr(iPosStart);
		int iDist = 0;
		if (sscanf(strCmd.c_str(), "%*s %d", &iDist) == 1)
		{
			DEBUG("ExcuLeft iDist:%d!", iDist);
			if (false == m_pwmCtrl.move(MOTOR_INDEX, MOTOR_LEFT, iDist,
			MOTOR_STD_SPEED))
			{
				ERROR("Move Left Fail!", strCmd.c_str());
			}
			else
			{
				return true;
			}
		}
		else
		{
			ERROR("Move Left Dist Miss!");
		}
	}
	return false;
}

bool WayOnFootor::ExcuRight(string strCmd)
{
	DEBUG("ExcuRight:%s!", strCmd.c_str());
	int iPosStart = strCmd.find(CMD_KEY_RIGHT);
	if (iPosStart != string::npos)
	{
		strCmd = strCmd.substr(iPosStart);
		int iDist = 0;
		if (sscanf(strCmd.c_str(), "%*s %d", &iDist) == 1)
		{
			DEBUG("ExcuRight iDist:%d!", iDist);
			if (false == m_pwmCtrl.move(MOTOR_INDEX, MOTOR_RIGHT, iDist,
			MOTOR_STD_SPEED))
			{
				ERROR("Move Right Fail!", strCmd.c_str());
			}
			else
			{
				return true;
			}
		}
		else
		{
			ERROR("Move Right Dist Miss!");
		}
	}
	return false;
}

bool WayOnFootor::SleepM(string strCmd)
{
	DEBUG("SleepM:%s!", strCmd.c_str());
	int iPosStart = strCmd.find(CMD_KEY_SLEEPM);
	if (iPosStart != string::npos)
	{
		strCmd = strCmd.substr(iPosStart);
		int iMSecs = 0;
		if (sscanf(strCmd.c_str(), "%*s %d", &iMSecs) == 1)
		{
			DEBUG("SleepM iMSecs:%d!", iMSecs);
			usleep(iMSecs *1000);
			return true;
		}
		else
		{
			ERROR("SleepM iMSecs Miss!");
		}
	}
	return false;
}

bool WayOnFootor::BuildChildProcess()
{
	m_pidMainWork = getpid();
	if (m_pidMasgPress == -1)
	{
		m_pidMasgPress = fork();
		if (m_pidMasgPress == 0)
		{
			m_pressWork.Working();
		}
		if (m_pidMasgPress != -1)
		{
			setpgid(m_pidMasgPress, m_pidMainWork);
			DEBUG("setpgid %d ---> %d!", m_pidMasgPress, m_pidMainWork);
			if (checkChildPid(m_pidMasgPress) < 0)
			{
				ERROR("Something wrong Press Process!");
			}
		}
		else
		{
			ERROR("Fork Press Process fail!");
			return false;
		}
	}
	else
	{
		if (checkChildPid(m_pidMasgPress) != 0)
		{
			ERROR("Press Process does not exist!");
			m_pidMasgPress = -1;
			return false;
		}
	}

	if (m_pidDevLstn == -1)
	{
		m_pidDevLstn = fork();
		if (m_pidDevLstn == 0)
		{
			m_lstnDev.Listen();
		}
		if (m_pidDevLstn != -1)
		{
			setpgid(m_pidDevLstn, m_pidMainWork);
			DEBUG("setpgid %d ---> %d!", m_pidDevLstn, m_pidMainWork);
			if (checkChildPid(m_pidDevLstn) < 0)
			{
				ERROR("Something wrong Dev Listener Process!");
			}
		}
		else
		{
			ERROR("Fork Dev Listener Process fail!");
			return false;
		}
	}
	else
	{
		if (checkChildPid(m_pidDevLstn) != 0)
		{
			ERROR("Dev Listener Process does not exist!");
			m_pidDevLstn = -1;
			return false;
		}
	}

	return true;
}

int WayOnFootor::checkChildPid(pid_t pidWait)
{
	int status = -1;
	pid_t ret = waitpid(pidWait, &status, WNOHANG);
	if (ret < 0)
	{
		ret = errno;
		if (ret == ECHILD)
		{
			DEBUG("Child Process[%d] return ECHILD!", pidWait);
		}
		else
		{
			DEBUG("Child Process[%d] Something error!", pidWait);
		}
	}
	if (WIFEXITED(status))
	{
		DEBUG("Child Process[%d] exit normal!code:%d!", pidWait,
				WEXITSTATUS(status));
		pidWait = -1;
		if (WEXITSTATUS(status) == -1)
		{
			//child exit code
		}
		return WEXITSTATUS(status);
	}
	else if (WIFSIGNALED(status))
	{
		char szTmp[16];
		int sign = WTERMSIG(status);
		sprintf(szTmp, "%d", sign);
		DEBUG("Child Process[%d] signaled by %d", pidWait, sign);
		if (sign == SIGTERM || sign == SIGKILL)
		{
			DEBUG("Child Process[%d] Signal is SIGTERM or SIGKILL!", pidWait,
					sign);
		}
		pidWait = -1;
		return -1;
	}
	return 0;
}

double WayOnFootor::timeDiff(timeval tEnd, timeval tStart)
{
	double diff = (tEnd.tv_sec - tStart.tv_sec) * 1000.0
			+ (tEnd.tv_usec - tStart.tv_usec) / 1000.0;
//	DEBUG("timeDiff:%f!%ld %ld  --- %ld %ld!", diff, tEnd.tv_sec, tEnd.tv_usec,
//			tStart.tv_sec, tStart.tv_usec);
	return diff;
}

string WayOnFootor::TrimSpace(string & str)
{
	string strTmp;
	if (str.empty())
		return strTmp;
	string::iterator it;
	it = str.begin();
	while (it != str.end())
	{
		if ((*it) == '\t' || (*it) == '\r' || (*it) == '\n' || (*it) == ' ')
		{
			it++;
			continue;
		}
		strTmp += (*it);
		it++;
	};

	return strTmp;
}

bool WayOnFootor::getLocalIP(void)
{
	struct ifaddrs * ifAddrStruct = NULL;
	void * ptrAddrTmp = NULL;
	string strIfa;
	getifaddrs(&ifAddrStruct);
	m_strLocalIpWlan.clear();
	m_strLocalIpEth.clear();
	while (ifAddrStruct != NULL)
	{
		strIfa = ifAddrStruct->ifa_name;
		if (strIfa.find("wlan") != string::npos)
		{
			DEBUG("Found wlan:%s!", ifAddrStruct->ifa_name);
			if (ifAddrStruct->ifa_addr->sa_family == AF_INET)
			{
				ptrAddrTmp =
						&((struct sockaddr_in *) ifAddrStruct->ifa_addr)->sin_addr;
				char addressBuffer[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, ptrAddrTmp, addressBuffer, INET_ADDRSTRLEN);
				m_strLocalIpWlan = addressBuffer;
				//DEBUG("%s IPv4 Address %s!", ifAddrStruct->ifa_name,
				//		addressBuffer);
				break;
			}
			else if (ifAddrStruct->ifa_addr->sa_family == AF_INET6)
			{
				ptrAddrTmp =
						&((struct sockaddr_in *) ifAddrStruct->ifa_addr)->sin_addr;
				char addressBuffer[INET6_ADDRSTRLEN];
				inet_ntop(AF_INET6, ptrAddrTmp, addressBuffer,
				INET6_ADDRSTRLEN);
				m_strLocalIpWlan = addressBuffer;
				DEBUG("%s IPv6 Address %s!", ifAddrStruct->ifa_name,
						addressBuffer);
				break;
			}
		}
		else if (strIfa.find("eth") != string::npos)
		{
			TRACE("Found eth:%s!", ifAddrStruct->ifa_name);
			if (ifAddrStruct->ifa_addr->sa_family == AF_INET)
			{
				ptrAddrTmp =
						&((struct sockaddr_in *) ifAddrStruct->ifa_addr)->sin_addr;
				char addressBuffer[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, ptrAddrTmp, addressBuffer, INET_ADDRSTRLEN);
				m_strLocalIpEth = addressBuffer;
				//				DEBUG("%s IPv4 Address %s!", ifAddrStruct->ifa_name,
				//						addressBuffer);
				break;
			}
			else if (ifAddrStruct->ifa_addr->sa_family == AF_INET6)
			{
				ptrAddrTmp =
						&((struct sockaddr_in *) ifAddrStruct->ifa_addr)->sin_addr;
				char addressBuffer[INET6_ADDRSTRLEN];
				inet_ntop(AF_INET6, ptrAddrTmp, addressBuffer,
				INET6_ADDRSTRLEN);
				m_strLocalIpEth = addressBuffer;
				DEBUG("%s IPv6 Address %s!", ifAddrStruct->ifa_name,
						addressBuffer);
				break;
			}
		}
		else
		{
			TRACE("IFA Name:%s!", ifAddrStruct->ifa_name);
		}
		ifAddrStruct = ifAddrStruct->ifa_next;
	}
	return true;
}

bool WayOnFootor::RefreshIP()
{
	if (m_strLocalIpWlan.size() > strlen("1.1.1.1"))
	{

	}
	else if (m_strLocalIpEth.size() > strlen("1.1.1.1"))
	{

	}
	else
	{

	}
	return true;
}

Display::Display()
{
	DEBUG("DISPLAY!");
	m_chKeyIndex = 0;
	m_strCmdBuff.clear();
	if (intiDev() == false)
	{
		ERROR("Open LCD DEV fail!");
	}
	else
	{
		DEBUG("LCD Open success:%d!", m_objLcd.GetHandle());
	}
}

Display::~Display()
{
	m_objLcd.Close();
}

bool Display::intiDev()
{
	DEBUG("intiDev LCD!");
	if (m_objLcd.GetHandle() > 0)
	{
		DEBUG("Close Lcd!");
		m_objLcd.Close();
	}
	if (m_objLcd.Open(LCD_DEV) < 0)
	{
		DEBUG("Open LCD fail!");
		return false;
	}
	return true;
}

unsigned int Display::getLineSerial(unsigned int iLine)
{
	switch (iLine)
	{
	case DISP_LINE_0:
		TRACE("Line0:%d!", line1);
		return line1;
	case DISP_LINE_1:
		TRACE("Line1:%d!", line2);
		return line2;
	case DISP_LINE_2:
		TRACE("Line2:%d!", line3);
		return line3;
	case DISP_LINE_3:
		TRACE("Line3:%d!", line4);
		return line4;
	}
	ERROR("Line Error:%d!", iLine);
	return -1;
}

bool Display::repaint(unsigned short iKeyCode)
{
	if ((iKeyCode != BUTTON_UP) && (iKeyCode != BUTTON_DOWN)
			&& (iKeyCode != BUTTON_OK))
	{
		return false;
	}
	switch (iKeyCode)
	{
	case BUTTON_UP:
		DEBUG("BUTTON_UP!");
		m_chKeyIndex = KeyTab[m_chKeyIndex].chUpState;
		break;
	case BUTTON_DOWN:
		DEBUG("BUTTON_DOWN!");
		m_chKeyIndex = KeyTab[m_chKeyIndex].chDnState;
		break;
	case BUTTON_OK:
		DEBUG("BUTTON_OK!");
		m_chKeyIndex = KeyTab[m_chKeyIndex].chCfState;
		addCmdStr(m_chKeyIndex);
		break;
	default:
		ERROR("No this Key!%d!", iKeyCode);
		return false;
	}
	DEBUG("m_chKeyIndex:%d!", m_chKeyIndex);
	(*(KeyTab[m_chKeyIndex].Operation))();
	return true;
}

bool Display::addCmdStr(unsigned char chIndex)
{
	switch (chIndex)
	{
	case MENU_MOD_EXC_0:
		DEBUG("Add MOD 0 Str!");
		m_strCmdBuff = "MOD 0";
		break;
	case MENU_MOD_EXC_1:
		DEBUG("Add MOD 1 Str!");
		m_strCmdBuff = "MOD 1";
		break;
	case MENU_MOD_EXC_2:
		DEBUG("Add MOD 2 Str!");
		m_strCmdBuff = "MOD 2";
		break;
	case MENU_STRH_EXC_0:
		DEBUG("To Change Streng to High!");
		break;
	case MENU_STRH_EXC_1:
		DEBUG("To Change Streng to Media!");
		break;
	case MENU_STRH_EXC_2:
		DEBUG("To Change Streng to Low!");
		break;
	default:
		return false;
	}
	ERROR("No Cmd to Excute:%d!", chIndex);
	return true;
}

bool Display::Write(unsigned int iLine, unsigned int iStartPos,
		unsigned char* szData, unsigned char chLen)
{

	LINE_CMD iLineSerial = (LINE_CMD) getLineSerial(iLine);
	if ((iLineSerial == line1) || (iLineSerial == line2)
			|| (iLineSerial == line3) || (iLineSerial == line4))
	{
		m_objLcd.lcd_write_matrix(iLineSerial, (unsigned char) iStartPos,
				(FONT_MATRIX *) szData, chLen);
		return true;
	}
	return false;
}

//Main Menu, Selected Choice 0 for Model
int Display::InitDisplay()
{
	DEBUG("InitDisplay!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_1);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	return 0;
}

//Main Menu, Selected Choice 1 for Strength
int Display::Process0_1()
{
	DEBUG("Process0_1!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_2);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	return 0;
}

//Model Menu, Selected Choice Model 0
int Display::Process1_0()
{
	DEBUG("Process1_0!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_1);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Model Menu, Selected Choice Model 1
int Display::Process1_1()
{
	DEBUG("Process1_1!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_2);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Model Menu, Selected Choice Model 2
int Display::Process1_2()
{
	DEBUG("Process1_2!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_3);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Model Menu, Selected Choice Back Up to Main Menu
int Display::Process1_3()
{
	DEBUG("Process1_3!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_3);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_2, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Strength Menu, Selected Choice Strength High
int Display::Process2_0()
{
	DEBUG("Process2_0!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_1);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Strength Menu, Selected Choice Strength Media
int Display::Process2_1()
{
	DEBUG("Process2_1!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_2);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Strength Menu, Selected Choice Strength Low
int Display::Process2_2()
{
	DEBUG("Process2_2!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_3);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START, (unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_2, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Strength Menu, Selected Choice Back Up to Main Menu
int Display::Process2_3()
{
	DEBUG("Process2_3!");
	m_objLcd.lcd_clear_all();
	setCursor(DISP_LINE_3);		//todo: OR high light
	Write(DISP_LINE_1, MENU_START + MENU_START, (unsigned char*) (ZHENG_HZ),
			12);
	Write(DISP_LINE_2, MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	Write(DISP_LINE_3, MENU_START + MENU_START + MENU_START + MENU_START,
			(unsigned char*) (ZHENG_HZ), 12);
	return 0;
}

//Model 0 Excute Menu, Selected Single Choice Back Up to Model Menu
int Display::Process3_0()
{
	DEBUG("Process3_0!");
	m_objLcd.lcd_clear_all();
//display model 0 excute info
//dispaly selected info to back up
	return 0;
}

//Model 1 Excute Menu, Selected Single Choice Back Up to Model Menu
int Display::Process3_1()
{
	DEBUG("Process3_1!");
	m_objLcd.lcd_clear_all();
//display model 1 excute info
//dispaly selected info to back up
	return 0;
}

//Model 2 Excute Menu, Selected Single Choice Back Up to Model Menu
int Display::Process3_2()
{
	DEBUG("Process3_2!");
	m_objLcd.lcd_clear_all();
//display model 2 excute info
//dispaly selected info to back up
	return 0;
}

//Strength High Excute Menu, Selected Single Choice Back Up to Model Menu
int Display::Process4_0()
{
	DEBUG("Process4_0!");
	m_objLcd.lcd_clear_all();
//display strength high excute info
//dispaly selected info to back up
	return 0;
}

//Strength Media Excute Menu, Selected Single Choice Back Up to Model Menu
int Display::Process4_1()
{
	DEBUG("Process4_1!");
	m_objLcd.lcd_clear_all();
//display strength media excute info
//dispaly selected info to back up
	return 0;
}

//Strength Low Excute Menu, Selected Single Choice Back Up to Model Menu
int Display::Process4_2()
{
	DEBUG("Process4_2!");
	m_objLcd.lcd_clear_all();
//display strength low excute info
//dispaly selected info to back up
	return 0;
}

bool Display::setCursor(unsigned int iLine)
{
	if (Write(iLine, CURSOR_START, (unsigned char*) ZHENG_HZ, 12) == true)
	{
		return true;
	}
	ERROR("Set Cursor fail!");
	return false;
}

